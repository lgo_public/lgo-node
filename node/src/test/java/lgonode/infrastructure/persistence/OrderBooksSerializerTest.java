package lgonode.infrastructure.persistence;

import lgonode.domain.*;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.*;

import static org.assertj.core.api.Assertions.*;

class OrderBooksSerializerTest {

    @Test
    void it_serializes() throws IOException {
        Map<Pair, OrderBook> orderBooks = new HashMap<>();
        OrderBook orderBookBTCUSD = new OrderBook();
        Order order = new Order(1, 1, "key", 22, "L", "B", TestUtils.Pairs.BTC_USD, "0.1", "1000");
        order.consume(TestUtils.Currencies.BTC.parse("0.01"));
        orderBookBTCUSD.side("B").enter(order);
        orderBookBTCUSD.side("B").enter(new Order(2, 1, "key", 23, "L", "B", TestUtils.Pairs.BTC_USD, "17.2", "1100"));
        orderBookBTCUSD.side("S").enter(new Order(3, 1, "key", 24, "L", "S", TestUtils.Pairs.BTC_USD, "3", "1200"));
        orderBooks.put(TestUtils.Pairs.BTC_USD, orderBookBTCUSD);
        OrderBook orderBookTLGO = new OrderBook();
        orderBookTLGO.side("B").enter(new Order(4, 1, "key", 25, "L", "B", TestUtils.Pairs.tLGO, "1.6", "10"));
        orderBooks.put(TestUtils.Pairs.tLGO, orderBookTLGO);

        String content = new OrderBooksSerializer().serialize(orderBooks);

        assertThat(content).isEqualTo(
                "{\"BTC-USD\":{\"B\":{\"11000000\":[{\"id\":2,\"qt\":1720000000}],\"10000000\":[{\"id\":1,\"qt\":9000000}]},\"S\":{\"12000000\":[{\"id\":3,\"qt\":300000000}]}}," +
                "\"TLGO1-TLGO2\":{\"B\":{\"100000\":[{\"id\":4,\"qt\":160000000}]},\"S\":{}}}");
    }
}