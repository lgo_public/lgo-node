package lgonode.infrastructure.persistence.repository;

import lgonode.domain.Order;
import lgonode.domain.*;
import lgonode.domain.configuration.ConfigurationHelper;
import lgonode.infrastructure.persistence.JooqTestCase;
import lgonode.infrastructure.persistence.dao.OrderDAO;
import org.junit.jupiter.api.*;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.*;

class OrderJooqRepositoryTest extends JooqTestCase {

    public OrderJooqRepositoryTest() {
        super(OrderDAO.TABLE.getName());
    }

    @BeforeEach
    void setUp() {
        new BatchJooqRepository(dsl()).save(new Metadata(1, "hash", "previousHash", 23, false, -1, null));
        orderJooqRepository = new OrderJooqRepository(dsl());
    }

    @Test
    void saves_and_loads_order() {
        Order order1 = new Order(123L, 1, "keyid", 56789L, "L", "S", TestUtils.Pairs.BTC_USD, "4.35000000", "3200.00");
        Order order2 = new Order(124L, 1, "keyid", 56790L, "M", "B", TestUtils.Pairs.BTC_USD, "4.35000000", null);
        orderJooqRepository.save(Arrays.asList(order1, order2));
        var configuration = ConfigurationHelper.defaultConfiguration(1);

        Order retrievedOrder1 = orderJooqRepository.load(123L, configuration);
        Order retrievedOrder2 = orderJooqRepository.load(124L, configuration);

        assertThat(retrievedOrder1).isEqualToComparingFieldByField(order1);
        assertThat(retrievedOrder2).isEqualToComparingFieldByField(order2);
    }

    private OrderJooqRepository orderJooqRepository;
}