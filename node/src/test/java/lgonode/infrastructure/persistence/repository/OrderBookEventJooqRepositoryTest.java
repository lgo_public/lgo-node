package lgonode.infrastructure.persistence.repository;

import lgonode.domain.*;
import lgonode.infrastructure.persistence.JooqTestCase;
import lgonode.infrastructure.persistence.dao.OrderBookEventsDAO;
import org.jooq.*;
import org.junit.jupiter.api.Test;

import java.util.*;

import static lgonode.domain.TestUtils.Currencies.*;
import static org.assertj.core.api.Assertions.*;

class OrderBookEventJooqRepositoryTest extends JooqTestCase {

    public OrderBookEventJooqRepositoryTest() {
        super(OrderBookEventsDAO.TABLE.getName());
    }

    @Test
    void it_saves_the_events() {
        OrderBookEventJooqRepository repository = new OrderBookEventJooqRepository(dsl());
        BatchJooqRepository batchRepository = new BatchJooqRepository(dsl());
        batchRepository.save(new Metadata(13, "hash", "hash", 4, false, -1, null));
        batchRepository.save(new Metadata(14, "hash", "hash", 4, false, -1, null));
        List<OrderBookEvent> eventList = Arrays.asList(new OrderBookUpdateOrderEvent(13, "BTC", "USD", "S", 1000000, 14, BTC.parse("5")),
                new OrderBookAddOrderEvent(13, "BTC", "USD", "B", 1000000, 16, BTC.parse("10")),
                new OrderBookUpdateOrderEvent(13, "BTC", "USD", "B", 1000000, 17, BTC.parse("12")),
                new OrderBookUpdateOrderEvent(13, "BTC", "USD", "B", 1000000, 18, BTC.parse("12")),
                new OrderBookUpdateOrderEvent(13, "BTC", "USD", "B", 1000000, 18, BTC.parse("2")),
                new OrderBookRemoveOrderEvent(13, "BTC", "USD", "B", 1000000, 18),
                new OrderBookUpdateOrderEvent(13, "BTC", "USD", "S", 1000000, 20, BTC.parse("8")),
                new OrderBookCancelOrdersEvent(14, "BTC", "USD"));

        repository.save(eventList);

        Result<Record> records = dsl().selectFrom(OrderBookEventsDAO.TABLE).fetch();
        assertThat(records).hasSize(8);
        assertThat(records.get(0).get(OrderBookEventsDAO.ID)).isEqualTo(1);
        assertThat(records.get(0).get(OrderBookEventsDAO.BATCH_ID)).isEqualTo(13);
        assertThat(records.get(0).get(OrderBookEventsDAO.BASE_CURRENCY)).isEqualTo("BTC");
        assertThat(records.get(0).get(OrderBookEventsDAO.QUOTE_CURRENCY)).isEqualTo("USD");
        assertThat(records.get(0).get(OrderBookEventsDAO.TYPE)).isEqualTo(OrderBookEventType.UPDATE_ORDER.name());
        assertThat(records.get(0).get(OrderBookEventsDAO.PAYLOAD)).isEqualTo("S,1000000,14,500000000");
        assertThat(records.get(1).get(OrderBookEventsDAO.ID)).isEqualTo(2);
        assertThat(records.get(1).get(OrderBookEventsDAO.BATCH_ID)).isEqualTo(13);
        assertThat(records.get(1).get(OrderBookEventsDAO.BASE_CURRENCY)).isEqualTo("BTC");
        assertThat(records.get(1).get(OrderBookEventsDAO.QUOTE_CURRENCY)).isEqualTo("USD");
        assertThat(records.get(1).get(OrderBookEventsDAO.TYPE)).isEqualTo(OrderBookEventType.ADD_ORDER.name());
        assertThat(records.get(1).get(OrderBookEventsDAO.PAYLOAD)).isEqualTo("B,1000000,16,1000000000");
        assertThat(records.get(5).get(OrderBookEventsDAO.ID)).isEqualTo(6);
        assertThat(records.get(5).get(OrderBookEventsDAO.BATCH_ID)).isEqualTo(13);
        assertThat(records.get(5).get(OrderBookEventsDAO.BASE_CURRENCY)).isEqualTo("BTC");
        assertThat(records.get(5).get(OrderBookEventsDAO.QUOTE_CURRENCY)).isEqualTo("USD");
        assertThat(records.get(5).get(OrderBookEventsDAO.TYPE)).isEqualTo(OrderBookEventType.REMOVE_ORDER.name());
        assertThat(records.get(5).get(OrderBookEventsDAO.PAYLOAD)).isEqualTo("B,1000000,18");
        assertThat(records.get(7).get(OrderBookEventsDAO.ID)).isEqualTo(8);
        assertThat(records.get(7).get(OrderBookEventsDAO.BATCH_ID)).isEqualTo(14);
        assertThat(records.get(7).get(OrderBookEventsDAO.BASE_CURRENCY)).isEqualTo("BTC");
        assertThat(records.get(7).get(OrderBookEventsDAO.QUOTE_CURRENCY)).isEqualTo("USD");
        assertThat(records.get(7).get(OrderBookEventsDAO.TYPE)).isEqualTo(OrderBookEventType.CANCEL_BOOK.name());
        assertThat(records.get(7).get(OrderBookEventsDAO.PAYLOAD)).isEqualTo("");
    }

    @Test
    void it_loads_the_events() {
        OrderBookEventJooqRepository repository = new OrderBookEventJooqRepository(dsl());
        BatchJooqRepository batchRepository = new BatchJooqRepository(dsl());
        batchRepository.save(new Metadata(13, "hash", "hash", 4, false, -1, null));
        batchRepository.save(new Metadata(14, "hash", "hash", 4, false, -1, null));
        List<OrderBookEvent> eventList = Arrays.asList(new OrderBookUpdateOrderEvent(13, "BTC", "USD", "S", 1000000, 14, BTC.parse("5")),
                new OrderBookAddOrderEvent(13, "BTC", "USD", "B", 1000000, 16, BTC.parse("10")),
                new OrderBookUpdateOrderEvent(13, "BTC", "USD", "B", 1000000, 17, BTC.parse("12")),
                new OrderBookUpdateOrderEvent(13, "BTC", "USD", "B", 1000000, 18, BTC.parse("12")),
                new OrderBookUpdateOrderEvent(13, "BTC", "USD", "B", 1000000, 18, BTC.parse("2")),
                new OrderBookRemoveOrderEvent(13, "BTC", "USD", "B", 1000000, 155386625116800001L),
                new OrderBookUpdateOrderEvent(13, "BTC", "USD", "S", 1000000, 20, BTC.parse("8")),
                new OrderBookCancelOrdersEvent(14, "BTC", "USD"));
        repository.save(eventList);

        List<OrderBookEvent> after12 = repository.loadEventsAfterBatch(12);
        List<OrderBookEvent> after13 = repository.loadEventsAfterBatch(13);
        List<OrderBookEvent> after14 = repository.loadEventsAfterBatch(14);
        List<OrderBookEvent> after_1 = repository.loadEventsAfterBatch(-1);

        assertThat(after12).hasSize(8);
        assertThat(after13).hasSize(1);
        assertThat(after14).hasSize(0);
        assertThat(after_1).hasSize(8);
        assertThat(after12.get(0)).isEqualToComparingFieldByField(eventList.get(0));
        assertThat(after12.get(1)).isEqualToComparingFieldByField(eventList.get(1));
        assertThat(after12.get(5)).isEqualToComparingFieldByField(eventList.get(5));
        assertThat(after12.get(7)).isEqualToComparingFieldByField(eventList.get(7));
        assertThat(after13.get(0)).isEqualToComparingFieldByField(eventList.get(7));
    }
}