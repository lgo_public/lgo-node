package lgonode.infrastructure.persistence.repository;

import lgonode.domain.*;
import lgonode.infrastructure.persistence.JooqTestCase;
import lgonode.infrastructure.persistence.dao.ProofDAO;
import org.junit.jupiter.api.*;

import java.util.List;

import static org.assertj.core.api.Assertions.*;

class ProofJooqRepositoryTest extends JooqTestCase {

    private ProofJooqRepository repository;

    ProofJooqRepositoryTest() {
        super(ProofDAO.TABLE.getName());
    }

    @BeforeEach
    void before() {
        repository = new ProofJooqRepository(dsl());
    }

    @Test
    void saves_proof() {
        BatchJooqRepository batchJooqRepository = new BatchJooqRepository(dsl());
        batchJooqRepository.save(new Metadata(123L, "hash", "previousHash", 23, false, -1, null));
        Proof proof = new Proof(123L, "aaa", "123".getBytes());

        repository.save(proof);

        List<Proof> proofsList = dsl().selectFrom(ProofDAO.TABLE).fetchInto(Proof.class);
        assertThat(proofsList).hasSize(1);
        assertThat(proofsList.get(0)).isEqualToComparingFieldByField(proof);
    }
}