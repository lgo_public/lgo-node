package lgonode.infrastructure.persistence.repository;

import lgonode.domain.*;
import lgonode.infrastructure.persistence.JooqTestCase;
import lgonode.infrastructure.persistence.dao.CancelOrderDAO;
import org.junit.jupiter.api.*;

import java.util.List;

import static java.util.Collections.*;
import static lgonode.infrastructure.persistence.dao.CancelOrderDAO.*;
import static org.assertj.core.api.Assertions.*;

class CancelOrderJooqRepositoryTest extends JooqTestCase {

    public CancelOrderJooqRepositoryTest() {
        super(CancelOrderDAO.TABLE.getName());
    }

    @BeforeEach
    void setUp() {
        new BatchJooqRepository(dsl()).save(new Metadata(1, "hash", "previousHash", 23, false, -1, null));
    }

    @Test
    void saves_a_cancel_order() {
        CancelOrder cancelOrder = new CancelOrder(123L, 1, "keyid", 56789L, 26L);

        new CancelOrderJooqRepository(dsl()).save(singletonList(cancelOrder));

        List<CancelOrder> cancelOrders = dsl().select(ID, BATCH_ID, KEY_ID, CREATION_DATE, ORDER_TO_CANCEL).from(CancelOrderDAO.TABLE).fetchInto(CancelOrder.class);
        assertThat(cancelOrders).hasSize(1);
        assertThat(cancelOrders.get(0)).isEqualToComparingFieldByField(cancelOrder);
    }
}