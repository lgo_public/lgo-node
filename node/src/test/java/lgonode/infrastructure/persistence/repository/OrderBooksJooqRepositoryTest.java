package lgonode.infrastructure.persistence.repository;

import lgonode.domain.*;
import lgonode.infrastructure.persistence.JooqTestCase;
import lgonode.infrastructure.persistence.dao.OrderBooksDAO;
import org.junit.jupiter.api.*;

import java.util.*;

import static lgonode.infrastructure.persistence.dao.OrderBooksDAO.*;
import static org.assertj.core.api.Assertions.*;

class OrderBooksJooqRepositoryTest extends JooqTestCase {

    private OrderBooksJooqRepository orderBooksJooqRepository;

    public OrderBooksJooqRepositoryTest() {
        super(OrderBooksDAO.TABLE.getName());
    }

    @BeforeEach
    void setUp() {
        BatchJooqRepository repository = new BatchJooqRepository(dsl());
        repository.save(new Metadata(0, "hash", "previousHash", 23, false, -1, null));
        repository.save(new Metadata(1, "hash", "previousHash", 23, false, -1, null));
        orderBooksJooqRepository = new OrderBooksJooqRepository(dsl());
    }

    @Test
    void saves_content() {

        new OrderBooksJooqRepository(dsl()).save(1, "plop");

        List<String> content = dsl().select(CONTENT).from(TABLE).where(BATCH_ID.eq(1L)).fetchInto(String.class);
        assertThat(content).hasSize(1);
        assertThat(content.get(0)).isEqualTo("plop");
    }

    @Test
    void loads_content() {
        orderBooksJooqRepository.save(1, "plop");

        OrderBookSnapshot res = orderBooksJooqRepository.lastOrderBook().get();

        assertThat(res.serializedContent).isEqualTo("plop");
        assertThat(res.batchId).isEqualTo(1);
    }

    @Test
    void loads_last_saved() {
        orderBooksJooqRepository.save(0, "plop0");
        orderBooksJooqRepository.save(1, "plop1");

        OrderBookSnapshot res = orderBooksJooqRepository.lastOrderBook().get();

        assertThat(res.batchId).isEqualTo(1);
        assertThat(res.serializedContent).isEqualTo("plop1");
    }

    @Test
    void it_returns_empty_when_nothing_saved() {
        Optional<OrderBookSnapshot> res = orderBooksJooqRepository.lastOrderBook();

        assertThat(res).isEmpty();
    }
}