package lgonode.infrastructure.persistence.repository;

import lgonode.domain.*;
import lgonode.infrastructure.persistence.JooqTestCase;
import lgonode.infrastructure.persistence.dao.TradeDAO;
import org.junit.jupiter.api.*;

import java.util.*;

import static lgonode.infrastructure.persistence.dao.TradeDAO.*;
import static org.assertj.core.api.Assertions.*;

class TradeJooqRepositoryTest extends JooqTestCase {

    public TradeJooqRepositoryTest() {
        super(TradeDAO.TABLE.getName());
    }

    @BeforeEach
    void setUp() {
        new BatchJooqRepository(dsl()).save(new Metadata(1, "hash", "previousHash", 23, false, -1, null));
    }

    @Test
    void saves_trades() {
        Trade trade1 = new Trade(1, TestUtils.Currencies.USD.parse("100"), TestUtils.Currencies.BTC.parse("1"), TestUtils.Pairs.BTC_USD, 225);
        Trade trade2 = new Trade(1, TestUtils.Currencies.USD.parse("200"), TestUtils.Currencies.LGO.parse("2"), TestUtils.Pairs.LGO_USD, 226);

        new TradeJooqRepository(dsl()).save(Arrays.asList(trade1, trade2));

        List<Trade> savedTrades = dsl()
                .select(ID, BATCH_ID, CREATION_DATE, BASE_CURRENCY, QUOTE_CURRENCY, QUANTITY, PRICE)
                .from(TradeDAO.TABLE)
                .orderBy(TradeDAO.ID)
                .fetch(record -> new Trade(record.component1(), record.component2(), record.component7(), record.component6(), TestUtils.Pairs.of(record.component4(), record.component5()), record.component3()));
        assertThat(savedTrades).hasSize(2);
        assertThat(savedTrades.get(0)).isEqualToIgnoringGivenFields(trade1, "id");
        assertThat(savedTrades.get(0).id).isEqualTo(1);
        assertThat(savedTrades.get(1)).isEqualToIgnoringGivenFields(trade2, "id");
        assertThat(savedTrades.get(1).id).isEqualTo(2);
    }
}