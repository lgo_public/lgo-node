package lgonode.infrastructure.persistence;

import com.google.common.collect.Maps;
import lgonode.domain.*;
import lgonode.infrastructure.BatchProcessingContext;
import org.junit.jupiter.api.Test;
import org.mockito.*;

import static java.util.Collections.*;
import static org.mockito.Mockito.*;

class BatchPersistenceStepTest {

    @Test
    void saves_all_data_starting_with_batch_metadata() {
        BatchRepository batchRepository = mock(BatchRepository.class);
        OrderRepository orderRepository = mock(OrderRepository.class);
        FailedOrderRepository failedOrderRepository = mock(FailedOrderRepository.class);
        CancelOrderRepository cancelOrderRepository = mock(CancelOrderRepository.class);
        ProofRepository proofRepository = mock(ProofRepository.class);
        TradeRepository tradeRepository = mock(TradeRepository.class);
        OrderBooksRepository orderBooksRepository = mock(OrderBooksRepository.class);
        BatchPersistenceStep step = new BatchPersistenceStep(batchRepository, orderRepository, failedOrderRepository, cancelOrderRepository, proofRepository, singleton(tradeRepository), orderBooksRepository, mock(OrderBookEventRepository.class));
        BatchProcessingContext context = BatchProcessingContext.create("00001", "lastBatchHash");
        context.failedOrderRead(1, "reason", Maps.newHashMap());
        context.orderContentRead(2, "keyid", 123456L, "M", "S", TestUtils.Pairs.BTC_USD, "1.20000000", null);
        context.cancelOrderContentRead(3, "keyid", 123457L, 2);
        context.metadataRead(1, "hash", "previous", 3, false, -1, null);
        context.matched(1000, 10000, TestUtils.Pairs.BTC_USD, 1234);
        context.otsProofRead("hash", "toto".getBytes());
        context.orderBookSerialized("plop");

        step.process(context);

        InOrder inOrder = Mockito.inOrder(batchRepository, orderRepository, failedOrderRepository, cancelOrderRepository, proofRepository, tradeRepository, orderBooksRepository);
        inOrder.verify(batchRepository).save(context.metadata());
        inOrder.verify(orderBooksRepository).save(context.batchId(), "plop");
        inOrder.verify(orderRepository).save(context.orders());
        inOrder.verify(failedOrderRepository).save(context.failedOrders());
        inOrder.verify(cancelOrderRepository).save(context.cancelOrders());
        inOrder.verify(proofRepository).save(context.otsProof());
        inOrder.verify(tradeRepository).save(context.trades());
    }
}