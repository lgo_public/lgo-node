package lgonode.infrastructure.persistence;

import lgonode.domain.*;
import lgonode.domain.TestUtils.Currencies;
import lgonode.domain.configuration.ConfigurationHelper;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Map;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

class OrderBooksDeserializerTest {

    @Test
    void it_deserialize() throws IOException {
        Order order1NotFilled = new Order(1, 1, "key", 22, "L", "B", TestUtils.Pairs.BTC_USD, "0.1", "1000");
        Order order1 = new Order(1, 1, "key", 22, "L", "B", TestUtils.Pairs.BTC_USD, "22.4", "1000");
        order1.consume(Currencies.BTC.parse("0.01"));
        Order order2 = new Order(2, 1, "key", 23, "L", "B", TestUtils.Pairs.BTC_USD, "17.2", "1100");
        Order order3 = new Order(3, 1, "key", 24, "L", "S", TestUtils.Pairs.BTC_USD, "3", "1200");
        Order order4 = new Order(4, 1, "key", 25, "L", "B", TestUtils.Pairs.tLGO, "1.6", "10");
        OrderRepository orderRepository = mock(OrderRepository.class);
        var configuration = ConfigurationHelper.defaultConfiguration(1);
        when(orderRepository.load(1, configuration)).thenReturn(order1NotFilled);
        when(orderRepository.load(2, configuration)).thenReturn(order2);
        when(orderRepository.load(3, configuration)).thenReturn(order3);
        when(orderRepository.load(4, configuration)).thenReturn(order4);

        String content = "{\"BTC-USD\":{\"B\":{\"11000000\":[{\"id\":2,\"qt\":1720000000}],\"10000000\":[{\"id\":1,\"qt\":9000000}]},\"S\":{\"12000000\":[{\"id\":3,\"qt\":300000000}]}}," +
                "\"tLGO1-tLGO2\":{\"B\":{\"100000\":[{\"id\":4,\"qt\":160000000}]},\"S\":{}}}";
        Map<Pair, OrderBook> orderBooks = new OrderBooksDeserializer(orderRepository).deserialize(content, configuration);

        assertThat(orderBooks.size()).isEqualTo(2);
        assertThat(orderBooks.keySet()).containsExactlyInAnyOrder(TestUtils.Pairs.BTC_USD, TestUtils.Pairs.tLGO);
        assertThat(new OrderBooksSerializer().serialize(orderBooks)).isEqualToIgnoringCase(content);
    }
}