package lgonode.infrastructure.decipher;

import lgonode.domain.DecryptedOrder;
import lgonode.infrastructure.BatchProcessingContext;
import lgonode.infrastructure.avro.BatchFilesReadingStep;
import lgonode.infrastructure.gcs.GCSKeyBucketReader;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.jupiter.api.*;

import java.io.*;
import java.nio.file.Path;
import java.security.Security;
import java.util.*;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

class OrderDecryptionStepTest {


    @BeforeEach
    void setUp() {
        Security.addProvider(new BouncyCastleProvider());
        gcsKeyBucketReader = mock(GCSKeyBucketReader.class);
    }

    @AfterEach
    void tearDown() throws IOException {
        fileReader.close();
    }

    @Test
    void it_processes_old_order_format() throws FileNotFoundException {
        setUp1();

        orderDecipherer.process(context);

        Optional<DecryptedOrder> order = context.decryptedOrders().stream().filter(o -> o.id == 155248875816700001L).findFirst();
        assertThat(order.isPresent()).isTrue();
        DecryptedOrder decryptedOrder = order.get();
        assertThat(decryptedOrder.data).isEqualTo("L,B,BTC-USD,7.7855,26.4,gtc,1552488758");
        assertThat(decryptedOrder.id).isEqualTo(155248875816700001L);
    }

    @Test
    void it_processes_new_order_format() throws FileNotFoundException {
        setUp2();

        orderDecipherer.process(context);

        List<DecryptedOrder> decryptedOrders = context.decryptedOrders();
        assertThat(decryptedOrders).hasSize(19);
        assertThat(decryptedOrders.get(0).id).isEqualTo(156715853297600001L);
        assertThat(decryptedOrders.get(0).data).isEqualTo("M,B,BTC-USD,100,");
        assertThat(decryptedOrders.get(0).keyId).isEqualTo("99d02355-e687-4dfc-a7c2-429b5bb55a2d");
        assertThat(decryptedOrders.get(1).id).isEqualTo(156715853298200001L);
        assertThat(decryptedOrders.get(1).data).isEqualTo("M,B,BTC-USD,200,");
        assertThat(decryptedOrders.get(1).keyId).isEqualTo("N/A");
    }

    @Test
    void it_does_not_fail_on_malformed_order() throws FileNotFoundException {
        setUp3();

        orderDecipherer.process(context);

        List<DecryptedOrder> decryptedOrders = context.decryptedOrders();
        assertThat(decryptedOrders).hasSize(1);
        assertThat(decryptedOrders.get(0).id).isEqualTo(158657015120800001L);
        assertThat(decryptedOrders.get(0).data).isEqualTo("C,158657015064200001,1586570151306");
        assertThat(decryptedOrders.get(0).keyId).isEqualTo("6f46e502-c60a-4dff-81f2-c8d9c89bf6ae");
    }

    void setUp1() throws FileNotFoundException {
        Path pathBatch = new File(getClass().getClassLoader().getResource("files/batch").getFile()).toPath();
        context = BatchProcessingContext.create("0002020005", "lastBatchHash");
        new BatchFilesReadingStep(pathBatch.toString()).process(context);
        Path path = new File(getClass().getClassLoader().getResource("files/key").getFile()).toPath();

        fileReader = new FileReader(path.resolve("618bbc95-b6d4-4a24-8967-bfa8e83367b1_private.pem").toFile());
        when(gcsKeyBucketReader.loadPrivateKey("618bbc95-b6d4-4a24-8967-bfa8e83367b1")).thenReturn(Optional.of(fileReader));
        orderDecipherer = new OrderDecryptionStep(new KeyRetriever(gcsKeyBucketReader));
    }

    void setUp2() throws FileNotFoundException {
        Path pathBatch = new File(getClass().getClassLoader().getResource("files/batch").getFile()).toPath();
        context = BatchProcessingContext.create("0000000034", "9ab403c91bfe0af3b40bf6ecdad466f1fa1a24be8c6d857a5cf21df7e22fb052");
        new BatchFilesReadingStep(pathBatch.toString()).process(context);
        Path path = new File(getClass().getClassLoader().getResource("files/key").getFile()).toPath();

        fileReader = new FileReader(path.resolve("99d02355-e687-4dfc-a7c2-429b5bb55a2d_private.pem").toFile());
        when(gcsKeyBucketReader.loadPrivateKey("99d02355-e687-4dfc-a7c2-429b5bb55a2d")).thenReturn(Optional.of(fileReader));
        orderDecipherer = new OrderDecryptionStep(new KeyRetriever(gcsKeyBucketReader));
    }

    void setUp3() throws FileNotFoundException {
        Path pathBatch = new File(getClass().getClassLoader().getResource("files/batch").getFile()).toPath();
        context = BatchProcessingContext.create("0066804237", "cb39a532316deec85e69dbe75e312b4eb0b3aa4a000f4a0872a280407e595fac");
        new BatchFilesReadingStep(pathBatch.toString()).process(context);
        Path path = new File(getClass().getClassLoader().getResource("files/key").getFile()).toPath();

        fileReader = new FileReader(path.resolve("6f46e502-c60a-4dff-81f2-c8d9c89bf6ae_private.pem").toFile());
        when(gcsKeyBucketReader.loadPrivateKey("6f46e502-c60a-4dff-81f2-c8d9c89bf6ae")).thenReturn(Optional.of(fileReader));
        orderDecipherer = new OrderDecryptionStep(new KeyRetriever(gcsKeyBucketReader));
    }

    private OrderDecryptionStep orderDecipherer;
    private BatchProcessingContext context;
    private GCSKeyBucketReader gcsKeyBucketReader;
    private FileReader fileReader;
}