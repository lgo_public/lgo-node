package lgonode.infrastructure.decipher;

import lgonode.infrastructure.BatchProcessingContext;
import lgonode.infrastructure.ProcessingResult;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.security.PrivateKey;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class EnsurePrivateKeysAreReleasedStepTest {

    @BeforeEach
    void setUp() {
        keyRetriever = mock(KeyRetriever.class);
        step = new EnsurePrivateKeysAreReleasedStep(keyRetriever);
    }

    @Test
    void it_processes() {
        BatchProcessingContext context = BatchProcessingContext.create(1l, "hash");
        context.encryptedOrderRead(2L, "123", new byte[0]);
        context.encryptedOrderRead(2L, "124", new byte[0]);
        when(keyRetriever.getPrivateKey("123")).thenReturn(Optional.of(mock(PrivateKey.class)));
        when(keyRetriever.getPrivateKey("124")).thenReturn(Optional.empty());
        when(keyRetriever.isPublicKeyAvailable("124")).thenReturn(false);

        ProcessingResult result = step.process(context);

        assertThat(result).isEqualTo(ProcessingResult.CONTINUE);
    }

    @Test
    void it_processes_with_missing_private_key() {
        BatchProcessingContext context = BatchProcessingContext.create(1l, "hash");
        context.encryptedOrderRead(2L, "124", new byte[0]);
        when(keyRetriever.getPrivateKey("124")).thenReturn(Optional.empty());
        when(keyRetriever.isPublicKeyAvailable("124")).thenReturn(true);

        ProcessingResult result = step.process(context);

        assertThat(result).isEqualTo(ProcessingResult.WAIT);
    }

    private KeyRetriever keyRetriever;
    private EnsurePrivateKeysAreReleasedStep step;
}