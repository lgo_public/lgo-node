package lgonode.infrastructure.reader;

import lgonode.infrastructure.*;
import org.junit.jupiter.api.*;

import static org.assertj.core.api.Assertions.*;

class EnsureBatchFilesWereReadStepTest {

    @BeforeEach
    void setUp() {
        step = new EnsureBatchFilesWereReadStep();
    }

    @Test
    void check_metadata_read() {
        BatchProcessingContext context = BatchProcessingContext.create(22, "hash");

        ProcessingResult res = step.process(context);

        assertThat(res).isEqualTo(ProcessingResult.FAILURE);
    }

    @Test
    void checks_order_read() {
        BatchProcessingContext context = BatchProcessingContext.create(22, "hash");
        context.metadataRead(22, "hash", "hash", 4, false, -1, null);

        ProcessingResult res = step.process(context);

        assertThat(res).isEqualTo(ProcessingResult.FAILURE);
    }

    @Test
    void succeeds() {
        BatchProcessingContext context = BatchProcessingContext.create(22, "hash");
        context.metadataRead(22, "hash", "hash", 4, false, -1, null);
        context.encryptedOrderRead(1, "key", "data".getBytes());
        context.encryptedOrderRead(2, "key", "data".getBytes());
        context.encryptedOrderRead(3, "key", "data".getBytes());
        context.encryptedOrderRead(4, "key", "data".getBytes());

        ProcessingResult res = step.process(context);

        assertThat(res).isEqualTo(ProcessingResult.CONTINUE);
    }

    private EnsureBatchFilesWereReadStep step;
}