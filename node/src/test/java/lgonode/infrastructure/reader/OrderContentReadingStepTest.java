package lgonode.infrastructure.reader;

import lgonode.domain.*;
import lgonode.domain.configuration.ConfigurationHelper;
import lgonode.infrastructure.*;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.assertj.core.api.Assertions.*;

class OrderContentReadingStepTest {

    @Test
    void it_reads_order() {
        String orderData = "L,B,BTC-USD,7.7855,26.4,gtc,1552488758";
        OrderContentReadingStep orderReader = new OrderContentReadingStep();
        BatchProcessingContext context = BatchProcessingContext.create("000001", "lastBatchHash");
        context.configurationRead(ConfigurationHelper.defaultConfiguration(0));
        context.orderDecrypted(156109795818500001L, "keyid", orderData);

        orderReader.process(context);

        assertThat(context.orders()).hasSize(1);
        Order order = context.orders().get(0);
        assertThat(order.id).isEqualTo(156109795818500001L);
        assertThat(order.type).isEqualTo("L");
        assertThat(order.direction).isEqualTo("B");
        assertThat(order.pair.base.code).isEqualTo("BTC");
        assertThat(order.pair.quote.code).isEqualTo("USD");
        assertThat(order.formatQuantity()).isEqualTo("7.78550000");
        assertThat(order.formatPrice()).isEqualTo("26.4000");
        assertThat(order.batchId).isEqualTo(1);
        assertThat(order.creationDate).isEqualTo(1561097958185L);
    }

    @Test
    void it_reads_cancel_order() {
        String orderData = "C,12345,1552488758";
        OrderContentReadingStep orderReader = new OrderContentReadingStep();
        BatchProcessingContext context = BatchProcessingContext.create("000001", "lastBatchHash");
        context.orderDecrypted(156109795818500001L, "keyid", orderData);

        orderReader.process(context);

        assertThat(context.cancelOrders()).hasSize(1);
        CancelOrder cancelOrder = context.cancelOrders().get(0);
        assertThat(cancelOrder.id).isEqualTo(156109795818500001L);
        assertThat(cancelOrder.batchId).isEqualTo(1);
        assertThat(cancelOrder.orderIdToCancel).isEqualTo(12345L);
        assertThat(cancelOrder.creationDate).isEqualTo(1561097958185L);
    }

    @Test
    void it_does_not_read_if_order_content_is_not_readable() {
        String orderData = "C,03T14:52:55.118Z,1556895175";
        OrderContentReadingStep orderReader = new OrderContentReadingStep();
        BatchProcessingContext context = BatchProcessingContext.create("000001", "lastBatchHash");
        context.orderDecrypted(156109795818500001L, "keyid", orderData);
        context.failedOrderRead(156109795818500001L, "INVALID_PAYLOAD", new HashMap<>());

        ProcessingResult process = orderReader.process(context);

        assertThat(context.cancelOrders()).hasSize(0);
        assertThat(process).isEqualTo(ProcessingResult.CONTINUE);
    }

    @Test
    void it_reads_order_with_post_trade_settlement_product() {
        String orderData = "L,B,USDT.P-USD.P,0.7855,26.4,gtc,1552488758";
        OrderContentReadingStep orderReader = new OrderContentReadingStep();
        BatchProcessingContext context = BatchProcessingContext.create("000001", "lastBatchHash");
        context.configurationRead(ConfigurationHelper.defaultConfiguration(0));
        context.orderDecrypted(156109795818500001L, "keyid", orderData);

        orderReader.process(context);

        assertThat(context.orders()).hasSize(1);
        Order order = context.orders().get(0);
        assertThat(order.id).isEqualTo(156109795818500001L);
        assertThat(order.type).isEqualTo("L");
        assertThat(order.direction).isEqualTo("B");
        assertThat(order.pair).isEqualTo(TestUtils.Pairs.USDTP_USDP);
        assertThat(order.pair.base).isEqualTo(TestUtils.Currencies.USDT_P);
        assertThat(order.pair.quote).isEqualTo(TestUtils.Currencies.USD_P);
        assertThat(order.formatQuantity()).isEqualTo("0.7855");
        assertThat(order.formatPrice()).isEqualTo("26.4000");
        assertThat(order.batchId).isEqualTo(1);
        assertThat(order.creationDate).isEqualTo(1561097958185L);
    }
}