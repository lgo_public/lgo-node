package lgonode.infrastructure.matching;

import lgonode.domain.Currency;
import lgonode.domain.Order;
import lgonode.domain.*;
import lgonode.domain.configuration.*;
import lgonode.infrastructure.BatchProcessingContext;
import lgonode.infrastructure.persistence.*;
import org.junit.jupiter.api.*;

import java.io.IOException;
import java.util.*;

import static lgonode.domain.TestUtils.*;
import static org.assertj.core.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@SuppressWarnings("OptionalGetWithoutIsPresent")
class MatchingStepTest {

    @BeforeEach
    void setUp() {
        repository = mock(OrderRepository.class);
        matching = new MatchingStep(new OrderBooksSerializer(), new OrderBooksDeserializer(repository), repository, mock(ConfigurationLoader.class));
    }

    @Test
    void it_adds_an_order_to_the_right_orderbook_side() {
        BatchProcessingContext batch = BatchProcessingContext.create(0, null);
        batch.metadataRead(0, "", "", 1, false, -1, null);
        batch.configurationRead(ConfigurationHelper.defaultConfiguration(0));
        batch.orderContentRead(1, "", 1, "L", "B", TestUtils.Pairs.tLGO, "1", "10");

        matching.process(batch);

        assertThat(batch.trades()).isEmpty();
        assertThat(matching.books).hasSize(1);
        assertThat(matching.books.get(Pairs.tLGO).side("B").topOrder().get().id).isEqualTo(1);
        assertThat(matching.books.get(Pairs.tLGO).side("S").topOrder()).isEmpty();
    }

    @Test
    void it_adds_orders_in_fifo_order() {
        BatchProcessingContext batch = BatchProcessingContext.create(0, null);
        batch.metadataRead(0, "", "", 1, false, -1, null);
        batch.configurationRead(ConfigurationHelper.defaultConfiguration(0));
        batch.orderContentRead(1, "", 1, "L", "B", TestUtils.Pairs.tLGO, "1", "10");
        batch.orderContentRead(2, "", 1, "L", "B", TestUtils.Pairs.tLGO, "1", "10");

        matching.process(batch);

        assertThat(batch.trades()).isEmpty();
        assertThat(matching.books).hasSize(1);
        assertThat(matching.books.get(Pairs.tLGO).side("B").topOrder().get().id).isEqualTo(1);
        assertThat(matching.books.get(Pairs.tLGO).side("S").topOrder()).isEmpty();
    }


    @Test
    void it_cancels_a_resting_order() {
        BatchProcessingContext batch = BatchProcessingContext.create(1, null);
        batch.metadataRead(1, "", "", 1, false, -1, null);
        batch.configurationRead(ConfigurationHelper.defaultConfiguration(1));
        batch.orderContentRead(1, "", 1, "L", "B", TestUtils.Pairs.tLGO, "1", "10");
        matching.process(batch);
        BatchProcessingContext batch2 = BatchProcessingContext.create(1, null);
        batch2.metadataRead(1, "", "", 1, false, -1, null);
        batch2.configurationRead(ConfigurationHelper.defaultConfiguration(1));
        batch2.cancelOrderContentRead(2, "", 1, 1);

        matching.process(batch2);

        assertThat(batch2.trades()).isEmpty();
        assertThat(matching.books.get(Pairs.tLGO).side("B").topOrder()).isEmpty();
        assertThat(matching.books.get(Pairs.tLGO).side("S").topOrder()).isEmpty();
    }

    @Test
    void it_does_not_execute_a_failed_order() {
        BatchProcessingContext batch = BatchProcessingContext.create(3, null);
        batch.configurationRead(ConfigurationHelper.defaultConfiguration(3));
        batch.metadataRead(3, "", "", 1, false, -1, null);
        batch.orderContentRead(3, "", 1, "L", "S", TestUtils.Pairs.BTC_USD, "1", "20");
        batch.orderContentRead(4, "", 1, "L", "B", TestUtils.Pairs.BTC_USD, "1", "21");
        batch.failedOrderRead(4, "insufficent-funds", new HashMap<>());

        matching.process(batch);

        assertThat(batch.trades()).isEmpty();
        assertThat(matching.books.get(Pairs.BTC_USD).side("B").topOrder()).isEmpty();
        assertThat(matching.books.get(Pairs.BTC_USD).side("S").topOrder().get().id).isEqualTo(3);
    }

    @Test
    void it_does_not_match_on_non_matching_prices() {
        BatchProcessingContext batch = BatchProcessingContext.create(4, null);
        batch.metadataRead(4, "", "", 1, false, -1, null);
        batch.configurationRead(ConfigurationHelper.defaultConfiguration(4));
        batch.orderContentRead(3, "", 1, "L", "S", TestUtils.Pairs.BTC_USD, "1", "20");
        batch.orderContentRead(5, "", 1, "L", "B", TestUtils.Pairs.BTC_USD, "1", "19");

        matching.process(batch);

        assertThat(batch.trades()).isEmpty();
        assertThat(matching.books.get(Pairs.BTC_USD).side("B").topOrder().get().id).isEqualTo(5);
        assertThat(matching.books.get(Pairs.BTC_USD).side("S").topOrder().get().id).isEqualTo(3);
    }

    @Test
    void it_does_match_on_a_partial_quantity() {
        BatchProcessingContext batch = BatchProcessingContext.create(5, null);
        batch.metadataRead(5, "", "", 1, false, -1, null);
        batch.configurationRead(ConfigurationHelper.defaultConfiguration(5));
        batch.orderContentRead(3, "", 1, "L", "S", TestUtils.Pairs.BTC_USD, "1", "20");
        batch.orderContentRead(5, "", 2, "L", "B", TestUtils.Pairs.BTC_USD, "1", "19");
        batch.orderContentRead(6, "", 3, "L", "B", TestUtils.Pairs.BTC_USD, "0.5", "25");

        matching.process(batch);

        assertThat(batch.trades()).hasSize(1);
        assertThat(batch.trades().get(0).batchId).isEqualTo(5);
        assertThat(batch.trades().get(0).timestamp).isEqualTo(3);
        assertThat(batch.trades().get(0).formatPrice()).isEqualTo("20.0000");
        assertThat(batch.trades().get(0).formatQuantity()).isEqualTo("0.50000000");
        assertThat(batch.trades().get(0).pair.base.code).isEqualTo("BTC");
        assertThat(batch.trades().get(0).pair.quote.code).isEqualTo("USD");
        assertThat(matching.books.get(Pairs.BTC_USD).side("B").topOrder().get().id).isEqualTo(5);
        assertThat(matching.books.get(Pairs.BTC_USD).side("S").topOrder().get().id).isEqualTo(3);
        assertThat(matching.books.get(Pairs.BTC_USD).side("S").topOrder().get().formatRemainingQuantity()).isEqualTo("0.50000000");
    }

    @Test
    void a_filled_order_leaves_orderbook() {
        BatchProcessingContext batch = BatchProcessingContext.create(6, null);
        batch.metadataRead(6, "", "", 1, false, -1, null);
        batch.configurationRead(ConfigurationHelper.defaultConfiguration(6));
        batch.orderContentRead(3, "", 1, "L", "S", TestUtils.Pairs.BTC_USD, "0.5", "20");
        batch.orderContentRead(5, "", 1, "L", "B", TestUtils.Pairs.BTC_USD, "1", "19");
        batch.orderContentRead(7, "", 1, "L", "B", TestUtils.Pairs.BTC_USD, "1", "22");

        matching.process(batch);

        assertThat(batch.trades()).hasSize(1);
        assertThat(batch.trades().get(0).batchId).isEqualTo(6);
        assertThat(batch.trades().get(0).formatPrice()).isEqualTo("20.0000");
        assertThat(batch.trades().get(0).formatQuantity()).isEqualTo("0.50000000");
        assertThat(batch.trades().get(0).pair.base.code).isEqualTo("BTC");
        assertThat(batch.trades().get(0).pair.quote.code).isEqualTo("USD");
        assertThat(matching.books.get(Pairs.BTC_USD).side("B").topOrder().get().id).isEqualTo(7);
        assertThat(matching.books.get(Pairs.BTC_USD).side("B").topOrder().get().formatRemainingQuantity()).isEqualTo("0.50000000");
        assertThat(matching.books.get(Pairs.BTC_USD).side("S").topOrder()).isEmpty();
    }

    @Test
    void it_does_not_match_with_another_book() {
        BatchProcessingContext batch = BatchProcessingContext.create(7, null);
        batch.metadataRead(7, "", "", 1, false, -1, null);
        batch.configurationRead(ConfigurationHelper.defaultConfiguration(7));
        batch.orderContentRead(7, "", 1, "L", "B", TestUtils.Pairs.BTC_USD, "1", "22");
        batch.orderContentRead(8, "", 1, "L", "S", TestUtils.Pairs.tLGO, "1", "22");

        matching.process(batch);

        assertThat(batch.trades()).hasSize(0);
        assertThat(matching.books.get(Pairs.BTC_USD).side("S").topOrder()).isEmpty();
        assertThat(matching.books.get(Pairs.BTC_USD).side("B").topOrder().get().id).isEqualTo(7);
        assertThat(matching.books.get(Pairs.tLGO).side("B").topOrder()).isEmpty();
        assertThat(matching.books.get(Pairs.tLGO).side("S").topOrder().get().id).isEqualTo(8);
    }

    @Test
    void a_market_order_does_not_enter_the_book() {
        BatchProcessingContext batch = BatchProcessingContext.create(8, null);
        batch.metadataRead(8, "", "", 1, false, -1, null);
        batch.configurationRead(ConfigurationHelper.defaultConfiguration(8));
        batch.orderContentRead(9, "", 1, "M", "B", TestUtils.Pairs.BTC_USD, "100", null);

        matching.process(batch);

        assertThat(matching.books.get(Pairs.BTC_USD).side("S").topOrder()).isEmpty();
        assertThat(matching.books.get(Pairs.BTC_USD).side("B").topOrder()).isEmpty();
    }

    @Test
    void a_market_sell_order_matches_at_market_price() {
        BatchProcessingContext batch = BatchProcessingContext.create(9, null);
        batch.metadataRead(9, "", "", 1, false, -1, null);
        batch.configurationRead(ConfigurationHelper.defaultConfiguration(9));
        batch.orderContentRead(5, "", 1, "L", "B", TestUtils.Pairs.BTC_USD, "1", "19");
        batch.orderContentRead(7, "", 1, "L", "B", TestUtils.Pairs.BTC_USD, "0.5", "22");
        batch.orderContentRead(10, "", 1, "M", "S", TestUtils.Pairs.BTC_USD, "0.6", null);

        matching.process(batch);

        assertThat(batch.trades()).hasSize(2);
        assertThat(batch.trades().get(0).batchId).isEqualTo(9);
        assertThat(batch.trades().get(0).formatPrice()).isEqualTo("22.0000");
        assertThat(batch.trades().get(0).formatQuantity()).isEqualTo("0.50000000");
        assertThat(batch.trades().get(0).pair.base.code).isEqualTo("BTC");
        assertThat(batch.trades().get(0).pair.quote.code).isEqualTo("USD");
        assertThat(batch.trades().get(1).batchId).isEqualTo(9);
        assertThat(batch.trades().get(1).formatPrice()).isEqualTo("19.0000");
        assertThat(batch.trades().get(1).formatQuantity()).isEqualTo("0.10000000");
        assertThat(batch.trades().get(1).pair.base.code).isEqualTo("BTC");
        assertThat(batch.trades().get(1).pair.quote.code).isEqualTo("USD");
        assertThat(matching.books.get(Pairs.BTC_USD).side("S").topOrder()).isEmpty();
        assertThat(matching.books.get(Pairs.BTC_USD).side("B").topOrder().get().id).isEqualTo(5);
        assertThat(matching.books.get(Pairs.BTC_USD).side("B").topOrder().get().formatRemainingQuantity()).isEqualTo("0.90000000");
    }

    @Test
    void an_order_leaves_orderbook_when_no_sufficient_remaining() {
        BatchProcessingContext batch = BatchProcessingContext.create(10, null);
        batch.metadataRead(10, "", "", 1, false, -1, null);
        batch.configurationRead(ConfigurationHelper.defaultConfiguration(10));
        batch.orderContentRead(5, "", 1, "L", "B", TestUtils.Pairs.BTC_USD, "1", "19");
        batch.orderContentRead(11, "", 1, "M", "S", TestUtils.Pairs.BTC_USD, "0.5", null);

        matching.process(batch);

        assertThat(batch.trades()).hasSize(1);
        assertThat(batch.trades().get(0).batchId).isEqualTo(10);
        assertThat(batch.trades().get(0).formatPrice()).isEqualTo("19.0000");
        assertThat(batch.trades().get(0).formatQuantity()).isEqualTo("0.50000000");
        assertThat(batch.trades().get(0).pair.base.code).isEqualTo("BTC");
        assertThat(batch.trades().get(0).pair.quote.code).isEqualTo("USD");
        assertThat(matching.books.get(Pairs.BTC_USD).side("S").topOrder()).isEmpty();
        assertThat(matching.books.get(Pairs.BTC_USD).side("B").topOrder()).isEmpty();
    }

    @Test
    void an_order_with_insufficient_remaining_does_not_enter_orderbook() {
        BatchProcessingContext batch = BatchProcessingContext.create(11, null);
        batch.metadataRead(11, "", "", 1, false, -1, null);
        batch.configurationRead(ConfigurationHelper.defaultConfiguration(11));
        batch.orderContentRead(12, "", 1, "L", "B", TestUtils.Pairs.BTC_USD, "10", "10");
        batch.orderContentRead(13, "", 1, "L", "S", TestUtils.Pairs.BTC_USD, "10.2", "10");

        matching.process(batch);

        assertThat(batch.trades()).hasSize(1);
        assertThat(batch.trades().get(0).batchId).isEqualTo(11);
        assertThat(batch.trades().get(0).formatPrice()).isEqualTo("10.0000");
        assertThat(batch.trades().get(0).formatQuantity()).isEqualTo("10.00000000");
        assertThat(batch.trades().get(0).pair.base.code).isEqualTo("BTC");
        assertThat(batch.trades().get(0).pair.quote.code).isEqualTo("USD");
        assertThat(matching.books.get(Pairs.BTC_USD).side("S").topOrder()).isEmpty();
        assertThat(matching.books.get(Pairs.BTC_USD).side("B").topOrder()).isEmpty();
    }

    @Test
    void a_market_buy_order_matches() {
        BatchProcessingContext batch = BatchProcessingContext.create(12, null);
        batch.metadataRead(12, "", "", 1, false, -1, null);
        batch.configurationRead(ConfigurationHelper.defaultConfiguration(12));
        batch.orderContentRead(14, "", 1, "L", "S", TestUtils.Pairs.BTC_USD, "10", "100");
        batch.orderContentRead(15, "", 1, "M", "B", TestUtils.Pairs.BTC_USD, "500", null);

        matching.process(batch);

        assertThat(batch.trades()).hasSize(1);
        assertThat(batch.trades().get(0).batchId).isEqualTo(12);
        assertThat(batch.trades().get(0).formatPrice()).isEqualTo("100.0000");
        assertThat(batch.trades().get(0).formatQuantity()).isEqualTo("5.00000000");
        assertThat(batch.trades().get(0).pair.base.code).isEqualTo("BTC");
        assertThat(batch.trades().get(0).pair.quote.code).isEqualTo("USD");
        assertThat(matching.books.get(Pairs.BTC_USD).side("S").topOrder().get().id).isEqualTo(14);
        assertThat(matching.books.get(Pairs.BTC_USD).side("S").topOrder().get().formatRemainingQuantity()).isEqualTo("5.00000000");
        assertThat(matching.books.get(Pairs.BTC_USD).side("B").topOrder()).isEmpty();
    }

    @Test
    void it_cancels_a_batch_order_before_matching() {
        BatchProcessingContext batch = BatchProcessingContext.create(13, null);
        batch.metadataRead(13, "", "", 1, false, -1, null);
        batch.configurationRead(ConfigurationHelper.defaultConfiguration(13));
        batch.orderContentRead(14, "", 1, "L", "S", TestUtils.Pairs.BTC_USD, "5", "100");
        batch.orderContentRead(16, "", 1, "L", "B", TestUtils.Pairs.BTC_USD, "10", "100");
        batch.cancelOrderContentRead(17, "", 1, 16);

        matching.process(batch);

        assertThat(batch.trades()).hasSize(0);
        assertThat(matching.books.get(Pairs.BTC_USD).side("S").topOrder().get().id).isEqualTo(14);
        assertThat(matching.books.get(Pairs.BTC_USD).side("B").topOrder()).isEmpty();
    }

    @Test
    void it_cancels_all_order_books() {
        BatchProcessingContext batch = BatchProcessingContext.create(13, null);
        batch.metadataRead(13, "", "", 1, false, -1, null);
        batch.configurationRead(ConfigurationHelper.defaultConfiguration(13));
        batch.orderContentRead(14, "", 1, "L", "S", TestUtils.Pairs.tLGO, "5", "100");
        batch.orderContentRead(16, "", 1, "L", "B", TestUtils.Pairs.BTC_USD, "10", "100");
        matching.process(batch);
        BatchProcessingContext batch2 = BatchProcessingContext.create(14, null);
        batch2.metadataRead(14, "", "", 1, true, -1, null);
        batch2.configurationRead(ConfigurationHelper.defaultConfiguration(14));

        matching.process(batch2);

        assertThat(batch2.trades()).hasSize(0);
        assertThat(matching.books).hasSize(0);
    }

    @Test
    void it_cancels_resting_order_on_STP() {
        BatchProcessingContext batch = BatchProcessingContext.create(15, null);
        batch.metadataRead(15, "", "", 1, false, -1, null);
        batch.configurationRead(ConfigurationHelper.defaultConfiguration(15));
        batch.orderContentRead(18, "", 1, "L", "B", TestUtils.Pairs.BTC_USD, "10", "100");
        batch.orderContentRead(19, "", 1, "L", "S", TestUtils.Pairs.BTC_USD, "10", "100");
        batch.failedOrderRead(18, "OrderCanceledBySTP", Map.of("counterOrderId", "19"));
        matching.process(batch);

        assertThat(batch.trades()).hasSize(0);
        assertThat(matching.books.get(Pairs.BTC_USD).side("S").topOrder().get().id).isEqualTo(19);
        assertThat(matching.books.get(Pairs.BTC_USD).side("S").topOrder().get().formatRemainingQuantity()).isEqualTo("10.00000000");
        assertThat(matching.books.get(Pairs.BTC_USD).side("B").topOrder()).isEmpty();
    }

    @Test
    void it_saves_orderbooks_state_at_the_end_of_the_batch_processing() {
        BatchProcessingContext batch = BatchProcessingContext.create(1300, null);
        batch.metadataRead(1300, "", "", 1, false, -1, null);
        batch.configurationRead(ConfigurationHelper.defaultConfiguration(1300));
        batch.orderContentRead(14, "", 1, "L", "S", TestUtils.Pairs.BTC_USD, "5", "1000");
        batch.orderContentRead(16, "", 1, "L", "B", TestUtils.Pairs.BTC_USD, "10", "100");
        batch.orderContentRead(17, "", 1, "L", "B", TestUtils.Pairs.BTC_USD, "12", "100");
        batch.orderContentRead(18, "", 1, "L", "B", TestUtils.Pairs.BTC_USD, "12", "102");

        matching.process(batch);

        assertThat(batch.orderBookSerializedContent()).isEqualTo("{\"BTC-USD\":{" +
                "\"B\":{\"1020000\":[{\"id\":18,\"qt\":1200000000}],\"1000000\":[{\"id\":16,\"qt\":1000000000},{\"id\":17,\"qt\":1200000000}]}," +
                "\"S\":{\"10000000\":[{\"id\":14,\"qt\":500000000}]}}}");
    }

    @Test
    void it_saves_events() {
        BatchProcessingContext batch = BatchProcessingContext.create(13, null);
        batch.metadataRead(13, "", "", 1, false, -1, null);
        batch.configurationRead(ConfigurationHelper.defaultConfiguration(13));
        batch.orderContentRead(14, "", 1, "L", "S", TestUtils.Pairs.BTC_USD, "5", "1000");
        batch.orderContentRead(16, "", 1, "L", "B", TestUtils.Pairs.BTC_USD, "10", "100");
        batch.orderContentRead(17, "", 1, "L", "B", TestUtils.Pairs.BTC_USD, "12", "100");
        batch.orderContentRead(18, "", 1, "L", "B", TestUtils.Pairs.BTC_USD, "12", "102");
        batch.orderContentRead(19, "", 1, "L", "S", TestUtils.Pairs.BTC_USD, "10", "102");
        batch.orderContentRead(20, "", 1, "L", "S", TestUtils.Pairs.BTC_USD, "10", "102");
        batch.orderContentRead(21, "", 1, "L", "S", TestUtils.Pairs.BTC_USD, "10", "102");
        BatchProcessingContext batch2 = BatchProcessingContext.create(14, null);
        batch2.metadataRead(14, "", "", 0, false, -1, null);
        batch2.configurationRead(ConfigurationHelper.defaultConfiguration(14));
        batch2.cancelOrderContentRead(22, "", 2, 20);
        batch2.cancelOrderContentRead(23, "", 2, 21);
        BatchProcessingContext batch3 = BatchProcessingContext.create(15, null);
        batch3.metadataRead(15, "", "", 0, true, -1, null);
        batch3.configurationRead(ConfigurationHelper.defaultConfiguration(15));

        matching.process(batch);
        matching.process(batch2);
        matching.process(batch3);

        assertThat(batch.orderBookEvents()).hasSize(8);
        assertThat(batch.orderBookEvents().get(0)).isEqualToComparingFieldByField(new OrderBookUpdateOrderEvent(13, "BTC", "USD", "S", 10000000, 14, Currencies.BTC.parse("5")));
        assertThat(batch.orderBookEvents().get(1)).isEqualToComparingFieldByField(new OrderBookUpdateOrderEvent(13, "BTC", "USD", "B", 1000000, 16, Currencies.BTC.parse("10")));
        assertThat(batch.orderBookEvents().get(2)).isEqualToComparingFieldByField(new OrderBookUpdateOrderEvent(13, "BTC", "USD", "B", 1000000, 17, Currencies.BTC.parse("12")));
        assertThat(batch.orderBookEvents().get(3)).isEqualToComparingFieldByField(new OrderBookUpdateOrderEvent(13, "BTC", "USD", "B", 1020000, 18, Currencies.BTC.parse("12")));
        assertThat(batch.orderBookEvents().get(4)).isEqualToComparingFieldByField(new OrderBookUpdateOrderEvent(13, "BTC", "USD", "B", 1020000, 18, Currencies.BTC.parse("2")));
        assertThat(batch.orderBookEvents().get(5)).isEqualToComparingFieldByField(new OrderBookRemoveOrderEvent(13, "BTC", "USD", "B", 1020000, 18));
        assertThat(batch.orderBookEvents().get(6)).isEqualToComparingFieldByField(new OrderBookUpdateOrderEvent(13, "BTC", "USD", "S", 1020000, 20, Currencies.BTC.parse("8")));
        assertThat(batch.orderBookEvents().get(7)).isEqualToComparingFieldByField(new OrderBookUpdateOrderEvent(13, "BTC", "USD", "S", 1020000, 21, Currencies.BTC.parse("10")));
        assertThat(batch2.orderBookEvents()).hasSize(2);
        assertThat(batch2.orderBookEvents().get(0)).isEqualToComparingFieldByField(new OrderBookRemoveOrderEvent(14, "BTC", "USD", "S", 1020000, 20));
        assertThat(batch2.orderBookEvents().get(1)).isEqualToComparingFieldByField(new OrderBookRemoveOrderEvent(14, "BTC", "USD", "S", 1020000, 21));
        assertThat(batch3.orderBookEvents()).hasSize(1);
        assertThat(batch3.orderBookEvents().get(0)).isEqualToComparingFieldByField(new OrderBookCancelOrdersEvent(15, "BTC", "USD"));
    }

    @Test
    void it_restores_books_() throws IOException {
        when(repository.load(eq(155386706230300001L), any())).thenReturn(order(155386706230300001L, "B", 18300000, 40770000));
        List<OrderBookEvent> events = Collections.singletonList(new OrderBookRemoveOrderEvent(1399, "BTC", "USD", "B", 40770000, 155386706230300001L));
        matching.replay(new Metadata(1, "", "", 0, false, -1, null), "{\"BTC-USD\":{\"B\":{\"40770000\":[{\"id\":155386706230300001,\"qt\":18300000}]},\"S\":{}}}", events);
        BatchProcessingContext batch = BatchProcessingContext.create(1400, null);
        batch.metadataRead(1400, "", "", 0, false, -1, null);
        batch.configurationRead(ConfigurationHelper.defaultConfiguration(1400));

        matching.process(batch);

        assertThat(batch.orderBookSerializedContent()).doesNotContain("155386706230300001");
    }

    @Test
    void it_restores_books() throws IOException {
        var platformConfiguration = ConfigurationHelper.defaultConfiguration(1);
        BatchProcessingContext batch = BatchProcessingContext.create(13, null);
        batch.metadataRead(13, "", "", 1, false, -1, null);
        batch.configurationRead(platformConfiguration);
        batch.orderContentRead(14, "", 1, "L", "S", TestUtils.Pairs.BTC_USD, "5", "1000");
        batch.orderContentRead(16, "", 1, "L", "B", TestUtils.Pairs.BTC_USD, "10", "100");
        batch.orderContentRead(17, "", 1, "L", "B", TestUtils.Pairs.BTC_USD, "12", "100");
        batch.orderContentRead(18, "", 1, "L", "B", TestUtils.Pairs.BTC_USD, "12", "102");
        batch.orderContentRead(19, "", 1, "L", "S", TestUtils.Pairs.BTC_USD, "10", "102");
        batch.orderContentRead(20, "", 1, "L", "S", TestUtils.Pairs.BTC_USD, "10", "102");
        when(repository.load(14, platformConfiguration)).thenReturn(order(14, "S", 500000000, 10000000));
        when(repository.load(16, platformConfiguration)).thenReturn(order(16, "B", 1000000000, 1000000));
        when(repository.load(17, platformConfiguration)).thenReturn(order(17, "B", 1200000000, 1000000));
        when(repository.load(18, platformConfiguration)).thenReturn(order(18, "B", 1200000000, 1020000));
        when(repository.load(19, platformConfiguration)).thenReturn(order(19, "S", 1000000000, 1020000));
        when(repository.load(20, platformConfiguration)).thenReturn(order(20, "S", 1000000000, 1020000));
        List<OrderBookEvent> events = List.of(
                new OrderBookAddOrderEvent(13, "BTC", "USD", "S", 10000000, 14, 500000000),
                new OrderBookAddOrderEvent(13, "BTC", "USD", "B", 1000000, 16, 1000000000),
                new OrderBookAddOrderEvent(13, "btc", "usd", "B", 1000000, 17, 1200000000),
                new OrderBookAddOrderEvent(13, "BTC", "USD", "B", 1020000, 18, 1200000000),
                new OrderBookUpdateOrderEvent(13, "btc", "USD", "B", 1020000, 18, Currencies.BTC.parse("2")),
                new OrderBookRemoveOrderEvent(13, "BTC", "usd", "B", 1020000, 18),
                new OrderBookAddOrderEvent(13, "BTC", "USD", "S", 1020000, 20, 800000000));
        BatchProcessingContext batch2 = BatchProcessingContext.create(14, null);
        batch2.metadataRead(14, "", "", 0, false, -1, null);
        batch2.configurationRead(platformConfiguration);
        matching.process(batch);
        String originalSerializedContent = matching.serializeOrderBooks();
        System.out.println(originalSerializedContent);

        matching.replay(new Metadata(13, "", "", 0, false, -1, null), "{}", events);

        matching.process(batch2);
        assertThat(matching.serializeOrderBooks()).isEqualTo(originalSerializedContent);
    }

    @Test
    void orderbooks_are_case_insensitive() throws IOException {
        var platformConfiguration = ConfigurationHelper.defaultConfiguration(0);
        BatchProcessingContext batch = BatchProcessingContext.create(0, null);
        when(repository.load(23, platformConfiguration)).thenReturn(order(23, "B", 18300000, 40770000));
        matching.replay(new Metadata(1, "", "", 0, false, -1, null), "{\"tLGO1-TLGO2\":{\"B\":{\"40770000\":[{\"id\":23,\"qt\":18300000}]},\"S\":{}}}", List.of());
        batch.metadataRead(0, "", "", 1, false, -1, null);
        batch.configurationRead(platformConfiguration);
        batch.orderContentRead(1, "", 1, "L", "B", TestUtils.Pairs.tLGO, "1", "10");

        matching.process(batch);

        assertThat(batch.trades()).isEmpty();
        assertThat(matching.books).hasSize(1);
        assertThat(matching.books.get(Pairs.tLGO).side("B").getContent().values()).hasSize(2);
        assertThat(matching.books.get(Pairs.tLGO).side("B").topOrder().get().id).isEqualTo(23);
        assertThat(matching.books.get(new Pair(new Currency("tlgo1", 8), new Currency("tlgo2", 4))).side("B").topOrder().get().id).isEqualTo(23);
        assertThat(matching.books.get(new Pair(new Currency("TLGO1", 8), new Currency("TLGO2", 4))).side("B").topOrder().get().id).isEqualTo(23);
        assertThat(matching.books.get(new Pair(new Currency("tLGO1", 8), new Currency("tLGO2", 4))).side("B").topOrder().get().id).isEqualTo(23);
        matching.books.get(Pairs.tLGO).side("B").removeTopOrder();
        assertThat(matching.books.get(Pairs.tLGO).side("B").topOrder().get().id).isEqualTo(1);
        assertThat(matching.books.get(Pairs.tLGO).side("S").topOrder()).isEmpty();
    }

    private Order order(long id, String direction, long quantity, long price) {
        return new Order(id, 1, "", 1, "L", direction, TestUtils.Pairs.BTC_USD, Currencies.BTC.format(quantity), Currencies.USD.format(price));
    }

    private MatchingStep matching;
    private OrderRepository repository;
}