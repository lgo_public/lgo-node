package lgonode.infrastructure.avro;

import lgonode.domain.*;
import lgonode.infrastructure.BatchProcessingContext;
import org.junit.jupiter.api.*;

import java.io.*;
import java.nio.file.Path;
import java.util.List;

import static org.assertj.core.api.Assertions.*;

class BatchFilesReadingStepTest {

    @BeforeEach
    void setUp() {
        Path path = new File(getClass().getClassLoader().getResource("files/batch").getFile()).toPath();
        batchFilesReader = new BatchFilesReadingStep(path.toString());
        context = BatchProcessingContext.create("0000000000", "lastBatchHash");
    }

    @Test
    void it_reads_new_order_format() throws IOException {
        context = BatchProcessingContext.create("0000000034", "9ab403c91bfe0af3b40bf6ecdad466f1fa1a24be8c6d857a5cf21df7e22fb052");
        batchFilesReader.readOrders(context);

        List<RawOrder> orders = context.rawOrders();
        assertThat(orders).hasSize(19);
        assertThat(orders.get(0).id).isEqualTo(156715853297600001L);
        assertThat(orders.get(0).batchId).isEqualTo(34);
        assertThat(orders.get(0).isEncrypted()).isTrue();
        assertThat(((EncryptedOrder) orders.get(0)).keyId).isEqualTo("99d02355-e687-4dfc-a7c2-429b5bb55a2d");
        assertThat(orders.get(1).id).isEqualTo(156715853298200001L);
        assertThat(orders.get(1).batchId).isEqualTo(34);
        assertThat(orders.get(1).isEncrypted()).isFalse();
    }

    @Test
    void it_reads_old_order_format() throws IOException {
        batchFilesReader.readOrders(context);

        List<RawOrder> orders = context.rawOrders();
        assertThat(orders).hasSize(1);
        assertThat(orders.get(0).id).isEqualTo(155075557546300001L);
        assertThat(orders.get(0).batchId).isEqualTo(0);
        assertThat(((EncryptedOrder) orders.get(0)).keyId).isEqualTo("465beb3d-5b67-4051-90e2-b0a54caa6de1");
    }

    @Test
    void reads_failed_orders() throws IOException {
        batchFilesReader.readFailedOrders(context);

        List<FailedOrder> failedOrders = context.failedOrders();
        assertThat(failedOrders).hasSize(1);
        assertThat(failedOrders.get(0).id).isEqualTo(155075557546300001L);
        assertThat(failedOrders.get(0).batchId).isEqualTo(0);
        assertThat(failedOrders.get(0).reason).isEqualTo("invalid-key");
        assertThat(failedOrders.get(0).details).isEmpty();
    }

    @Test
    void reads_metadata() throws IOException {
        batchFilesReader.readMetadata(context);

        Metadata metadata = context.metadata();
        assertThat(metadata.id).isEqualTo(0);
        assertThat(metadata.hash).isEqualTo("e59ad0e36fd3896badd9a7534ec3c9889220a1cb6f0014f4a72f2e05ad897b11");
        assertThat(metadata.previousHash).isEmpty();
        assertThat(metadata.orderCount).isEqualTo(1);
        assertThat(metadata.cancelOrderBooks).isFalse();
    }

    @Test
    void reads_metadata_with_cancel_order_book() throws IOException {
        Path path = new File(getClass().getClassLoader().getResource("files/batch").getFile()).toPath();
        BatchFilesReadingStep batchFilesReader = new BatchFilesReadingStep(path.toString());
        BatchProcessingContext context = BatchProcessingContext.create("0002840150", "lastBatchHash");

        batchFilesReader.readMetadata(context);

        Metadata metadata = context.metadata();
        assertThat(metadata.id).isEqualTo(2840150);
        assertThat(metadata.cancelOrderBooks).isTrue();
    }

    @Test
    void reads_metadata_with_configuration() throws IOException {
        Path path = new File(getClass().getClassLoader().getResource("files/batch").getFile()).toPath();
        BatchFilesReadingStep batchFilesReader = new BatchFilesReadingStep(path.toString());
        BatchProcessingContext context = BatchProcessingContext.create("0000000004", "lastBatchHash");

        batchFilesReader.readMetadata(context);

        Metadata metadata = context.metadata();
        assertThat(metadata.id).isEqualTo(4);
        assertThat(metadata.hash).isEqualTo("plop");
        assertThat(metadata.previousHash).isEqualTo("plop2");
        assertThat(metadata.orderCount).isEqualTo(1);
        assertThat(metadata.cancelOrderBooks).isFalse();
        assertThat(metadata.configVersion).isEqualTo(24);
        assertThat(metadata.configHash).isEqualTo("blah24");
    }

    @Test
    void reads_metadata_with_configuration_and_cancel_order_books() throws IOException {
        Path path = new File(getClass().getClassLoader().getResource("files/batch").getFile()).toPath();
        BatchFilesReadingStep batchFilesReader = new BatchFilesReadingStep(path.toString());
        BatchProcessingContext context = BatchProcessingContext.create("0000000005", "lastBatchHash");

        batchFilesReader.readMetadata(context);

        Metadata metadata = context.metadata();
        assertThat(metadata.id).isEqualTo(5);
        assertThat(metadata.hash).isEqualTo("plop");
        assertThat(metadata.previousHash).isEqualTo("plop2");
        assertThat(metadata.orderCount).isEqualTo(0);
        assertThat(metadata.cancelOrderBooks).isTrue();
        assertThat(metadata.configVersion).isEqualTo(25);
        assertThat(metadata.configHash).isEqualTo("blah25");
    }

    @Test
    void reads_ots_upgraded_proof() throws IOException {
        batchFilesReader.readUpgradedOtsProof(context, new Metadata(1L, "hash", "prev", 1, false, -1, null));

        Proof proof = context.otsProof();
        assertThat(proof).isNotNull();
        assertThat(proof.batchId).isEqualTo(0);
        assertThat(proof.timestampedHash).isEqualTo("d04b98f48e8f8bcc15c6ae5ac050801cd6dcfd428fb5f9e65c4e16e7807340fa");
        assertThat(proof.content).isNotEmpty();
    }

    private BatchFilesReadingStep batchFilesReader;
    private BatchProcessingContext context;
}
