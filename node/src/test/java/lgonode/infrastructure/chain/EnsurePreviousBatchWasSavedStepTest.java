package lgonode.infrastructure.chain;

import lgonode.domain.*;
import lgonode.infrastructure.*;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

class EnsurePreviousBatchWasSavedStepTest {

    @Test
    void should_fail_if_previous_batch_was_not_saved() {
        BatchProcessingContext context = BatchProcessingContext.create(22, "hash22");
        BatchRepository mock = mock(BatchRepository.class);
        when(mock.lastBatch()).thenReturn(new Metadata(20, "hash20", "hash19", 3, false, -1, null));
        EnsurePreviousBatchWasSavedStep step = new EnsurePreviousBatchWasSavedStep(mock);

        assertThat(step.process(context)).isEqualTo(ProcessingResult.FAILURE);
    }

    @Test
    void should_succeed_if_last_batch_was_saved() {
        BatchProcessingContext context = BatchProcessingContext.create(21, "hash21");
        BatchRepository mock = mock(BatchRepository.class);
        when(mock.lastBatch()).thenReturn(new Metadata(20, "hash20", "hash19", 3, false, -1, null));
        EnsurePreviousBatchWasSavedStep step = new EnsurePreviousBatchWasSavedStep(mock);

        assertThat(step.process(context)).isEqualTo(ProcessingResult.CONTINUE);
    }

    @Test
    void does_continue_for_batch_0() {
        BatchProcessingContext context = BatchProcessingContext.create(0, null);
        BatchRepository mock = mock(BatchRepository.class);
        when(mock.lastBatch()).thenReturn(null);
        EnsurePreviousBatchWasSavedStep step = new EnsurePreviousBatchWasSavedStep(mock);

        assertThat(step.process(context)).isEqualTo(ProcessingResult.CONTINUE);
    }

    @Test
    void should_fail_if_no_batch_saved_and_id_not_0() {
        BatchProcessingContext context = BatchProcessingContext.create(22, "hash22");
        BatchRepository mock = mock(BatchRepository.class);
        when(mock.lastBatch()).thenReturn(null);
        EnsurePreviousBatchWasSavedStep step = new EnsurePreviousBatchWasSavedStep(mock);

        assertThat(step.process(context)).isEqualTo(ProcessingResult.FAILURE);
    }
}