package lgonode;

import lgonode.domain.Order;
import lgonode.domain.*;
import lgonode.domain.configuration.*;
import lgonode.infrastructure.BatchProcessingContext;
import lgonode.infrastructure.avro.BatchFilesReadingStep;
import lgonode.infrastructure.chain.*;
import lgonode.infrastructure.decipher.*;
import lgonode.infrastructure.gcs.*;
import lgonode.infrastructure.matching.MatchingStep;
import lgonode.infrastructure.persistence.*;
import lgonode.infrastructure.persistence.dao.BatchDAO;
import lgonode.infrastructure.persistence.repository.*;
import lgonode.infrastructure.reader.OrderContentReadingStep;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.RegisterExtension;

import java.io.*;
import java.nio.file.Path;
import java.security.Security;
import java.util.*;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

@Disabled
public class Batch1562879ReaderNotATest {

    @RegisterExtension
    public WithPostgres postgres = WithPostgres.onSchemaAndMigration("test", "lgonode");
    private BatchHashValidationStep batchHashValidationStep;
    private BatchChainingValidationStep chainingValidationStep;
    private BatchPersistenceStep batchPersistenceStep;

    @AfterEach
    void tearDown() {
        postgres.dsl().dropTable(BatchDAO.TABLE);
    }

    @BeforeEach
    void setUp() throws IOException {
        Security.addProvider(new BouncyCastleProvider());

        Path pathBatch = new File(Objects.requireNonNull(getClass().getClassLoader().getResource("files/batch")).getFile()).toPath();
        Path pathKey = new File(Objects.requireNonNull(getClass().getClassLoader().getResource("files/key")).getFile()).toPath();

        GCSKeyBucketReader gcsKeyBucketReader = mock(GCSKeyBucketReader.class);
        FileReader fileReader1 = new FileReader(pathKey.resolve("df54480d-8c33-488b-abb1-61f682a42025_private.pem").toFile());
        when(gcsKeyBucketReader.loadPrivateKey("df54480d-8c33-488b-abb1-61f682a42025")).thenReturn(Optional.of(fileReader1));
        FileReader fileReader2 = new FileReader(pathKey.resolve("31a0db0a-f471-4d69-afac-03fabbacca34_private.pem").toFile());
        when(gcsKeyBucketReader.loadPrivateKey("31a0db0a-f471-4d69-afac-03fabbacca34")).thenReturn(Optional.of(fileReader2));

        context = BatchProcessingContext.create("0001562879", "e76129ebe3be4ced1819f80ac100a8413f91edcbe0285ff584a7e168c1a56289");
        batchFilesReader = new BatchFilesReadingStep(pathBatch.toString());
        batchHashValidationStep = new BatchHashValidationStep();
        loadBatchConfiguration = new LoadBatchConfigurationStep((configVersion, configHash) -> ConfigurationHelper.defaultConfiguration(1562879));
        orderDecipherer = new OrderDecryptionStep(new KeyRetriever(gcsKeyBucketReader));
        orderReader = new OrderContentReadingStep();
        OrderRepository repository = mock(OrderRepository.class);
        var configuration = ConfigurationHelper.defaultConfiguration(1562879);
        when(repository.load(155768265169100001L, configuration)).thenReturn(order(155768265169100001L, "B", 467200000, 68834000));
        when(repository.load(155768265485100001L, configuration)).thenReturn(order(155768265485100001L, "B", 1212600000, 68824000));
        when(repository.load(155768265172500001L, configuration)).thenReturn(order(155768265172500001L, "B", 409600000, 68810000));
        when(repository.load(155768264374100001L, configuration)).thenReturn(order(155768264374100001L, "B", 1213600000, 68807000));
        when(repository.load(155768265675500001L, configuration)).thenReturn(order(155768265675500001L, "B", 1391600000, 68797000));
        when(repository.load(155768264114100001L, configuration)).thenReturn(order(155768264114100001L, "B", 511300000, 68752000));
        when(repository.load(155768263059300001L, configuration)).thenReturn(order(155768263059300001L, "B", 5300000, 68725000));
        when(repository.load(155768262184100001L, configuration)).thenReturn(order(155768262184100001L, "B", 1178100000, 68676000));
        when(repository.load(155768261675000001L, configuration)).thenReturn(order(155768261675000001L, "B", 1929100000, 68674000));
        when(repository.load(155768260941800001L, configuration)).thenReturn(order(155768260941800001L, "B", 591400000, 68657000));
        when(repository.load(155768260186900001L, configuration)).thenReturn(order(155768260186900001L, "B", 2701800000L, 68486000));
        when(repository.load(155768265181600001L, configuration)).thenReturn(order(155768265181600001L, "S", 1178800000, 68875000));
        when(repository.load(155768264185000001L, configuration)).thenReturn(order(155768264185000001L, "S", 2984300000L, 68917000));
        when(repository.load(155768264185700001L, configuration)).thenReturn(order(155768264185700001L, "S", 2686300000L, 68965000));
        when(repository.load(155768265184600001L, configuration)).thenReturn(order(155768265184600001L, "S", 2359900000L, 69019000));
        when(repository.load(155768264710400001L, configuration)).thenReturn(order(155768264710400001L, "S", 415100000, 69103000));
        
        matchingStep = new MatchingStep(new OrderBooksSerializer(), new OrderBooksDeserializer(repository), repository, mock(ConfigurationLoader.class));
        matchingStep.replay(new Metadata(1562878, "", "", 0, false, -1, null), "{\"BTC-USD\":{" +
                "\"B\":{" +
                "\"68834000\":[{\"id\":155768265169100001,\"qt\":467200000}]," +
                "\"68824000\":[{\"id\":155768265485100001,\"qt\":1212600000}]," +
                "\"68810000\":[{\"id\":155768265172500001,\"qt\":409600000}]," +
                "\"68807000\":[{\"id\":155768264374100001,\"qt\":1213600000}]," +
                "\"68797000\":[{\"id\":155768265675500001,\"qt\":1391600000}]," +
                "\"68752000\":[{\"id\":155768264114100001,\"qt\":511300000}]," +
                "\"68725000\":[{\"id\":155768263059300001,\"qt\":5300000}]," +
                "\"68676000\":[{\"id\":155768262184100001,\"qt\":1178100000}]," +
                "\"68674000\":[{\"id\":155768261675000001,\"qt\":1929100000}]," +
                "\"68657000\":[{\"id\":155768260941800001,\"qt\":591400000}]," +
                "\"68486000\":[{\"id\":155768260186900001,\"qt\":2701800000}]}," +
                "\"S\":{" +
                "\"68875000\":[{\"id\":155768265181600001,\"qt\":1178800000}]," +
                "\"68917000\":[{\"id\":155768264185000001,\"qt\":2984300000}]," +
                "\"68965000\":[{\"id\":155768264185700001,\"qt\":2686300000}]," +
                "\"69019000\":[{\"id\":155768265184600001,\"qt\":2359900000}]," +
                "\"69103000\":[{\"id\":155768264710400001,\"qt\":415100000}]}}}", List.of());

        batchPersistenceStep = new BatchPersistenceStep(new BatchJooqRepository(postgres.dsl()), new OrderJooqRepository(postgres.dsl()), new FailedOrderJooqRepository(postgres.dsl()), new CancelOrderJooqRepository(postgres.dsl()), new ProofJooqRepository(postgres.dsl()), Set.of(new TradeJooqRepository(postgres.dsl())), new OrderBooksJooqRepository(postgres.dsl()), new OrderBookEventJooqRepository(postgres.dsl()));
        chainingValidationStep = new BatchChainingValidationStep();
    }


    private Order order(long id, String direction, long quantity, long price) {
        return new Order(id, 1, "", 1, "L", direction, TestUtils.Pairs.BTC_USD, TestUtils.Currencies.BTC.format(quantity), TestUtils.Currencies.USD.format(price));
    }

    @Test
    void it_reads_batch_1562879() throws IOException {
        batchFilesReader.process(context);
        batchHashValidationStep.process(context);
        chainingValidationStep.process(context);
        loadBatchConfiguration.process(context);
        orderDecipherer.process(context);
        orderReader.process(context);
        matchingStep.process(context);
        batchPersistenceStep.process(context);
        assertThat(matchingStep.serializeOrderBooks()).doesNotContain("68807000");
        assertThat(postgres.dsl().selectCount().from(BatchDAO.TABLE).execute()).isEqualTo(1);
    }

    private BatchFilesReadingStep batchFilesReader;
    private OrderDecryptionStep orderDecipherer;
    private LoadBatchConfigurationStep loadBatchConfiguration;
    private BatchProcessingContext context;
    private OrderContentReadingStep orderReader;
    private MatchingStep matchingStep;

}
