package lgonode;

import lgonode.configuration.GCSConfiguration;
import lgonode.domain.*;
import lgonode.infrastructure.BatchProcessingContext;
import lgonode.infrastructure.avro.BatchFilesReadingStep;
import lgonode.infrastructure.decipher.*;
import lgonode.infrastructure.gcs.*;
import lgonode.infrastructure.reader.OrderContentReadingStep;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.jupiter.api.*;

import java.io.*;
import java.nio.file.Path;
import java.security.Security;
import java.util.Objects;

@Disabled
public class BatchReaderUtilNotATest {

    @BeforeEach
    void setup() {
        Security.addProvider(new BouncyCastleProvider());

        Path pathBatch = new File(Objects.requireNonNull(getClass().getClassLoader().getResource("files/batch")).getFile()).toPath();

        GCSConfiguration gcsConfiguration = new GCSConfiguration() {
            @Override
            public String batch() {
                return "lgo-markets-batches";
            }

            @Override
            public String key() {
                return "lgo-markets-keys";
            }

            @Override
            public String configuration() {
                return "lgo_platform_configuration";
            }
        };
        gcsKeyBucketReader = new GCSKeyBucketReader(gcsConfiguration);
        batchFilesReader = new BatchFilesReadingStep(pathBatch.toString());
        configurationLoader = new LoadBatchConfigurationStep(new GCSConfigurationLoader(gcsConfiguration));
        orderDecipherer = new OrderDecryptionStep(new KeyRetriever(gcsKeyBucketReader));
        orderReader = new OrderContentReadingStep();
    }

    /**
     * Put your batch files directory in src/test/resources/files/batch
     */
    @Test
    void run() throws IOException {
        analyzeBatch("0048208660");
        analyzeBatch("0048208661");
    }

    void analyzeBatch(String batchIdAsString) {
        context = BatchProcessingContext.create(batchIdAsString, "");
        batchFilesReader.process(context);
        configurationLoader.process(context);
        orderDecipherer.process(context);
        orderReader.process(context);
        printBatch();
    }

    private void printBatch() {
        System.out.println("----------------");
        System.out.println("Batch "+context.batchDirectory());
        System.out.println(printBatchStats());
        System.out.println("----------------");
        context.decryptedOrders().forEach(this::print);
        System.out.println("-");
        context.failedOrders().forEach(this::print);
        System.out.println("----------------\n");
    }

    private String printBatchStats() {
        return String.format("%s raw orders, %s failed orders, %s decrypted orders", context.rawOrders().size(), context.failedOrders().size(), context.decryptedOrders().size());
    }

    private void print(FailedOrder failedOrder) {
        System.out.println(String.format("%s failed:%s", failedOrder.id, failedOrder.reason));
    }

    private void print(DecryptedOrder order) {
        if (order.keyId.equals("N/A")) {
            System.out.print("Unencrypted");
        } else {
            System.out.print("Encrypted");
        }
        System.out.println(String.format(" order: %s\n%s\n", order.id, order.data));
    }

    private BatchFilesReadingStep batchFilesReader;
    private OrderDecryptionStep orderDecipherer;
    private BatchProcessingContext context;
    private LoadBatchConfigurationStep configurationLoader;
    private OrderContentReadingStep orderReader;
    private GCSKeyBucketReader gcsKeyBucketReader;
}
