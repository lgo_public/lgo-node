package lgonode.domain;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.*;

class CurrencyTest {

    @Test
    void it_is_case_insensitive() {
        var btc1 = new Currency("btc", 8);
        var btc2 = new Currency("BTC", 8);
        assertThat(btc1).isEqualTo(btc2);
        assertThat(btc1).isEqualTo(TestUtils.Currencies.BTC);
    }
}