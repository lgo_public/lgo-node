package lgonode.domain;

import org.junit.jupiter.api.Test;

import static java.util.Collections.*;
import static org.assertj.core.api.Assertions.*;

class OrderBookSideTest {

    @Test
    void it_should_remove_key_when_there_is_no_order_left_on_remove() {
        SellOrderBookSide side = new SellOrderBookSide();
        Order order1 = new Order(123L, 1, "keyid", 56789L, "L", "S", TestUtils.Pairs.BTC_USD, "4.35000000", "3200.00");
        side.enter(order1);

        side.removeTopOrder();

        assertThat(side.getContent().entrySet()).isEmpty();
    }

    @Test
    void it_should_remove_key_when_there_is_no_order_left_in_value_on_cancel() {
        SellOrderBookSide side = new SellOrderBookSide();
        Order order1 = new Order(123L, 1, "keyid", 56789L, "L", "S", TestUtils.Pairs.BTC_USD, "4.35000000", "3200.00");
        Order order2 = new Order(124L, 1, "keyid", 56789L, "L", "S", TestUtils.Pairs.BTC_USD, "4.35000000", "3300.00");
        side.enter(order1);
        side.enter(order2);

        side.cancel(singletonList(123L));

        assertThat(side.getContent().entrySet()).hasSize(1);
    }
}