package lgonode.domain.configuration;

import org.codehaus.jackson.map.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.nio.file.*;

import static org.assertj.core.api.Assertions.*;

class PlatformConfigurationTest {

    @Test
    void it_reads_the_new_format() throws Exception {
        var jsonContent = Files.readString(Path.of(getClass().getClassLoader().getResource("configuration/configuration.json").toURI()));

        var platformConfiguration = new ObjectMapper().readValue(jsonContent, PlatformConfiguration.class);

        assertThat(platformConfiguration.products.get("BTC-USD"))
                .usingRecursiveComparison()
                .isEqualTo(new ProductConfiguration("BTC-USD", "0.001", "1000","10", "1000000", "10", "50000000"));
    }
}