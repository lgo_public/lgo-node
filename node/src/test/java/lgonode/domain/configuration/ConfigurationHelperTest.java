package lgonode.domain.configuration;

import lgonode.domain.TestUtils;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.*;

class ConfigurationHelperTest {

    @Test
    void to_pair_is_case_insensitive() {
        var configuration = ConfigurationHelper.DEFAULT_BEFORE_65598189;

        var pair1 = ConfigurationHelper.toPair("BTC-USD", configuration);
        var pair2 = ConfigurationHelper.toPair("btc-usd", configuration);

        assertThat(pair1).isEqualTo(pair2);
        assertThat(pair1).isEqualTo(TestUtils.Pairs.BTC_USD);
    }

    @Test
    void to_products_limits_is_case_insensitive() {
        var configuration = ConfigurationHelper.DEFAULT_BEFORE_65598189;

        var productLimits1 = ConfigurationHelper.toProductLimits("BTC-USD", configuration);
        var productLimits2 = ConfigurationHelper.toProductLimits("btc-usd", configuration);

        assertThat(productLimits1).isNotNull().usingRecursiveComparison().isEqualTo(productLimits2);
    }

    @Test
    void to_products_limits_is_case_insensitive_() {
        var configuration = new PlatformConfiguration();
        configuration.currencies.put("tLGO1", new CurrencyConfiguration("tLGO1", 8));
        configuration.currencies.put("tLGO2", new CurrencyConfiguration("tLGO2", 4));
        configuration.products.put("tLGO1-tLGO2", new ProductConfiguration("tLGO1-tLGO2", "100", "1000", "100", "1000", "1000", "100000"));

        var productLimits1 = ConfigurationHelper.toProductLimits("TLGO1-TLGO2", configuration);
        var productLimits2 = ConfigurationHelper.toProductLimits("tLGO1-tLGO2", configuration);

        assertThat(productLimits1).isNotNull().usingRecursiveComparison().isEqualTo(productLimits2);
    }
}