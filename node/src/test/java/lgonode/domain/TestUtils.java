package lgonode.domain;

import java.util.*;

public class TestUtils {

    public static class Currencies {
        public static Currency of(String code) {
            return CURRENCIES.get(code.toUpperCase());
        }

        public static final Currency BTC = new Currency("BTC", 8);
        public static final Currency LGO = new Currency("LGO", 8);
        public static final Currency USD = new Currency("USD", 4);
        public static final Currency tLGO1 = new Currency("tLGO1", 8);
        public static final Currency tLGO2 = new Currency("tLGO2", 4);
        public static final Currency USDT_P = new Currency("USDT.P", 4);
        public static final Currency USD_P = new Currency("USD.P", 4);

        private static final Map<String, Currency> CURRENCIES;
        static {
            CURRENCIES = new HashMap<>();
            CURRENCIES.put("BTC", BTC);
            CURRENCIES.put("LGO", LGO);
            CURRENCIES.put("USD", USD);
            CURRENCIES.put("TLGO1", tLGO1);
            CURRENCIES.put("TLGO2", tLGO2);
            CURRENCIES.put("USDT.P", USDT_P);
            CURRENCIES.put("USD.P", USD_P);
        }


    }
    
    public static class Pairs {
        public static Pair of(String base, String quote) {
            return new Pair(Currencies.of(base), Currencies.of(quote));
        }

        public static final Pair BTC_USD = new Pair(Currencies.BTC, Currencies.USD);
        public static final Pair LGO_USD = new Pair(Currencies.LGO, Currencies.USD);
        public static final Pair tLGO = new Pair(Currencies.tLGO1, Currencies.tLGO2);
        public static final Pair USDTP_USDP = new Pair(Currencies.USDT_P, Currencies.USD_P);
    }

}
