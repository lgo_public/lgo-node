package lgonode.infrastructure.avro;

import com.google.common.collect.Maps;
import group.lgo.*;
import lgonode.domain.Metadata;
import lgonode.infrastructure.*;
import org.apache.avro.file.DataFileReader;
import org.apache.avro.generic.*;
import org.apache.avro.specific.SpecificDatumReader;
import org.slf4j.*;

import javax.inject.*;
import java.io.*;
import java.nio.ByteBuffer;
import java.nio.file.*;
import java.util.Map;
import java.util.regex.*;

public class BatchFilesReadingStep implements BatchProcessingStep {

    @Inject
    public BatchFilesReadingStep(@Named("storage.batch.path") String batchStoragePath) {
        this.batchStoragePath = Paths.get(batchStoragePath);
    }

    @Override
    public ProcessingResult process(BatchProcessingContext context) {
        try {
            Metadata metadata = readMetadata(context);
            readFailedOrders(context);
            readOrders(context);
            readUpgradedOtsProof(context, metadata);
        } catch (IOException e) {
            LOGGER.error("IO error while processing batch files", e);
            return ProcessingResult.FAILURE;
        }
        return ProcessingResult.CONTINUE;
    }

    Metadata readMetadata(BatchProcessingContext context) throws IOException {
        String metadataContent = new String(Files.readAllBytes(FileNames.metadataFileFor(batchStoragePath, context.batchDirectory())));
        Matcher matcher = metadataPattern.matcher(metadataContent);
        if (!matcher.matches() || matcher.groupCount() != 8) {
            throw new IOException("Cannot read metadata content");
        }
        long id = Long.parseLong(matcher.group(1));
        String hash = matcher.group(2);
        String previousHash = matcher.group(3);
        int orderCount = Integer.parseInt(matcher.group(4));
        boolean cancelOrderBooks = matcher.group(5) != null;
        var configVersion = matcher.group(6) != null ? Integer.parseInt(matcher.group(7)) : -1;
        var configHash = matcher.group(6) != null ? matcher.group(8) : null;
        context.metadataRead(id, hash, previousHash, orderCount, cancelOrderBooks, configVersion, configHash);
        return context.metadata();
    }

    void readFailedOrders(BatchProcessingContext context) throws IOException {
        File failedOrdersFile = FileNames.failedOrdersFor(batchStoragePath, context.batchDirectory()).toFile();
        try (var failedOrders = new DataFileReader<>(failedOrdersFile, new SpecificDatumReader<>(FailedOrder.class))) {
            for (var failedOrder : failedOrders) {
                context.failedOrderRead(failedOrder.getId(), failedOrder.getFailureReason().toString(), getDetails(failedOrder));
            }
        }
    }

    private Map<String, String> getDetails(FailedOrder failedOrder) {
        Map<String, String> result = Maps.newHashMap();
        if (failedOrder.getDetails() == null) {
            return result;
        }
        for (Map.Entry<CharSequence, CharSequence> entry : failedOrder.getDetails().entrySet()) {
            result.put(entry.getKey().toString(), entry.getValue().toString());
        }
        return result;
    }

    void readOrders(BatchProcessingContext context) throws IOException {
        File ordersFile = FileNames.ordersFor(batchStoragePath, context.batchDirectory()).toFile();
        DataFileReader<GenericRecord> reader = new DataFileReader<>(ordersFile, new GenericDatumReader<>());
        GenericRecord reuse = null;
        while (reader.hasNext()) {
            GenericRecord object = reader.next(reuse);
            if (Order.SCHEMA$.equals(object.getSchema()) || EncryptedOrder.SCHEMA$.equals(object.getSchema())) {
                context.encryptedOrderRead((Long) object.get("id"), ((CharSequence) object.get("keyId")).toString(), data((ByteBuffer) object.get("data")));
            } else if (UnencryptedOrder.SCHEMA$.equals(object.getSchema())) {
                context.unencryptedOrderRead((Long) object.get("id"), data((ByteBuffer) object.get("data")));
            }
        }
    }

    void readUpgradedOtsProof(BatchProcessingContext context, Metadata metadata) throws IOException {
        Path proof = FileNames.upgradedProofFileFor(batchStoragePath, context.batchDirectory());
        context.otsProofRead(metadata.hash, Files.readAllBytes(proof));
    }

    private byte[] data(ByteBuffer byteBuffer) {
        byte[] data = new byte[byteBuffer.capacity()];
        byteBuffer.get(data);
        return data;
    }

    private final Path batchStoragePath;
    private static Pattern metadataPattern = Pattern.compile("ID=([0-9]*)\nHASH=(.*)\nPREVIOUS_HASH=(.*)\nORDER_COUNT=([0-9]*)(\nCANCEL_ORDER_BOOKS=true)?(\nCONFIG_VERSION=([0-9]*)\nCONFIG_HASH=(.*))?");
    private static final Logger LOGGER = LoggerFactory.getLogger(BatchFilesReadingStep.class);
}
