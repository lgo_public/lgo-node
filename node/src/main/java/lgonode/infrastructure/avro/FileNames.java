package lgonode.infrastructure.avro;

import java.nio.file.Path;

public final class FileNames {

    private FileNames() {
    }

    public static Path batchDirectory(Path storagePath, String batchId) {
        return storagePath.resolve(batchId);
    }

    public static Path upgradedProofFileFor(Path storagePath, String batchId) {
        return storagePath.resolve(batchId + "/" + proofFileName(batchId));
    }

    public static String proofFileName(String batchId) {
        return batchId + ".ots.upgraded.ots";
    }

    public static Path metadataFileFor(Path storagePath, String batchId) {
        return storagePath.resolve(batchId + "/" + metadataFileName(batchId));
    }

    public static String metadataFileName(String batchId) {
        return batchId + ".metadata";
    }

    public static Path failedOrdersFor(Path storagePath, String batchId) {
        return storagePath.resolve(batchId + "/" + failedOrdersFileName(batchId));
    }

    public static String failedOrdersFileName(String batchId) {
        return batchId + "-failed-orders.avro";
    }

    public static Path ordersFor(Path storagePath, String batchId) {
        return storagePath.resolve(batchId + "/" + ordersFileName(batchId));
    }

    public static String ordersFileName(String batchId) {
        return batchId + ".avro";
    }

}
