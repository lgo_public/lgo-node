package lgonode.infrastructure;

import com.google.common.base.Charsets;
import com.google.common.hash.Hashing;
import lgonode.domain.*;
import lgonode.domain.configuration.PlatformConfiguration;

import java.util.*;

public class BatchProcessingContext {

    private BatchProcessingContext(String batchDirectory, String lastBatchHash) {
        this.batchDirectory = batchDirectory;
        this.batchId = Long.parseLong(batchDirectory);
        this.lastBatchHash = lastBatchHash;
    }

    public static BatchProcessingContext create(String batchIdAsString, String lastBatchHash) {
        return new BatchProcessingContext(batchIdAsString, lastBatchHash);
    }

    public static BatchProcessingContext create(long batchId, String lastBatchHash) {
        return new BatchProcessingContext(formatBatchId(batchId), lastBatchHash);
    }

    public void encryptedOrderRead(long id, String keyId, byte[] data) {
        rawOrders.add(new EncryptedOrder(id, batchId, keyId, data));
    }

    public void unencryptedOrderRead(long id, byte[] data) {
        rawOrders.add(new UnencryptedOrder(id, batchId, data));
    }

    public void failedOrderRead(long id, String reason, Map<String, String> details) {
        failedOrders.add(new FailedOrder(id, batchId, reason, details));
    }

    public void metadataRead(long id, String hash, String previousHash, int orderCount, boolean cancelOrderBooks, int configVersion, String configHash) {
        metadata = new Metadata(id, hash, previousHash, orderCount, cancelOrderBooks, configVersion, configHash);
    }

    public void configurationRead(PlatformConfiguration platformConfiguration) {
        this.platformConfiguration = platformConfiguration;
    }

    public void otsProofRead(String batchHash, byte[] content) {
        otsProof = new Proof(batchId, Hashing.sha256().hashString(batchHash, Charsets.UTF_8).toString(), content);
    }

    public void orderDecrypted(long id, String keyId, String data) {
        decryptedOrders.add(new DecryptedOrder(id, batchId, keyId, data));
    }

    public void orderContentRead(long id, String keyId, long creationDate, String type, String direction, Pair pair, String quantity, String price) {
        orders.add(new Order(id, batchId, keyId, creationDate, type, direction, pair, quantity, price));
    }

    public void cancelOrderContentRead(long orderId, String keyId, long creationDate, long orderIdToCancel) {
        cancelOrders.add(new CancelOrder(orderId, batchId, keyId, creationDate, orderIdToCancel));
    }

    public void matched(long price, long quantity, Pair pair, long timestamp) {
        trades.add(new Trade(batchId, price, quantity, pair, timestamp));
    }

    public void orderBookCanceled(Pair pair) {
        orderBookEvents.add(new OrderBookCancelOrdersEvent(batchId, pair.base.code, pair.quote.code));
    }

    public void orderLeavesOrderBook(Order order) {
        orderBookEvents.add(new OrderBookRemoveOrderEvent(batchId, order.pair.base.code, order.pair.quote.code, order.direction, order.price, order.id));
    }

    public void orderRemainingQuantityUpdated(Order order) {
        orderBookEvents.add(new OrderBookUpdateOrderEvent(batchId, order.pair.base.code, order.pair.quote.code, order.direction, order.price, order.id, order.remainingQuantity()));
    }

    public void orderEnteredBook(Order order) {
        orderBookEvents.add(new OrderBookAddOrderEvent(batchId, order.pair.base.code, order.pair.quote.code, order.direction, order.price, order.id, order.remainingQuantity()));
    }

    public void orderBookSerialized(String value) {
        serializedOrderBooks = value;
    }

    public void nextBatch() {
        lastBatchHash = metadata.hash;
        batchId++;
        batchDirectory = formatBatchId(batchId);
        metadata = null;
        serializedOrderBooks = null;
        rawOrders.clear();
        failedOrders.clear();
        decryptedOrders.clear();
        orders.clear();
        cancelOrders.clear();
        trades.clear();
        orderBookEvents.clear();
    }

    private static String formatBatchId(long batchId) {
        return String.format("%010d", batchId);
    }

    public String batchDirectory() {
        return batchDirectory;
    }

    public long batchId() {
        return batchId;
    }

    public String lastBatchHash() {
        return lastBatchHash;
    }

    public Metadata metadata() {
        return metadata;
    }

    public List<RawOrder> rawOrders() {
        return Collections.unmodifiableList(rawOrders);
    }

    public List<FailedOrder> failedOrders() {
        return Collections.unmodifiableList(failedOrders);
    }

    public List<DecryptedOrder> decryptedOrders() {
        return Collections.unmodifiableList(decryptedOrders);
    }

    public List<Order> orders() {
        return Collections.unmodifiableList(orders);
    }

    public List<CancelOrder> cancelOrders() {
        return Collections.unmodifiableList(cancelOrders);
    }

    public List<Trade> trades() {
        return Collections.unmodifiableList(trades);
    }

    public Proof otsProof() {
        return otsProof;
    }

    public String orderBookSerializedContent() {
        return serializedOrderBooks;
    }

    public List<OrderBookEvent> orderBookEvents() {
        return Collections.unmodifiableList(orderBookEvents);
    }

    public PlatformConfiguration platformConfiguration() {
        return this.platformConfiguration;
    }

    private String batchDirectory;
    private long batchId;
    private Proof otsProof;
    private String lastBatchHash;
    private Metadata metadata;
    private PlatformConfiguration platformConfiguration;
    private List<RawOrder> rawOrders = new ArrayList<>();
    private List<FailedOrder> failedOrders = new ArrayList<>();
    private List<DecryptedOrder> decryptedOrders = new ArrayList<>();
    private List<Order> orders = new ArrayList<>();
    private List<CancelOrder> cancelOrders = new ArrayList<>();
    private List<Trade> trades = new ArrayList<>();
    private List<OrderBookEvent> orderBookEvents = new ArrayList<>();
    private String serializedOrderBooks;
}
