package lgonode.infrastructure.gcs;

import com.google.cloud.storage.Blob;
import com.google.common.base.Charsets;
import com.google.common.hash.Hashing;
import lgonode.configuration.GCSConfiguration;
import lgonode.domain.configuration.*;
import org.codehaus.jackson.map.ObjectMapper;

import javax.inject.Inject;
import java.io.IOException;
import java.nio.file.*;

public class GCSConfigurationLoader extends GCSFileReader implements ConfigurationLoader {

    @Inject
    public GCSConfigurationLoader(GCSConfiguration gcsConfiguration) {
        this.gcsConfiguration = gcsConfiguration;
    }

    @Override
    public PlatformConfiguration load(int configVersion, String configHash) {
        var config = getBlob(configVersion);
        if (config == null) {
            throw new IllegalStateException("Configuration v"+configVersion+ ": not found");
        }
        try {
            Path path = copyFileToTemp(config);
            var jsonContent = Files.readString(path);
            if (configVersion != 0) {
                var computedHash = hash(jsonContent);
                if (!computedHash.equals(configHash)) {
                    throw new IllegalStateException("Configuration v" + configVersion + ": corrupted");
                }
            }
            return new ObjectMapper().readValue(jsonContent, PlatformConfiguration.class);
        } catch (IOException e) {
            throw new IllegalStateException("Bad configuration format", e);
        }
    }

    private String hash(String value) {
        return Hashing.sha256()
                .newHasher()
                .putString(value, Charsets.UTF_8)
                .hash()
                .toString();
    }

    private Blob getBlob(int configVersion) {
        return storage.get(gcsConfiguration.configuration(), "configuration-v" + configVersion + ".json");
    }

    private final GCSConfiguration gcsConfiguration;
}
