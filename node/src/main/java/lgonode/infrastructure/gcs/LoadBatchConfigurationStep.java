package lgonode.infrastructure.gcs;

import lgonode.domain.configuration.*;
import lgonode.infrastructure.*;
import org.slf4j.*;

import javax.inject.Inject;
import java.util.*;

public class LoadBatchConfigurationStep extends GCSFileReader implements BatchProcessingStep  {

    @Inject
    public LoadBatchConfigurationStep(ConfigurationLoader configurationLoader) {
        this.configurationLoader = configurationLoader;
    }

    @Override
    public ProcessingResult process(BatchProcessingContext context) {
        var configVersion = context.metadata().configVersion;
        if (configVersion < 0) {
            context.configurationRead(ConfigurationHelper.defaultConfiguration(context.batchId()));
            return ProcessingResult.CONTINUE;
        }
        if (retrievedConfigVersions.containsKey(configVersion)) {
            context.configurationRead(retrievedConfigVersions.get(configVersion));
            return ProcessingResult.CONTINUE;
        }
        try {
            var config = configurationLoader.load(configVersion, context.metadata().configHash);
            retrievedConfigVersions.put(configVersion, config);
            context.configurationRead(config);
            return ProcessingResult.CONTINUE;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return ProcessingResult.FAILURE;
        }
    }

    private final Map<Integer, PlatformConfiguration> retrievedConfigVersions = new HashMap<>();
    private final ConfigurationLoader configurationLoader;
    private static final Logger LOGGER = LoggerFactory.getLogger(LoadBatchConfigurationStep.class);
}
