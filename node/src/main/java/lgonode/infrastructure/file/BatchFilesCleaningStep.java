package lgonode.infrastructure.file;

import lgonode.infrastructure.*;
import lgonode.infrastructure.avro.FileNames;
import org.slf4j.*;

import javax.inject.*;
import java.io.*;
import java.nio.file.*;
import java.util.Comparator;

public class BatchFilesCleaningStep implements BatchProcessingStep {

    @Inject
    public BatchFilesCleaningStep(@Named("storage.batch.path") String batchStoragePath) {
        this.batchStoragePath = Paths.get(batchStoragePath);
    }

    @Override
    @SuppressWarnings("ResultOfMethodCallIgnored")
    public ProcessingResult process(BatchProcessingContext context) {
        Path path = FileNames.batchDirectory(batchStoragePath, context.batchDirectory());
        try {
            Files.walk(path)
                    .sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .forEach(File::delete);
        } catch (IOException e) {
            LOGGER.error("Error while deleting batch "+context.batchId()+" files", e);
        }
        return ProcessingResult.CONTINUE;
    }

    private final Path batchStoragePath;
    private static final Logger LOGGER = LoggerFactory.getLogger(BatchFilesCleaningStep.class);
}
