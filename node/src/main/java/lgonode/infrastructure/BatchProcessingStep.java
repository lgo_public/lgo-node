package lgonode.infrastructure;

public interface BatchProcessingStep {

    ProcessingResult process(BatchProcessingContext context);

}
