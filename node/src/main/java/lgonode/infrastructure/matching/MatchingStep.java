package lgonode.infrastructure.matching;

import lgonode.domain.*;
import lgonode.domain.configuration.*;
import lgonode.infrastructure.*;
import lgonode.infrastructure.persistence.*;
import org.slf4j.*;

import javax.inject.Inject;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static lgonode.infrastructure.ProcessingResult.*;

public class MatchingStep implements BatchProcessingStep {

    private static final int EVERY_N_BATCH_SAVE_ORDER_BOOK = 100;

    @Inject
    public MatchingStep(OrderBooksSerializer orderBooksSerializer,
                        OrderBooksDeserializer orderBooksDeserializer,
                        OrderRepository orderRepository,
                        ConfigurationLoader configurationLoader) {
        this.orderBooksSerializer = orderBooksSerializer;
        this.orderBooksDeserializer = orderBooksDeserializer;
        this.orderRepository = orderRepository;
        this.configurationLoader = configurationLoader;
    }

    @Override
    public ProcessingResult process(BatchProcessingContext context) {

        applyCancelOrderBooks(context);

        List<Order> orders = context.orders();
        Map<Long, FailedOrder> failedOrders = buildFailedOrdersMap(context);
        List<Long> canceledOrdersIds = buildCanceledOrdersIds(context, failedOrders.keySet());

        applyCanceledOrdersOnBooks(canceledOrdersIds, context);

        executeOrders(orders, failedOrders, canceledOrdersIds, context);

        if (context.batchId() % EVERY_N_BATCH_SAVE_ORDER_BOOK == 0) {
            try {
                saveOrderBookState(context);
            } catch (IOException e) {
                LOGGER.error("Error while saving orderbooks state", e);
                return FAILURE;
            }
        }

        return CONTINUE;
    }

    private void executeOrders(List<Order> orders, Map<Long, FailedOrder> failedOrders, List<Long> canceledOrdersIds, BatchProcessingContext context) {
        for (Order order : orders) {
            if (wasCanceled(order, canceledOrdersIds)) {
                continue;
            }
            if (wasNotExecuted(order, failedOrders)) {
                continue;
            }
            OrderBook orderBook = selectBook(order);
            OrderBookSide side = orderBook.counterSide(order.direction);
            while (verifiesLimits(order, context)) {
                Optional<Order> topOrder = side.topOrder();
                if (topOrder.isEmpty()) {
                    break;
                }
                Order restingOrder = topOrder.get();
                if (wasCanceledBySTPByThisOrder(restingOrder, order, failedOrders)) {
                    side.removeTopOrder();
                    context.orderLeavesOrderBook(restingOrder);
                    continue;
                }
                if (!priceMatches(order, restingOrder)) {
                    break;
                }
                generateTrade(order, restingOrder, context);
                if (!verifiesLimits(restingOrder, context)) {
                    side.removeTopOrder();
                    context.orderLeavesOrderBook(restingOrder);
                } else {
                    context.orderRemainingQuantityUpdated(restingOrder);
                }
            }
            if (verifiesLimits(order, context) && !order.type.equals("M")) {
                orderBook.side(order.direction).enter(order);
                context.orderEnteredBook(order);
            }
        }
    }

    private Map<Long, FailedOrder> buildFailedOrdersMap(BatchProcessingContext context) {
        return context.failedOrders().stream().collect(Collectors.toMap(o -> o.id, o -> o));
    }

    private List<Long> buildCanceledOrdersIds(BatchProcessingContext context, Set<Long> failedOrders) {
        return context
                .cancelOrders().stream()
                .filter(cancelOrder -> !failedOrders.contains(cancelOrder.id))
                .map(cancelOrder -> cancelOrder.orderIdToCancel)
                .collect(Collectors.toList());
    }

    private void generateTrade(Order order, Order restingOrder, BatchProcessingContext context) {
        if (order.type.equals("M") && order.direction.equals("B")) {
            createForMarketBuyOrder(order, restingOrder, context);
        } else {
            createForLimitAndMarketSellOrder(order, restingOrder, context);
        }
    }

    private void createForLimitAndMarketSellOrder(Order order, Order counterOrder, BatchProcessingContext context) {
        long consumed = Math.min(order.remainingQuantity(), counterOrder.remainingQuantity());
        order.consume(consumed);
        counterOrder.consume(consumed);
        context.matched(counterOrder.price, consumed, counterOrder.pair, order.creationDate);
    }

    private void createForMarketBuyOrder(Order order, Order counterOrder, BatchProcessingContext context) {
        long maxRestingOrderQuote = Price.total(counterOrder.price, counterOrder.remainingQuantity(), counterOrder.pair);
        long quoteToConsume = Math.min(order.remainingQuantity(), maxRestingOrderQuote);
        long baseToConsume = Price.quantity(counterOrder.price, quoteToConsume, counterOrder.pair);
        order.consume(quoteToConsume);
        counterOrder.consume(baseToConsume);
        context.matched(counterOrder.price, baseToConsume, counterOrder.pair, order.creationDate);
    }

    private boolean priceMatches(Order takerOrder, Order restingOrder) {
        if (takerOrder.type.equals("M")) {
            return true;
        }
        if (takerOrder.direction.equals("S")) {
            return takerOrder.price <= restingOrder.price;
        }
        return takerOrder.price >= restingOrder.price;
    }

    private boolean wasCanceledBySTPByThisOrder(Order restingOrder, Order takerOrder, Map<Long, FailedOrder> failedOrders) {
        return failedOrders.containsKey(restingOrder.id)
                && failedOrders.get(restingOrder.id).reason.equals("OrderCanceledBySTP")
                && failedOrders.get(restingOrder.id).details.get("counterOrderId").equals(Long.toString(takerOrder.id));
    }

    private boolean verifiesLimits(Order order, BatchProcessingContext context) {
        ProductLimits limits = ConfigurationHelper.toProductLimits(order.pair.toString(), context.platformConfiguration());
        if (order.type.equals("M") && order.direction.equals("B")) {
            return marketBuyOrderVerifiesLimits(order, limits);
        }
        if (order.type.equals("M") && order.direction.equals("S")) {
            return marketSellOrderVerifiesLimits(order, limits);
        }
        return limitOrderVerifiesLimits(order, limits);
    }

    private boolean limitOrderVerifiesLimits(Order order, ProductLimits limits) {
        return limits.baseLimit.valid(order.remainingQuantity())
                && limits.totalLimit.valid(order.remainingAmount());
    }

    private boolean marketSellOrderVerifiesLimits(Order order, ProductLimits limits) {
        return limits.baseLimit.valid(order.remainingQuantity());
    }

    /**
     * LGO execution-engine had a bug in the BUY Market Order limits computation
     * until batch of identifier 59620000.
     */
    private boolean marketBuyOrderVerifiesLimits(Order order, ProductLimits limits) {
        if (order.batchId < 59620000) {
            return limits.quoteLimit.valid(order.remainingQuantity());
        }
        return limits.totalLimit.valid(order.remainingQuantity());
    }

    private OrderBook selectBook(Order order) {
        return selectBook(order.pair);
    }

    private OrderBook selectBook(Pair pair) {
        if (!books.containsKey(pair)) {
            books.put(pair, new OrderBook());
        }
        return books.get(pair);
    }

    private boolean wasNotExecuted(Order order, Map<Long, FailedOrder> failedOrders) {
        return failedOrders.containsKey(order.id)
                && !failedOrders.get(order.id).reason.equals("OrderCanceledBySTP");
    }

    private boolean wasCanceled(Order order, List<Long> canceledOrdersIds) {
        return canceledOrdersIds.contains(order.id);
    }

    private void applyCanceledOrdersOnBooks(List<Long> canceledOrdersIds, BatchProcessingContext context) {
        for (OrderBook book : books.values()) {
            List<Order> canceled = book.cancel(canceledOrdersIds);
            for (Order order : canceled) {
                context.orderLeavesOrderBook(order);
            }
        }
    }

    private void applyCancelOrderBooks(BatchProcessingContext context) {
        if (context.metadata().cancelOrderBooks) {
            for (Pair pair : books.keySet()) {
                context.orderBookCanceled(pair);
            }
            books.clear();
        }
    }

    private void saveOrderBookState(BatchProcessingContext context) throws IOException {
        context.orderBookSerialized(serializeOrderBooks());
    }

    public String serializeOrderBooks() throws IOException {
        return orderBooksSerializer.serialize(books);
    }

    public void replay(Metadata lastBatch, String snapshot, List<OrderBookEvent> events) throws IOException {
        var configuration = loadConfiguration(lastBatch);
        loadOrderBooks(snapshot, configuration);
        applyEvents(events, configuration);
    }

    private PlatformConfiguration loadConfiguration(Metadata lastBatch) {
        if (lastBatch.configVersion < 0) {
            return ConfigurationHelper.defaultConfiguration(lastBatch.id);
        }
        return configurationLoader.load(lastBatch.configVersion, lastBatch.configHash);
    }

    private void loadOrderBooks(String content, PlatformConfiguration configuration) throws IOException {
        books.clear();
        books.putAll(orderBooksDeserializer.deserialize(content, configuration));
    }

    private void applyEvents(List<OrderBookEvent> events, PlatformConfiguration configuration) {
        for (OrderBookEvent event : events) {
            var product = ConfigurationHelper.toPair(event.product(), configuration);
            if (event instanceof OrderBookCancelOrdersEvent) {
                books.remove(product);
            }
            if (event instanceof OrderBookUpdateOrderEvent) {
                OrderBookUpdateOrderEvent orderBookEvent = (OrderBookUpdateOrderEvent) event;
                Order order = orderRepository.load(orderBookEvent.orderId, configuration);
                selectBook(product).apply(orderBookEvent, order);
            }
            if (event instanceof OrderBookAddOrderEvent) {
                OrderBookAddOrderEvent orderBookEvent = (OrderBookAddOrderEvent) event;
                Order order = orderRepository.load(orderBookEvent.orderId, configuration);
                selectBook(product).apply(orderBookEvent, order);
            }
            if (event instanceof OrderBookRemoveOrderEvent) {
                OrderBookRemoveOrderEvent orderBookEvent = (OrderBookRemoveOrderEvent) event;
                Order order = orderRepository.load(orderBookEvent.orderId, configuration);
                selectBook(product).apply(orderBookEvent, order);
            }
        }
    }

    Map<Pair, OrderBook> books = new HashMap<>();
    private final OrderBooksSerializer orderBooksSerializer;
    private final OrderBooksDeserializer orderBooksDeserializer;
    private final OrderRepository orderRepository;
    private final ConfigurationLoader configurationLoader;
    private static final Logger LOGGER = LoggerFactory.getLogger(MatchingStep.class);

}
