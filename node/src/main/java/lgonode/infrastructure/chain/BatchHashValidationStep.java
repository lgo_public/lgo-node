package lgonode.infrastructure.chain;

import com.google.common.base.Charsets;
import com.google.common.hash.Hasher;
import com.google.common.hash.Hashing;
import lgonode.domain.RawOrder;
import lgonode.infrastructure.BatchProcessingContext;
import lgonode.infrastructure.BatchProcessingStep;
import lgonode.infrastructure.ProcessingResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class BatchHashValidationStep implements BatchProcessingStep {

    @Override
    public ProcessingResult process(BatchProcessingContext context) {
        if (context.metadata() == null) {
            LOGGER.error("Hash validation failed. No metadata");
            return ProcessingResult.FAILURE;
        }
        String hash = batchHash(context);
        if (!hash.equals(context.metadata().hash)) {
            LOGGER.error("Hash validation failed. Current hash:" + context.metadata().hash + ", expected: " + hash);
            return ProcessingResult.FAILURE;
        }
        return ProcessingResult.CONTINUE;
    }

    public String batchHash(BatchProcessingContext context) {
        String ordersHash = ordersHash(context.rawOrders());
        return Hashing.sha256().newHasher()
                .putString(context.lastBatchHash() == null ? ordersHash : context.lastBatchHash(), Charsets.UTF_8)
                .putString(ordersHash, Charsets.UTF_8)
                .hash().toString();
    }

    private String ordersHash(List<RawOrder> rawOrders) {
        Hasher hasher = Hashing.sha256().newHasher();
        for (RawOrder rawOrder : rawOrders) {
            hasher.putString(rawOrder.hash(), Charsets.UTF_8);
        }
        return hasher.hash().toString();
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(BatchHashValidationStep.class);
}
