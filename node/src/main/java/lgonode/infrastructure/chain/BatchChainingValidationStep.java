package lgonode.infrastructure.chain;

import lgonode.domain.Metadata;
import lgonode.infrastructure.BatchProcessingContext;
import lgonode.infrastructure.BatchProcessingStep;
import lgonode.infrastructure.ProcessingResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Chaining is also validated with BatchHashValidationStep but
 * this step is here to verify that the metadata file content is correct + when we restart the node
 * to be sure the previous hash stored in db is correct.
 */
public class BatchChainingValidationStep implements BatchProcessingStep {

    @Override
    public ProcessingResult process(BatchProcessingContext context) {
        if (context.lastBatchHash() == null && context.batchId() == 0) {
            return ProcessingResult.CONTINUE;
        }
        Metadata metadata = context.metadata();
        if (metadata == null) {
            LOGGER.error("Previous hash validation failed. No metadata");
            return ProcessingResult.FAILURE;
        }
        if (metadata.previousHash == null) {
            LOGGER.error("Previous hash validation failed. Previous hash null");
            return ProcessingResult.FAILURE;
        }
        if (!metadata.previousHash.equals(context.lastBatchHash())) {
            LOGGER.error("Previous hash validation failed. Found "+metadata.previousHash + " but expected " + context.lastBatchHash());
            return ProcessingResult.FAILURE;
        }
        return ProcessingResult.CONTINUE;
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(BatchChainingValidationStep.class);
}
