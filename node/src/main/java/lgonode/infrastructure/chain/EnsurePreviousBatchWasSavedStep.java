package lgonode.infrastructure.chain;

import lgonode.domain.BatchRepository;
import lgonode.domain.Metadata;
import lgonode.infrastructure.BatchProcessingContext;
import lgonode.infrastructure.BatchProcessingStep;
import lgonode.infrastructure.ProcessingResult;

import javax.inject.Inject;

public class EnsurePreviousBatchWasSavedStep implements BatchProcessingStep {

    @Inject
    public EnsurePreviousBatchWasSavedStep(BatchRepository batchRepository) {
        this.batchRepository = batchRepository;
    }

    @Override
    public ProcessingResult process(BatchProcessingContext context) {
        if (context.batchId() == 0) {
            return ProcessingResult.CONTINUE;
        }
        Metadata lastSavedBatch = batchRepository.lastBatch();
        if (lastSavedBatch == null || lastSavedBatch.id != context.batchId() - 1) {
            return ProcessingResult.FAILURE;
        }
        return ProcessingResult.CONTINUE;
    }

    private final BatchRepository batchRepository;
}
