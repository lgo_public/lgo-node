package lgonode.infrastructure.influxDB;

import org.influxdb.InfluxDB;

public class InfluxDBConnection implements AutoCloseable {

    public InfluxDBConnection(InfluxDB influxDB, String database) {
        this.influxDB = influxDB;
        this.database = database;
    }

    public InfluxDB influxDB() {
        return influxDB;
    }

    public String database() {
        return database;
    }

    @Override
    public void close() {
        if(influxDB == null) {
            return;
        }
        influxDB.close();
    }

    private final InfluxDB influxDB;
    private final String database;
}
