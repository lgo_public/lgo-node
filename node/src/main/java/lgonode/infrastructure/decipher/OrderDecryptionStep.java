package lgonode.infrastructure.decipher;

import lgonode.domain.*;
import lgonode.infrastructure.*;
import org.slf4j.*;

import javax.crypto.*;
import javax.inject.Inject;
import java.security.*;
import java.util.stream.Collectors;

public class OrderDecryptionStep implements BatchProcessingStep {

    private final KeyRetriever keyRetriever;

    @Inject
    public OrderDecryptionStep(KeyRetriever keyRetriever) {
        this.keyRetriever = keyRetriever;
    }

    @Override
    public ProcessingResult process(BatchProcessingContext context) {
        retrieveKeys(context);
        return context
                .rawOrders()
                .stream()
                .map(encryptedOrder -> decipher(encryptedOrder, context))
                .anyMatch(ProcessingResult.FAILURE::equals) ? ProcessingResult.FAILURE : ProcessingResult.CONTINUE;
    }

    private void retrieveKeys(BatchProcessingContext context) {
        keyRetriever.retrievePrivateKeys(context
                .rawOrders()
                .stream()
                .filter(RawOrder::isEncrypted)
                .map(o -> ((EncryptedOrder) o).keyId)
                .distinct()
                .collect(Collectors.toList())
        );
    }

    private ProcessingResult decipher(RawOrder rawOrder, BatchProcessingContext context) {
        if (!rawOrder.isEncrypted()) {
            return readUnencrypted((UnencryptedOrder) rawOrder, context);
        }
        return readEncrypted((EncryptedOrder) rawOrder, context);

    }

    private ProcessingResult readEncrypted(EncryptedOrder encryptedOrder, BatchProcessingContext context) {
        if (context.failedOrders()
                .stream()
                .filter(failedOrder -> failedOrder.reason.contains("malformed-order"))
                .anyMatch(failedOrder -> failedOrder.id == encryptedOrder.id)) {
            return ProcessingResult.CONTINUE;
        }
        return keyRetriever.getPrivateKey(encryptedOrder.keyId).map(privateKey -> {
            try {
                String data = decipher(encryptedOrder.data, privateKey);
                context.orderDecrypted(encryptedOrder.id, encryptedOrder.keyId, data);
                return ProcessingResult.CONTINUE;
            } catch (Exception e) {
                LOGGER.error("Not possible to decipher", e);
                return ProcessingResult.FAILURE;
            }
        }).orElse(ProcessingResult.CONTINUE);
    }

    private ProcessingResult readUnencrypted(UnencryptedOrder unencryptedOrder, BatchProcessingContext context) {
        context.orderDecrypted(unencryptedOrder.id, "N/A", new String(unencryptedOrder.data));
        return ProcessingResult.CONTINUE;
    }

    private String decipher(byte[] data, PrivateKey privateKey) throws NoSuchPaddingException, NoSuchAlgorithmException, BadPaddingException, IllegalBlockSizeException, InvalidKeyException {
        Cipher cipher = Cipher.getInstance("RSA/None/OAEPPadding");
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        return new String(cipher.doFinal(data));
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(OrderDecryptionStep.class);

}
