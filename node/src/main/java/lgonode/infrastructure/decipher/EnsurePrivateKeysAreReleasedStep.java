package lgonode.infrastructure.decipher;

import com.google.cloud.storage.StorageException;
import lgonode.domain.EncryptedOrder;
import lgonode.domain.RawOrder;
import lgonode.infrastructure.BatchProcessingContext;
import lgonode.infrastructure.BatchProcessingStep;
import lgonode.infrastructure.ProcessingResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

public class EnsurePrivateKeysAreReleasedStep implements BatchProcessingStep {

    private final KeyRetriever keyRetriever;

    @Inject
    public EnsurePrivateKeysAreReleasedStep(KeyRetriever keyRetriever) {
        this.keyRetriever = keyRetriever;
    }

    @Override
    public ProcessingResult process(BatchProcessingContext context) {
        List<String> keyIds = context
                .rawOrders()
                .stream()
                .filter(RawOrder::isEncrypted)
                .map(o -> ((EncryptedOrder) o).keyId)
                .distinct()
                .collect(Collectors.toList());
        try {
            keyRetriever.retrievePrivateKeys(keyIds);
        } catch (StorageException e) {
            LOGGER.error("Cannot read GCS for private keys", e);
            return ProcessingResult.FAILURE;
        }
        List<String> publicKeyAvailablesButNoPrivateKeys = keyIds
                .stream()
                .filter(k -> keyRetriever.getPrivateKey(k).isEmpty() && keyRetriever.isPublicKeyAvailable(k))
                .collect(Collectors.toList());
        if (!publicKeyAvailablesButNoPrivateKeys.isEmpty()) {
            return ProcessingResult.WAIT;
        }
        return ProcessingResult.CONTINUE;
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(EnsurePrivateKeysAreReleasedStep.class);
}
