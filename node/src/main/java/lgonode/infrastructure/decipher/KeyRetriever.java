package lgonode.infrastructure.decipher;

import lgonode.infrastructure.gcs.GCSKeyBucketReader;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.IOException;
import java.io.Reader;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

public class KeyRetriever {

    @Inject
    public KeyRetriever(GCSKeyBucketReader gcsKeyBucketReader) {
        this.gcsKeyBucketReader = gcsKeyBucketReader;
    }

    public void retrievePrivateKeys(List<String> keyIds) {
        for (String keyId : keyIds) {
            if (!cache.containsKey(keyId)) {
                gcsKeyBucketReader.loadPrivateKey(keyId)
                        .ifPresent(reader -> {
                            try (reader) {
                                PrivateKey privateKey = readPrivateKey(reader);
                                cache.put(keyId, privateKey);
                            } catch (IOException e) {
                                LOGGER.error("Private key error", e);
                            }
                        });
            }
        }
    }

    public boolean isPublicKeyAvailable(String keyId) {
        return gcsKeyBucketReader.isPublicKeyAvailable(keyId);
    }

    public Optional<PrivateKey> getPrivateKey(String keyId) {
        return Optional.ofNullable(cache.getOrDefault(keyId, null));
    }

    private PrivateKey readPrivateKey(Reader reader) throws IOException {
        PEMParser pemParser = new PEMParser(reader);
        JcaPEMKeyConverter converter = new JcaPEMKeyConverter().setProvider("BC");
        PEMKeyPair pemKeyPair = (PEMKeyPair) pemParser.readObject();
        KeyPair kp = converter.getKeyPair(pemKeyPair);
        return kp.getPrivate();
    }

    private final GCSKeyBucketReader gcsKeyBucketReader;
    private final Map<String, PrivateKey> cache = new ConcurrentHashMap<>();
    private static final Logger LOGGER = LoggerFactory.getLogger(KeyRetriever.class);
}
