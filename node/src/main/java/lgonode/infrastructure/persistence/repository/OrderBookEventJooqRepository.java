package lgonode.infrastructure.persistence.repository;

import lgonode.domain.*;
import org.jooq.*;

import javax.inject.Inject;
import java.util.List;

import static lgonode.infrastructure.persistence.dao.OrderBookEventsDAO.*;

public class OrderBookEventJooqRepository implements OrderBookEventRepository {

    @Inject
    public OrderBookEventJooqRepository(DSLContext dsl) {
        this.dsl = dsl;
    }

    @Override
    public void save(List<OrderBookEvent> eventList) {
        var insert = this.dsl.insertInto(TABLE).columns(BATCH_ID, BASE_CURRENCY, QUOTE_CURRENCY, TYPE, PAYLOAD);
        for (OrderBookEvent event : eventList) {
            insert = insert.values(event.batchId, event.base, event.quote, event.type().name(), event.payload());
        }
        insert.execute();
    }

    @Override
    public List<OrderBookEvent> loadEventsAfterBatch(long batchId) {
        return this.dsl.select(BATCH_ID, BASE_CURRENCY, QUOTE_CURRENCY, TYPE, PAYLOAD)
                .from(TABLE)
                .where(BATCH_ID.greaterThan(batchId))
                .orderBy(ID.asc())
                .fetch()
                .map(this::map);
    }

    private OrderBookEvent map(Record5<Long, String, String, String, String> record) {
        long batchId = record.component1();
        String baseCurrency = record.component2();
        String quoteCurrency = record.component3();
        OrderBookEventType eventType = OrderBookEventType.valueOf(record.component4());
        String payload = record.component5();
        return eventType.restore(batchId, baseCurrency, quoteCurrency, payload);
    }

    private final DSLContext dsl;
}
