package lgonode.infrastructure.persistence.repository;

import lgonode.domain.Trade;
import lgonode.domain.TradeRepository;
import lgonode.infrastructure.influxDB.InfluxDBConnection;
import org.influxdb.dto.Point;

import javax.inject.Inject;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class TradeInfluxRepository implements TradeRepository {

    @Inject
    public TradeInfluxRepository(InfluxDBConnection influxDBConnection) {
        this.influxDBConnection = influxDBConnection;
    }

    @Override
    public void save(List<Trade> tradeList) {
        for (Trade trade : tradeList) {
            save(trade);
            count++;
        }
    }

    private void save(Trade trade) {
        // InfluxDB will overwrite values with same timestamp.
        // So we simulate different timestamp by incrementing counter.
        long influxTimestamp = trade.timestamp * 1_000_000 + count;
        Point point = Point.measurement("trades_"+trade.pair.base.code + trade.pair.quote.code)
                .time(influxTimestamp, TimeUnit.NANOSECONDS)
                .addField("id", trade.id)
                .addField("batchId", trade.batchId)
                .addField("price", trade.price)
                .addField("quantity", trade.quantity)
                .addField("amount", trade.amount())
                .build();
        influxDBConnection.influxDB().write(point);
    }

    private final InfluxDBConnection influxDBConnection;
    private long count = 0;
}
