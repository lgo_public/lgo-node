package lgonode.infrastructure.persistence.repository;

import lgonode.domain.*;
import lgonode.domain.configuration.*;
import org.jooq.*;

import javax.inject.Inject;
import java.util.List;

import static lgonode.infrastructure.persistence.dao.OrderDAO.*;

public class OrderJooqRepository implements OrderRepository {

    @Inject
    public OrderJooqRepository(DSLContext dsl) {
        this.dsl = dsl;
    }

    @Override
    public void save(List<Order> orderList) {
        var insert = this.dsl.insertInto(TABLE).columns(ID, BATCH_ID, KEY_ID, CREATION_DATE, TYPE, DIRECTION, BASE_CURRENCY, QUOTE_CURRENCY, QUANTITY, PRICE);
        for (Order order : orderList) {
            insert = insert.values(order.id, order.batchId, order.keyId, order.creationDate, order.type, order.direction, order.pair.base.code, order.pair.quote.code, order.formatQuantity(), order.formatPrice());
        }
        insert.execute();
    }

    @Override
    public Order load(long id, PlatformConfiguration configuration) {
        return this.dsl
                .select(ID, BATCH_ID, KEY_ID, CREATION_DATE, TYPE, DIRECTION, BASE_CURRENCY, QUOTE_CURRENCY, QUANTITY, PRICE)
                .from(TABLE)
                .where(ID.eq(id))
                .fetchOne(record -> map(record, configuration));
    }

    private Order map(Record10<Long, Long, String, Long, String, String, String, String, String, String> record, PlatformConfiguration platformConfiguration) {
        var pairKey = Pair.pairKey(record.get(BASE_CURRENCY), record.get(QUOTE_CURRENCY));
        var batchId = record.get(BATCH_ID);
        var pair = ConfigurationHelper.toPair(pairKey, platformConfiguration);
        return new Order(record.get(ID), batchId, record.get(KEY_ID), record.get(CREATION_DATE), record.get(TYPE), record.get(DIRECTION), pair, record.get(QUANTITY), record.get(PRICE));
    }

    private final DSLContext dsl;
}
