package lgonode.infrastructure.persistence.repository;

import lgonode.domain.Trade;
import lgonode.domain.TradeRepository;
import org.jooq.DSLContext;

import javax.inject.Inject;
import java.util.List;

import static lgonode.infrastructure.persistence.dao.TradeDAO.*;

public class TradeJooqRepository implements TradeRepository {

    @Inject
    public TradeJooqRepository(DSLContext dsl) {
        this.dsl = dsl;
    }

    @Override
    public void save(List<Trade> tradeList) {
        var insert = this.dsl.insertInto(TABLE).columns(BATCH_ID, CREATION_DATE, BASE_CURRENCY, QUOTE_CURRENCY, QUANTITY, PRICE);
        for (Trade trade : tradeList) {
            insert = insert.values(trade.batchId, trade.timestamp, trade.pair.base.code, trade.pair.quote.code, trade.formatQuantity(), trade.formatPrice());
        }
        insert.execute();
    }

    private final DSLContext dsl;
}
