package lgonode.infrastructure.persistence.repository;

import lgonode.domain.*;
import org.jooq.DSLContext;

import javax.inject.Inject;
import java.util.Optional;

import static lgonode.infrastructure.persistence.dao.OrderBooksDAO.*;

public class OrderBooksJooqRepository implements OrderBooksRepository {

    @Inject
    public OrderBooksJooqRepository(DSLContext dsl) {
        this.dsl = dsl;
    }

    @Override
    public void save(long batchId, String serializedOrderBooks) {
        this.dsl.insertInto(TABLE)
                .columns(BATCH_ID, CONTENT)
                .values(batchId, serializedOrderBooks)
                .execute();
    }

    @Override
    public Optional<OrderBookSnapshot> lastOrderBook() {
        return this.dsl.select(BATCH_ID, CONTENT)
                .from(TABLE)
                .orderBy(BATCH_ID.desc())
                .limit(1)
                .fetchOptionalInto(OrderBookSnapshot.class);
    }

    private final DSLContext dsl;
}
