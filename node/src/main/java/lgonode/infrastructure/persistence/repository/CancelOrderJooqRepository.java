package lgonode.infrastructure.persistence.repository;

import lgonode.domain.CancelOrder;
import lgonode.domain.CancelOrderRepository;
import org.jooq.DSLContext;

import javax.inject.Inject;
import java.util.List;

import static lgonode.infrastructure.persistence.dao.CancelOrderDAO.*;

public class CancelOrderJooqRepository implements CancelOrderRepository {

    @Inject
    public CancelOrderJooqRepository(DSLContext dsl) {
        this.dsl = dsl;
    }

    @Override
    public void save(List<CancelOrder> cancelOrderList) {
        var insert = this.dsl.insertInto(TABLE).columns(ID, BATCH_ID, KEY_ID, CREATION_DATE, ORDER_TO_CANCEL);
        for (CancelOrder cancelOrder : cancelOrderList) {
            insert.values(cancelOrder.id, cancelOrder.batchId, cancelOrder.keyId, cancelOrder.creationDate, cancelOrder.orderIdToCancel);
        }
        insert.execute();
    }

    private final DSLContext dsl;
}
