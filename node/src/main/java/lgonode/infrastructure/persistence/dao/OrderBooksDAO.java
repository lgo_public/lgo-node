package lgonode.infrastructure.persistence.dao;

import org.jooq.Field;
import org.jooq.Record;
import org.jooq.Table;
import org.jooq.impl.DSL;

public class OrderBooksDAO {
    public static Table<Record> TABLE = DSL.table("orderbooks_view");
    public static Field<Long> BATCH_ID = DSL.field("batch_id", Long.class);
    public static Field<String> CONTENT = DSL.field("content", String.class);
}