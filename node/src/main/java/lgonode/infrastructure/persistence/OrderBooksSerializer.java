package lgonode.infrastructure.persistence;

import lgonode.domain.Order;
import lgonode.domain.OrderBook;
import lgonode.domain.Pair;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.Version;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializerProvider;
import org.codehaus.jackson.map.module.SimpleModule;
import org.codehaus.jackson.map.ser.std.SerializerBase;

import java.io.IOException;
import java.util.Map;

public class OrderBooksSerializer {

    public OrderBooksSerializer() {
        mapper = new ObjectMapper();
        SimpleModule testModule = new SimpleModule("OrderBooksSerModule", new Version(1, 0, 0, null));
        testModule.addSerializer(Order.class, new OrderSerializer());
        testModule.addSerializer(OrderBook.class, new OrderBookSerializer());
        mapper.registerModule(testModule);
    }

    public String serialize(Map<Pair, OrderBook> orderBooks) throws IOException {
        return mapper.writeValueAsString(orderBooks);
    }

    private class OrderSerializer extends SerializerBase<Order> {

        OrderSerializer() {
            super(Order.class);
        }

        @Override
        public void serialize(Order value, JsonGenerator jgen, SerializerProvider provider) throws IOException {
            jgen.writeStartObject();
            jgen.writeNumberField("id", value.id);
            jgen.writeNumberField("qt", value.remainingQuantity());
            jgen.writeEndObject();
        }

    }

    private class OrderBookSerializer extends SerializerBase<OrderBook> {

        OrderBookSerializer() {
            super(OrderBook.class);
        }

        @Override
        public void serialize(OrderBook value, JsonGenerator jgen, SerializerProvider provider) throws IOException {
            jgen.writeStartObject();
            jgen.writeObjectField("B", value.side("B").getContent());
            jgen.writeObjectField("S", value.side("S").getContent());
            jgen.writeEndObject();
        }

    }

    private final ObjectMapper mapper;
}
