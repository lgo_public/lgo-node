package lgonode.infrastructure.persistence;

import lgonode.domain.*;
import lgonode.infrastructure.*;
import org.slf4j.*;

import javax.inject.Inject;
import java.util.Set;

public class BatchPersistenceStep implements BatchProcessingStep {

    @Inject
    public BatchPersistenceStep(BatchRepository batchRepository,
                                OrderRepository orderRepository,
                                FailedOrderRepository failedOrderRepository,
                                CancelOrderRepository cancelOrderRepository,
                                ProofRepository proofRepository,
                                Set<TradeRepository> tradeRepository,
                                OrderBooksRepository orderBooksRepository,
                                OrderBookEventRepository orderBookEventRepository) {
        this.batchRepository = batchRepository;
        this.orderRepository = orderRepository;
        this.failedOrderRepository = failedOrderRepository;
        this.cancelOrderRepository = cancelOrderRepository;
        this.proofRepository = proofRepository;
        this.tradeRepositories = tradeRepository;
        this.orderBooksRepository = orderBooksRepository;
        this.orderBookEventRepository = orderBookEventRepository;
    }

    @Override
    public ProcessingResult process(BatchProcessingContext context) {
        try {
            batchRepository.save(context.metadata());
            if (context.orderBookSerializedContent() != null) {
                orderBooksRepository.save(context.batchId(), context.orderBookSerializedContent());
            }
            orderRepository.save(context.orders());
            failedOrderRepository.save(context.failedOrders());
            cancelOrderRepository.save(context.cancelOrders());
            orderBookEventRepository.save(context.orderBookEvents());
            proofRepository.save(context.otsProof());
            for (TradeRepository tradeRepository : tradeRepositories) {
                tradeRepository.save(context.trades());
            }
        } catch (Exception e) {
            LOGGER.error("Persistence failure", e);
            return ProcessingResult.FAILURE;
        }
        return ProcessingResult.CONTINUE;
    }

    private final BatchRepository batchRepository;
    private final OrderRepository orderRepository;
    private final FailedOrderRepository failedOrderRepository;
    private final CancelOrderRepository cancelOrderRepository;
    private final ProofRepository proofRepository;
    private final Set<TradeRepository> tradeRepositories;
    private final OrderBooksRepository orderBooksRepository;
    private final OrderBookEventRepository orderBookEventRepository;
    private static final Logger LOGGER = LoggerFactory.getLogger(BatchPersistenceStep.class);
}
