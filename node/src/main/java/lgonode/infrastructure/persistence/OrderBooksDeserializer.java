package lgonode.infrastructure.persistence;

import lgonode.domain.*;
import lgonode.domain.configuration.*;
import org.codehaus.jackson.*;
import org.codehaus.jackson.map.*;
import org.codehaus.jackson.map.module.SimpleModule;
import org.codehaus.jackson.type.TypeReference;

import javax.inject.Inject;
import java.io.IOException;
import java.util.*;

public class OrderBooksDeserializer {

    @Inject
    public OrderBooksDeserializer(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public Map<Pair, OrderBook> deserialize(String content, PlatformConfiguration configuration) throws IOException {
        var mapper = new ObjectMapper();
        SimpleModule testModule = new SimpleModule("OrderBooksDeserModule", new Version(1, 0, 0, null));
        testModule.addDeserializer(Order.class, new OrderDeserializer(configuration));
        testModule.addDeserializer(OrderBook.class, new OrderBookDeserializer());
        testModule.addKeyDeserializer(Pair.class, new PairDeserializer(configuration));
        mapper.registerModule(testModule);
        return mapper.readValue(content, new TypeReference<Map<Pair, OrderBook>>() {});
    }

    private class OrderDeserializer extends JsonDeserializer<Order> {

        public OrderDeserializer(PlatformConfiguration configuration) {
            this.configuration = configuration;
        }

        @Override
        public Order deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
            JsonNode node = jp.readValueAsTree();
            long id = node.get("id").asLong();
            long qt = node.get("qt").asLong();
            Order order = orderRepository.load(id, configuration);
            order.setRemainingQuantity(qt);
            return order;
        }

        private final PlatformConfiguration configuration;
    }

    private static class PairDeserializer extends KeyDeserializer {

        public PairDeserializer(PlatformConfiguration configuration) {
            this.configuration = configuration;
        }

        @Override
        public Object deserializeKey(String key, DeserializationContext ctxt) throws IOException, JsonProcessingException {
            return ConfigurationHelper.toPair(key, configuration);
        }

        private final PlatformConfiguration configuration;
    }

    private static class OrderBookDeserializer extends JsonDeserializer<OrderBook> {
        @Override
        public OrderBook deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
            Map<String, TreeMap<Long, LinkedList<Order>>> value = jp.readValueAs(new TypeReference<Map<String, TreeMap<Long, LinkedList<Order>>>>(){});
            OrderBook orderBook = new OrderBook();
            orderBook.restoreBuySide(value.get("B"));
            orderBook.restoreSellSide(value.get("S"));
            return orderBook;
        }
    }

    private final OrderRepository orderRepository;
}
