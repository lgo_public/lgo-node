package lgonode.infrastructure;

public enum ProcessingResult {
    CONTINUE, FAILURE, WAIT
}
