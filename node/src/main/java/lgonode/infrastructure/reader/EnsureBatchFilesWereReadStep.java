package lgonode.infrastructure.reader;

import lgonode.infrastructure.BatchProcessingContext;
import lgonode.infrastructure.BatchProcessingStep;
import lgonode.infrastructure.ProcessingResult;

public class EnsureBatchFilesWereReadStep implements BatchProcessingStep {

    @Override
    public ProcessingResult process(BatchProcessingContext context) {
        if (context.metadata() == null) {
            return ProcessingResult.FAILURE;
        }
        if (context.metadata().orderCount != context.rawOrders().size()) {
            return ProcessingResult.FAILURE;
        }
        return ProcessingResult.CONTINUE;
    }

}
