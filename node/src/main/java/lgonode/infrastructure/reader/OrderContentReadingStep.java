package lgonode.infrastructure.reader;

import lgonode.domain.DecryptedOrder;
import lgonode.domain.configuration.ConfigurationHelper;
import lgonode.infrastructure.*;
import org.slf4j.*;

public class OrderContentReadingStep implements BatchProcessingStep {

    @Override
    public ProcessingResult process(BatchProcessingContext context) {
        for (DecryptedOrder order : context.decryptedOrders()) {
            try {
                if (contentReadable(order, context)) {
                    readData(order, context);
                }
            } catch (Exception e) {
                LOGGER.error("Not possible to read order content: "+order.data, e);
                return ProcessingResult.FAILURE;
            }
        }
        return ProcessingResult.CONTINUE;
    }

    private boolean contentReadable(DecryptedOrder order, BatchProcessingContext context) {
        boolean contentNotReadable = context.failedOrders().stream()
                .anyMatch(failedOrder -> failedOrder.id == order.id && failedOrder.reason.equals("INVALID_PAYLOAD"));
        return !contentNotReadable;
    }

    private void readData(DecryptedOrder order, BatchProcessingContext context) {
        if (order.data == null) {
            LOGGER.warn("Can't read order data for order: " + order.id);
            return;
        }
        String[] rawOrder = parseRawOrder(order.data);
        if (isOrder(rawOrder)) {
            parseOrder(order.id, order.keyId, rawOrder, context);
            return;
        }
        parseCancelOrder(order.id, order.keyId, rawOrder, context);
    }

    private String[] parseRawOrder(String order) {
        return order.split(",");
    }

    private boolean isOrder(String[] data) {
        return !isCancelOrder(data);
    }

    private boolean isCancelOrder(String[] orderType) {
        return "C".equals(orderType[0]);
    }

    private void parseOrder(long id, String keyId, String[] data, BatchProcessingContext context) {
        String orderType = data[0];
        String direction = data[1];
        String pairCode = data[2];
        String quantity = data[3];
        String price = data[4];
        long timestamp = parseTimestampFromId(id);
        var pair = ConfigurationHelper.toPair(pairCode, context.platformConfiguration());
        context.orderContentRead(
                id,
                keyId,
                timestamp,
                orderType,
                direction,
                pair,
                quantity,
                price
        );
    }

    private long parseTimestampFromId(long id) {
        String idAsString = Long.toString(id);
        String timestampAsString = idAsString.substring(0, idAsString.length() - 5);
        return Long.valueOf(timestampAsString);
    }

    private void parseCancelOrder(long id, String keyId, String[] data, BatchProcessingContext context) {
        context.cancelOrderContentRead(id, keyId, parseTimestampFromId(id), Long.parseLong(data[1]));
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(OrderContentReadingStep.class);

}