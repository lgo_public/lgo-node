package lgonode;

import lgonode.domain.*;
import lgonode.infrastructure.*;
import lgonode.infrastructure.matching.MatchingStep;
import org.jooq.DSLContext;
import org.slf4j.*;

import javax.inject.Inject;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicReference;

public class BatchesProcessingExecutor {

    @Inject
    public BatchesProcessingExecutor(BatchProcessor batchProcessor, BatchRepository batchRepository, DSLContext context, OrderBooksRepository orderBooksRepository, OrderBookEventRepository orderBookEventRepository, MatchingStep matchingStep) {
        this.batchProcessor = batchProcessor;
        this.batchRepository = batchRepository;
        this.context = context;
        this.orderBooksRepository = orderBooksRepository;
        this.orderBookEventRepository = orderBookEventRepository;
        this.matchingStep = matchingStep;
        this.shutdownRequested = false;
    }

    public void run() {
        Runtime.getRuntime().addShutdownHook(new ShutdownHandler());
        scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        scheduledFuture = scheduledExecutorService.scheduleWithFixedDelay(
                this::processBatches,
                0,
                5,
                TimeUnit.SECONDS);
    }

    private void processBatches() {
        try {
            batchProcessingContext = null;
            AtomicReference<ProcessingResult> processingResult = new AtomicReference<>();
            do {
                initiateContext();
                try {
                    context.transaction(() -> processingResult.set(processBatch()));
                } catch (Exception e) {
                    LOGGER.error("Can't process batch", e);
                    shutdownRequested = true;
                    shutdown();
                }
            } while (ProcessingResult.CONTINUE == processingResult.get() && !shutdownRequested);
            if (ProcessingResult.FAILURE == processingResult.get()) {
                shutdown();
            }
        } catch (Exception e) {
            LOGGER.error("ERROR", e);
            shutdown();
        }
    }

    private void shutdown() {
        scheduledFuture.cancel(true);
        scheduledExecutorService.shutdown();
    }

    private ProcessingResult processBatch() {
        LOGGER.info("Starting processing batch " + batchProcessingContext.batchId());
        ProcessingResult processingResult = batchProcessor.process(batchProcessingContext);
        LOGGER.info("End processing batch " + batchProcessingContext.batchId() + ", result : " + processingResult.name());
        return processingResult;
    }

    private void initiateContext() throws IOException {
        if (batchProcessingContext == null || batchProcessingContext.metadata() == null) {
            Metadata lastBatch = batchRepository.lastBatch();
            if (lastBatch == null) {
                batchProcessingContext = BatchProcessingContext.create(0, null);
            } else {
                LOGGER.info("Loading " + lastBatch.id + " end state...");
                batchProcessingContext = BatchProcessingContext.create(lastBatch.id + 1, lastBatch.hash);
                OrderBookSnapshot snapshot = orderBooksRepository.lastOrderBook().orElse(new OrderBookSnapshot(-1, "{}"));
                if (snapshot.batchId > lastBatch.id) {
                    throw new IllegalStateException("Last saved batch before last saved orderbooks, cannot retrieve order books state");
                }
                List<OrderBookEvent> events = orderBookEventRepository.loadEventsAfterBatch(snapshot.batchId);
                matchingStep.replay(lastBatch, snapshot.serializedContent, events);
            }
        } else {
            batchProcessingContext.nextBatch();
        }
    }

    private class ShutdownHandler extends Thread {
        @Override
        public void run() {
            LOGGER.info("Shutdown requested");
            BatchesProcessingExecutor.this.shutdownRequested = true;
            BatchesProcessingExecutor.this.shutdown();
        }
    }

    private final BatchProcessor batchProcessor;
    private final BatchRepository batchRepository;
    private DSLContext context;
    private final OrderBooksRepository orderBooksRepository;
    private final OrderBookEventRepository orderBookEventRepository;
    private final MatchingStep matchingStep;
    private boolean shutdownRequested;
    private ScheduledFuture<?> scheduledFuture;
    private ScheduledExecutorService scheduledExecutorService;
    private BatchProcessingContext batchProcessingContext = null;
    private static final Logger LOGGER = LoggerFactory.getLogger(BatchProcessor.class);
}
