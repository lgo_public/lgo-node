package lgonode.domain;

import java.util.*;

import static java.util.List.of;

public abstract class OrderBookSide {

    OrderBookSide(Comparator<Long> comparator) {
        content = new TreeMap<>(comparator);
    }

    List<Order> cancel(List<Long> canceledOrdersIds) {
        List<Long> keysToRemove = new ArrayList<>();
        List<Order> ordersRemoved = new ArrayList<>();
        for (Map.Entry<Long, LinkedList<Order>> entry : content.entrySet()) {
            entry.getValue().stream().filter(order -> canceledOrdersIds.contains(order.id)).forEach(ordersRemoved::add);
            entry.getValue().removeIf(order -> canceledOrdersIds.contains(order.id));
            if (entry.getValue().isEmpty()) {
                keysToRemove.add(entry.getKey());
            }
        }
        for (Long key : keysToRemove) {
            content.remove(key);
        }
        return ordersRemoved;
    }

    public void update(Order order) {
        for (Order o : content.get(order.price)) {
            if (o.id == order.id) {
                o.setRemainingQuantity(order.remainingQuantity());
                return;
            }
        }
    }

    public void cancel(Order order) {
        cancel(of(order.id));
    }

    public Optional<Order> topOrder() {
        return Optional.ofNullable(content.firstEntry())
                .map(Map.Entry::getValue)
                .filter(list -> !list.isEmpty())
                .map(LinkedList::getFirst);
    }

    public void removeTopOrder() {
        Map.Entry<Long, LinkedList<Order>> entry = content.firstEntry();
        content.firstEntry().getValue().remove();
        if (content.firstEntry().getValue().isEmpty()) {
            content.remove(entry.getKey());
        }
    }

    public void enter(Order order) {
        Optional.ofNullable(content.get(order.price))
                .ifPresentOrElse(
                        e -> e.add(order),
                        () -> content.put(order.price, new LinkedList<>(of(order))));
    }

    public TreeMap<Long, LinkedList<Order>> getContent() {
        return content;
    }

    void setContent(Long price, LinkedList<Order> orders) {
        content.put(price, orders);
    }

    private TreeMap<Long, LinkedList<Order>> content;
}
