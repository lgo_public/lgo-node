package lgonode.domain;

import java.util.Objects;

public class Pair {

    public Pair(Currency base, Currency quote) {
        this.base = base;
        this.quote = quote;
    }

    public static String pairKey(String base, String quote) {
        return base.toUpperCase() + "-" + quote.toUpperCase();
    }

    @Override
    public String toString() {
        return pairKey(base.code, quote.code);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pair pair = (Pair) o;
        return Objects.equals(base, pair.base) && Objects.equals(quote, pair.quote);
    }

    @Override
    public int hashCode() {
        return Objects.hash(base, quote);
    }

    public Currency base;
    public Currency quote;
}
