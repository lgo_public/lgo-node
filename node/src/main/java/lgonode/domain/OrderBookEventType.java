package lgonode.domain;

public enum OrderBookEventType {

    CANCEL_BOOK {
        @Override
        public OrderBookEvent restore(long batchId, String base, String quote, String payload) {
            return OrderBookCancelOrdersEvent.restore(batchId, base, quote);
        }
    }, UPDATE_ORDER {
        @Override
        public OrderBookEvent restore(long batchId, String base, String quote, String payload) {
            return OrderBookUpdateOrderEvent.restore(batchId, base, quote, payload);
        }
    }, ADD_ORDER {
        @Override
        public OrderBookEvent restore(long batchId, String base, String quote, String payload) {
            return OrderBookAddOrderEvent.restore(batchId, base, quote, payload);
        }
    }, REMOVE_ORDER {
        @Override
        public OrderBookEvent restore(long batchId, String base, String quote, String payload) {
            return OrderBookRemoveOrderEvent.restore(batchId, base, quote, payload);
        }
    };

    public abstract OrderBookEvent restore(long batchId, String base, String quote, String payload);
}
