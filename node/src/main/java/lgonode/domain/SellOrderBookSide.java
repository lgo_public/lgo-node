package lgonode.domain;

import java.util.Comparator;

public class SellOrderBookSide extends OrderBookSide {

    SellOrderBookSide() {
        super(Comparator.naturalOrder());
    }

}
