package lgonode.domain;

import java.util.Optional;

public interface OrderBooksRepository {
    void save(long batchId, String serializedOrderBooks);
    Optional<OrderBookSnapshot> lastOrderBook();
}
