package lgonode.domain;

import java.util.List;

public interface TradeRepository {
    void save(List<Trade> tradeList);
}
