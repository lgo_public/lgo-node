package lgonode.domain;

public class OrderBookSnapshot {

    public OrderBookSnapshot(long batchId, String serializedContent) {
        this.batchId = batchId;
        this.serializedContent = serializedContent;
    }

    public final long batchId;
    public final String serializedContent;
}
