package lgonode.domain;

public class Trade {

    public Trade(long batchId, long price, long quantity, Pair pair, long timestamp) {
        this.batchId = batchId;
        this.price = price;
        this.quantity = quantity;
        this.pair = pair;
        this.timestamp = timestamp;
    }

    public Trade(int id, long batchId, String price, String quantity, Pair pair, long timestamp) {
        this.id = id;
        this.batchId = batchId;
        this.price = pair.quote.parse(price);
        this.quantity = pair.base.parse(quantity);
        this.pair = pair;
        this.timestamp = timestamp;
    }

    public long amount() {
        return Price.total(price, quantity, pair);
    }

    public String formatPrice() {
        return pair.quote.format(price);
    }

    public String formatQuantity() {
        return pair.base.format(quantity);
    }

    public int id;
    public long batchId;
    public long price;
    public long quantity;
    public Pair pair;
    public final long timestamp;
}
