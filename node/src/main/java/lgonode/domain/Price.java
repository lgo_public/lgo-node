package lgonode.domain;

import org.decimal4j.api.DecimalArithmetic;
import org.decimal4j.scale.Scales;

public final class Price {

    private Price() {
    }

    public static long total(long unitPrice, long qt, Pair pair) {
        DecimalArithmetic priceArith = Scales.getScaleMetrics(pair.quote.scale).getDefaultArithmetic();
        return priceArith.multiplyByUnscaled(unitPrice, qt, pair.base.scale);
    }

    public static long quantity(long unitPrice, long amount, Pair pair) {
        DecimalArithmetic priceArith = Scales.getScaleMetrics(pair.quote.scale).getDefaultArithmetic();
        return priceArith.divideByUnscaled(amount, unitPrice, pair.base.scale);
    }
}
