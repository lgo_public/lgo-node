package lgonode.domain;

public class OrderBookAddOrderEvent extends OrderBookEvent {

    public OrderBookAddOrderEvent(long batchId, String base, String quote, String side, long price, long orderId, long quantity) {
        super(batchId, base, quote);
        this.side = side;
        this.price = price;
        this.orderId = orderId;
        this.quantity = quantity;
    }

    @Override
    public OrderBookEventType type() {
        return OrderBookEventType.ADD_ORDER;
    }

    @Override
    public String payload() {
        return side + "," + price + "," + orderId + "," + quantity;
    }

    public static OrderBookEvent restore(long batchId, String base, String quote, String payload) {
        String[] split = payload.split(",");
        String side = split[0];
        long price = Long.parseLong(split[1]);
        long orderId = Long.parseLong(split[2]);
        long quantity = Long.parseLong(split[3]);
        return new OrderBookAddOrderEvent(batchId, base, quote, side, price, orderId, quantity);
    }

    public final String side;
    public final long price;
    public final long orderId;
    public final long quantity;
}
