package lgonode.domain;

import lgonode.domain.configuration.PlatformConfiguration;

import java.util.List;

public interface OrderRepository {
    void save(List<Order> orders);
    Order load(long id, PlatformConfiguration configuration);
}
