package lgonode.domain;

import java.util.List;

public interface OrderBookEventRepository {
    void save(List<OrderBookEvent> eventList);
    List<OrderBookEvent> loadEventsAfterBatch(long batchId);
}
