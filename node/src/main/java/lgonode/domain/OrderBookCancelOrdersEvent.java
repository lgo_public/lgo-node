package lgonode.domain;

public class OrderBookCancelOrdersEvent extends OrderBookEvent {

    public OrderBookCancelOrdersEvent(long batchId, String base, String quote) {
        super(batchId, base, quote);
    }

    @Override
    public OrderBookEventType type() {
        return OrderBookEventType.CANCEL_BOOK;
    }

    @Override
    public String payload() {
        return "";
    }

    public static OrderBookEvent restore(long batchId, String base, String quote) {
        return new OrderBookCancelOrdersEvent(batchId, base, quote);
    }
}
