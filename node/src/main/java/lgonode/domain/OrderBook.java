package lgonode.domain;

import java.util.*;
import java.util.stream.Collectors;

public class OrderBook {

    public OrderBook() {
        sides.put("B", new BuyOrderBookSide());
        sides.put("S", new SellOrderBookSide());
    }

    public List<Order> cancel(List<Long> canceledOrdersIds) {
        return sides.values().stream().map(side -> side.cancel(canceledOrdersIds)).flatMap(List::stream).collect(Collectors.toList());
    }

    public OrderBookSide side(String direction) {
        return sides.get(direction);
    }

    public OrderBookSide counterSide(String direction) {
        return sides.get(direction.equals("B") ? "S" : "B");
    }

    private Map<String, OrderBookSide> sides = new HashMap<>();

    public void restoreSellSide(TreeMap<Long, LinkedList<Order>> content) {
        OrderBookSide side = side("S");
        for (long price : content.keySet()) {
            side.setContent(price, content.get(price));
        }
    }

    public void restoreBuySide(TreeMap<Long, LinkedList<Order>> content) {
        OrderBookSide side = side("B");
        for (Long price : content.keySet()) {
            side.setContent(price, content.get(price));
        }
    }

    public void apply(OrderBookUpdateOrderEvent event, Order order) {
        order.setRemainingQuantity(event.quantity);
        sides.get(event.side).update(order);
    }

    public void apply(OrderBookAddOrderEvent event, Order order) {
        order.setRemainingQuantity(event.quantity);
        sides.get(event.side).enter(order);
    }

    public void apply(OrderBookRemoveOrderEvent event, Order order) {
        sides.get(event.side).cancel(order);
    }

}
