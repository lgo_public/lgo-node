package lgonode.domain;

import com.google.common.base.Charsets;
import com.google.common.hash.Hashing;

public class EncryptedOrder extends RawOrder {

    public EncryptedOrder(long id, long batchId, String keyId, byte[] data) {
        super(id, batchId, data);
        this.keyId = keyId;
    }

    @Override
    public boolean isEncrypted() {
        return true;
    }

    @Override
    public String hash() {
        return Hashing.sha256().newHasher()
                .putLong(id)
                .putString(keyId, Charsets.UTF_8)
                .putBytes(data).hash().toString();
    }

    public final String keyId;
}
