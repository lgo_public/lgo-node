package lgonode.domain;

public abstract class OrderBookEvent {

    public OrderBookEvent(long batchId, String base, String quote) {
        this.batchId = batchId;
        this.base = base;
        this.quote = quote;
    }

    public abstract OrderBookEventType type();
    public abstract String payload();

    public String product() {
        return Pair.pairKey(base, quote);
    }


    public final long batchId;
    public final String base;
    public final String quote;
}
