package lgonode.domain;

public class ProductLimits {

    public ProductLimits(Pair pair, String baseMin, String baseMax, String quoteMin, String quoteMax, String totalMin, String totalMax) {
        this.baseLimit = new Limit(pair.base.parse(baseMin), pair.base.parse(baseMax));
        this.quoteLimit = new Limit(pair.quote.parse(quoteMin), pair.quote.parse(quoteMax));
        this.totalLimit = new Limit(pair.quote.parse(totalMin), pair.quote.parse(totalMax));
    }

    public class Limit {
        final long max;
        final long min;

        private Limit(long min, long max) {
            this.max = max;
            this.min = min;
        }

        public boolean valid(long quantity) {
            return quantity <= max && quantity >= min;
        }
    }

    public final Limit baseLimit;
    public final Limit quoteLimit;
    public final Limit totalLimit;
}
