package lgonode.domain;

import java.util.List;

public interface FailedOrderRepository {
    void save(List<FailedOrder> failedOrders);
}
