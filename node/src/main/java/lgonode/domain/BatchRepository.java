package lgonode.domain;

public interface BatchRepository {
    void save(Metadata metadata);
    Metadata lastBatch();
}
