package lgonode.domain;

import java.util.Comparator;

public class BuyOrderBookSide extends OrderBookSide {

    BuyOrderBookSide() {
        super(Comparator.reverseOrder());
    }

}
