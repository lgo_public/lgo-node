package lgonode.domain;

import org.decimal4j.api.DecimalArithmetic;
import org.decimal4j.scale.*;

import java.util.Objects;

public class Currency {

    public Currency(String code, int scale) {
        this.code = code;
        this.scale = scale;
        this.arithmetic = Scales.getScaleMetrics(scale).getDefaultArithmetic();
    }

    public String format(long unscaledValue) {
        return arithmetic.toString(unscaledValue);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Currency currency = (Currency) o;
        return Objects.equals(code.toUpperCase(), currency.code.toUpperCase());
    }

    @Override
    public int hashCode() {
        return Objects.hash(code.toUpperCase());
    }

    public long parse(String doubleValue) {
        ScaleMetrics scaleMetrics = Scales.getScaleMetrics(scale);
        return scaleMetrics.getDefaultArithmetic().parse(doubleValue);
    }

    public final String code;
    final int scale;

    private final DecimalArithmetic arithmetic;

}
