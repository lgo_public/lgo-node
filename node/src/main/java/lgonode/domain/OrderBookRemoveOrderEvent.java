package lgonode.domain;

public class OrderBookRemoveOrderEvent extends OrderBookEvent {

    public OrderBookRemoveOrderEvent(long batchId, String base, String quote, String side, long price, long orderId) {
        super(batchId, base, quote);
        this.side = side;
        this.price = price;
        this.orderId = orderId;
    }

    @Override
    public OrderBookEventType type() {
        return OrderBookEventType.REMOVE_ORDER;
    }

    @Override
    public String payload() {
        return side + "," + price + "," + orderId;
    }

    public static OrderBookEvent restore(long batchId, String base, String quote, String content) {
        String[] split = content.split(",");
        String side = split[0];
        long price = Long.parseLong(split[1]);
        long orderId = Long.parseLong(split[2]);
        return new OrderBookRemoveOrderEvent(batchId, base, quote, side, price, orderId);
    }

    public final String side;
    public final long price;
    public final long orderId;
}
