package lgonode.domain.configuration;

import lgonode.domain.*;

public class ConfigurationHelper {

    public static PlatformConfiguration defaultConfiguration(long batchId) {
        if (batchId < 65598189) {
            return DEFAULT_BEFORE_65598189;
        }
        return DEFAULT_AFTER__65598189;
    }

    public static Pair toPair(String product, PlatformConfiguration platformConfiguration) {
        var productConfiguration = platformConfiguration.products.get(product);
        var pairId = productConfiguration.id.split("-");
        var baseConfiguration = platformConfiguration.currencies.get(pairId[0]);
        var quoteConfiguration = platformConfiguration.currencies.get(pairId[1]);
        return toPair(baseConfiguration, quoteConfiguration);
    }

    public static ProductLimits toProductLimits(String product, PlatformConfiguration platformConfiguration) {
        var productConfiguration = platformConfiguration.products.get(product);
        var pairId = productConfiguration.id.split("-");
        var baseConfiguration = platformConfiguration.currencies.get(pairId[0]);
        var quoteConfiguration = platformConfiguration.currencies.get(pairId[1]);
        return toProductLimits(productConfiguration, baseConfiguration, quoteConfiguration);
    }

    private static ProductLimits toProductLimits(ProductConfiguration productConfiguration, CurrencyConfiguration baseConfiguration, CurrencyConfiguration quoteConfiguration) {
        return new ProductLimits(
                toPair(baseConfiguration, quoteConfiguration),
                productConfiguration.base.limits.min,
                productConfiguration.base.limits.max,
                productConfiguration.quote.limits.min,
                productConfiguration.quote.limits.max,
                productConfiguration.total.limits.min,
                productConfiguration.total.limits.max
        );
    }

    private static Pair toPair(CurrencyConfiguration baseConfiguration, CurrencyConfiguration quoteConfiguration) {
        return new Pair(toCurrency(baseConfiguration), toCurrency(quoteConfiguration));
    }

    private static Currency toCurrency(CurrencyConfiguration configuration) {
        return new Currency(configuration.code, configuration.scale);
    }

    public static final PlatformConfiguration DEFAULT_BEFORE_65598189;
    public static final PlatformConfiguration DEFAULT_AFTER__65598189;
    static {
        DEFAULT_BEFORE_65598189 = new PlatformConfiguration();
        DEFAULT_AFTER__65598189 = new PlatformConfiguration();

        DEFAULT_BEFORE_65598189.products.put("BTC-USD", new ProductConfiguration("BTC-USD", "0.001", "1000","10", "1000000", "10", "50000000"));
        DEFAULT_BEFORE_65598189.products.put("LGO-USD", new ProductConfiguration("LGO-USD", "500", "10000000","0.001", "100", "10", "3000000"));
        DEFAULT_BEFORE_65598189.products.put("TLGO1-TLGO2", new ProductConfiguration("TLGO1-TLGO2","0.001", "1000","10", "1000000", "10", "50000000"));
        DEFAULT_BEFORE_65598189.currencies.put("BTC", new CurrencyConfiguration("BTC", 8));
        DEFAULT_BEFORE_65598189.currencies.put("LGO", new CurrencyConfiguration("LGO", 8));
        DEFAULT_BEFORE_65598189.currencies.put("USD", new CurrencyConfiguration("USD", 4));
        DEFAULT_BEFORE_65598189.currencies.put("TLGO1", new CurrencyConfiguration("TLGO1", 8));
        DEFAULT_BEFORE_65598189.currencies.put("TLGO2", new CurrencyConfiguration("TLGO2", 4));
        DEFAULT_BEFORE_65598189.currencies.put("USDT.P", new CurrencyConfiguration("USDT.P", 4));
        DEFAULT_BEFORE_65598189.currencies.put("USD.P", new CurrencyConfiguration("USD.P", 4));

        DEFAULT_AFTER__65598189.products.putAll(DEFAULT_BEFORE_65598189.products);
        DEFAULT_AFTER__65598189.currencies.putAll(DEFAULT_BEFORE_65598189.currencies);

        DEFAULT_BEFORE_65598189.products.put("USDT.P-USD.P", new ProductConfiguration("USDT.P-USD.P", "10", "1000000", "0.5", "1.5", "10", "2000000"));
        DEFAULT_AFTER__65598189.products.put("USDT.P-USD.P", new ProductConfiguration("USDT.P-USD.P", "1",  "1000000", "0.5", "1.5", "1",  "2000000"));
    }

    private ConfigurationHelper() {}

}
