package lgonode.domain.configuration;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.*;

@SuppressWarnings("unused")
@JsonIgnoreProperties(ignoreUnknown = true)
public class PlatformConfiguration {

    /**
     * Without these setters Jackson replaces the original treemap by it's own that does not
     * contain the CASE_INSENSITIVE_ORDER configuration
     */
    public void setProducts(TreeMap<String, ProductConfiguration> products) {
        this.products.putAll(products);
    }
    public void setCurrencies(TreeMap<String, CurrencyConfiguration> currencies) {
        this.currencies.putAll(currencies);
    }

    public final Map<String, ProductConfiguration> products = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
    public final Map<String, CurrencyConfiguration> currencies = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
}
