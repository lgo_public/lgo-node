package lgonode.domain.configuration;

public class CurrencyLimits {

    public CurrencyLimits() {
    }

    public CurrencyLimits(String min, String max) {
        this.min = min;
        this.max = max;
    }

    public String min;
    public String max;
}
