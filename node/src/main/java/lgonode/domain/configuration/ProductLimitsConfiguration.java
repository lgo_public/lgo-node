package lgonode.domain.configuration;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductLimitsConfiguration {

    public ProductLimitsConfiguration() {
    }

    public ProductLimitsConfiguration(CurrencyLimits limits) {
        this.limits = limits;
    }

    public CurrencyLimits limits;
}
