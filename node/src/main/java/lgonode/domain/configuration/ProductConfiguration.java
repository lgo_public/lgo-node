package lgonode.domain.configuration;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductConfiguration {

    public ProductConfiguration() {
    }

    public ProductConfiguration(String id, String baseMin, String baseMax, String quoteMin, String quoteMax, String totalMin, String totalMax) {
        this.id = id;
        this.total = new ProductLimitsConfiguration(new CurrencyLimits(totalMin, totalMax));
        this.base = new ProductLimitsConfiguration(new CurrencyLimits(baseMin, baseMax));;
        this.quote = new ProductLimitsConfiguration(new CurrencyLimits(quoteMin, quoteMax));;
    }

    public String id;
    public ProductLimitsConfiguration total;
    public ProductLimitsConfiguration base;
    public ProductLimitsConfiguration quote;
}
