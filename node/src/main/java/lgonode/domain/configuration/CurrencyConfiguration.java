package lgonode.domain.configuration;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CurrencyConfiguration {

    public CurrencyConfiguration() {
    }

    public CurrencyConfiguration(String code, int scale) {
        this.code = code;
        this.scale = scale;
    }

    public String code;
    public int scale;
}
