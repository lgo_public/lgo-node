package lgonode.domain.configuration;

public interface ConfigurationLoader {

    PlatformConfiguration load(int configVersion, String configHash);
}
