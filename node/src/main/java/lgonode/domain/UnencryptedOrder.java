package lgonode.domain;

import com.google.common.hash.Hashing;

public class UnencryptedOrder extends RawOrder {

    public UnencryptedOrder(long id, long batchId, byte[] data) {
        super(id, batchId, data);
    }

    @Override
    public boolean isEncrypted() {
        return false;
    }

    @Override
    public String hash() {
        return Hashing.sha256().newHasher()
                .putLong(id)
                .putBytes(data).hash().toString();
    }

}
