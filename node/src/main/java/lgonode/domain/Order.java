package lgonode.domain;

public class Order {

    public Order(long id, long batchId, String keyId, long creationDate, String type, String direction, Pair pair, String quantity, String price) {
        this.id = id;
        this.batchId = batchId;
        this.keyId = keyId;
        this.type = type;
        this.direction = direction;
        this.pair = pair;
        this.quantity = parseQuantity(quantity, type, direction, this.pair.base, this.pair.quote);
        this.price = parsePrice(price, type, this.pair.quote);
        this.creationDate = creationDate;
    }

    private static long parseQuantity(String quantity, String type, String direction, Currency baseCurrency, Currency quoteCurrency) {
        if (type.equals("M") && direction.equals("B")) {
            return quoteCurrency.parse(quantity);
        }
        return baseCurrency.parse(quantity);
    }

    private static Long parsePrice(String price, String type, Currency quoteCurrency) {
        return type.equals("M") ? null : quoteCurrency.parse(price);
    }

    public String formatPrice() {
        return price == null ? null : pair.quote.format(price);
    }

    public String formatQuantity() {
        return isMarketBuy() ? pair.quote.format(quantity) : pair.base.format(quantity);
    }

    public String formatRemainingQuantity() {
        return isMarketBuy() ? pair.quote.format(remainingQuantity()) : pair.base.format(remainingQuantity());
    }

    private boolean isMarketBuy() {
        return type.equals("M") && direction.equals("B");
    }

    public long remainingQuantity() {
        return quantity - filledQuantity;
    }

    public long remainingAmount() {
        return Price.total(price, remainingQuantity(), pair);
    }

    public void consume(long quantity) {
        this.filledQuantity += quantity;
    }

    public void setRemainingQuantity(long remainingQuantity) {
        this.filledQuantity = this.quantity - remainingQuantity;
    }

    public final long id;
    public final long batchId;
    public final Pair pair;
    public final String keyId;
    public final String type;
    public final String direction;
    public final long quantity;
    private long filledQuantity = 0;
    public final Long price;
    public final long creationDate;
}
