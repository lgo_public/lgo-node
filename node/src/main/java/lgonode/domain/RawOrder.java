package lgonode.domain;

public abstract class RawOrder {

    public RawOrder(long id, long batchId, byte[] data) {
        this.id = id;
        this.batchId = batchId;
        this.data = data;
    }

    public abstract boolean isEncrypted();
    public abstract String hash();

    public final long id;
    public final long batchId;
    public final byte[] data;

}
