package lgonode.configuration;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import lgonode.infrastructure.influxDB.InfluxDBConnection;
import org.cfg4j.provider.ConfigurationProvider;
import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InfluxModule extends AbstractModule {

    @Provides
    @Singleton
    public InfluxDBConnection influxDB(ConfigurationProvider configurationProvider) {
        String url = configurationProvider.getProperty("influxdb.url", String.class);
        String user = configurationProvider.getProperty("influxdb.user", String.class);
        String password = configurationProvider.getProperty("influxdb.password", String.class);
        String database = configurationProvider.getProperty("influxdb.database", String.class);

        LOGGER.info("Looking for influxdb on url " + url);

        InfluxDB influxDB = InfluxDBFactory.connect(url, user, password);
        influxDB.setDatabase(database);
        return new InfluxDBConnection(influxDB, database);
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(InfluxModule.class);

}
