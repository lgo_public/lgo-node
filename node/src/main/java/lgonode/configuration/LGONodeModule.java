package lgonode.configuration;

import com.google.inject.*;
import com.google.inject.multibindings.Multibinder;
import lgonode.domain.*;
import lgonode.domain.configuration.ConfigurationLoader;
import lgonode.infrastructure.*;
import lgonode.infrastructure.avro.BatchFilesReadingStep;
import lgonode.infrastructure.chain.*;
import lgonode.infrastructure.decipher.*;
import lgonode.infrastructure.file.BatchFilesPresenceVerificationStep;
import lgonode.infrastructure.gcs.*;
import lgonode.infrastructure.matching.MatchingStep;
import lgonode.infrastructure.persistence.*;
import lgonode.infrastructure.persistence.repository.*;
import lgonode.infrastructure.reader.*;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.cfg4j.provider.ConfigurationProvider;

import javax.inject.Named;
import java.security.Security;

public class LGONodeModule extends AbstractModule {

    @Override
    public void configure() {
        bind(BatchRepository.class).to(BatchJooqRepository.class).in(Singleton.class);
        bind(CancelOrderRepository.class).to(CancelOrderJooqRepository.class).in(Singleton.class);
        bind(FailedOrderRepository.class).to(FailedOrderJooqRepository.class).in(Singleton.class);
        bind(OrderRepository.class).to(OrderJooqRepository.class).in(Singleton.class);
        bind(ProofRepository.class).to(ProofJooqRepository.class).in(Singleton.class);
        bind(OrderBooksRepository.class).to(OrderBooksJooqRepository.class).in(Singleton.class);
        bind(OrderBookEventRepository.class).to(OrderBookEventJooqRepository.class).in(Singleton.class);
        Multibinder<TradeRepository> multibinder = Multibinder.newSetBinder(binder(), TradeRepository.class);
        multibinder.addBinding().to(TradeInfluxRepository.class).in(Singleton.class);
        multibinder.addBinding().to(TradeJooqRepository.class).in(Singleton.class);
        bind(GCSKeyBucketReader.class).in(Singleton.class);
        bind(KeyRetriever.class).in(Singleton.class);
        bind(OrderBooksSerializer.class).in(Singleton.class);
        bind(OrderBooksDeserializer.class).in(Singleton.class);
        bind(MatchingStep.class).in(Singleton.class);
        bind(ConfigurationLoader.class).to(GCSConfigurationLoader.class).in(Singleton.class);
        configureBatchProcessor();
        Security.addProvider(new BouncyCastleProvider());
    }

    private void configureBatchProcessor() {
        bind(OrderContentReadingStep.class).in(Singleton.class);
        Multibinder<BatchProcessingStep> multibinder = Multibinder.newSetBinder(binder(), BatchProcessingStep.class);
        multibinder.addBinding().to(EnsurePreviousBatchWasSavedStep.class);
        multibinder.addBinding().to(BatchFilesPresenceVerificationStep.class);
        multibinder.addBinding().to(BatchFilesReadingStep.class);
        multibinder.addBinding().to(EnsureBatchFilesWereReadStep.class);
        multibinder.addBinding().to(BatchHashValidationStep.class);
        multibinder.addBinding().to(BatchChainingValidationStep.class);
        multibinder.addBinding().to(EnsurePrivateKeysAreReleasedStep.class);
        multibinder.addBinding().to(LoadBatchConfigurationStep.class);
        multibinder.addBinding().to(OrderDecryptionStep.class);
        multibinder.addBinding().to(OrderContentReadingStep.class);
        multibinder.addBinding().toProvider(binder().getProvider(MatchingStep.class));
        multibinder.addBinding().to(BatchPersistenceStep.class);
        //multibinder.addBinding().to(BatchFilesCleaningStep.class);
        bind(BatchProcessor.class).in(Singleton.class);
    }


    @Provides
    @Singleton
    @Named("storage.batch.path")
    public String batchStoragePath(ConfigurationProvider provider) {
        return provider.getProperty("storage.batch.path", String.class);
    }


    @Provides
    @Singleton
    @Named("storage.key.path")
    public String keyStoragePath(ConfigurationProvider provider) {
        return provider.getProperty("storage.key.path", String.class);
    }

    @Provides
    @Singleton
    public GCSConfiguration gcsConfiguration(ConfigurationProvider configurationProvider) {
        return configurationProvider.bind("gcs.bucket", GCSConfiguration.class);
    }
}
