CREATE TABLE orderbooks_view (
  batch_id      BIGINT NOT NULL PRIMARY KEY,
  content       TEXT
);

ALTER TABLE orderbooks_view ADD CONSTRAINT orderbooks_view_batch_id_fk FOREIGN KEY ( batch_id ) REFERENCES batch_view ( id ) ON DELETE CASCADE;
