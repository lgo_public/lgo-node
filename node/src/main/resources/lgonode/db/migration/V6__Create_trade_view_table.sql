CREATE TABLE trade_view (
  id              SERIAL PRIMARY KEY,
  batch_id        BIGINT NOT NULL,
  creation_date   BIGINT NOT NULL,
  base_currency   VARCHAR(242) NOT NULL,
  quote_currency  VARCHAR(242) NOT NULL,
  quantity        VARCHAR(242) NOT NULL,
  price           VARCHAR(242)
);

ALTER TABLE trade_view ADD CONSTRAINT trade_view_batch_id_fk FOREIGN KEY ( batch_id ) REFERENCES batch_view ( id ) ON DELETE CASCADE;
