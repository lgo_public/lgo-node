create index orderbookevents_view_batch_id_base_quote_index
    on orderbookevents_view (batch_id, base_currency, quote_currency);
create index order_view_batch_id_index
    on order_view (batch_id);
create index failed_order_view_batch_id_index
    on failed_order_view (batch_id);
create index cancel_order_view_batch_id_index
    on cancel_order_view (batch_id);
create index trade_view_batch_id_index
    on trade_view (batch_id);
create index orderbooks_view_batch_id_index
    on orderbooks_view (batch_id);

