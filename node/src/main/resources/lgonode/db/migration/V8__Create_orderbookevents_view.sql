CREATE TABLE orderbookevents_view (
  id              SERIAL PRIMARY KEY,
  batch_id        BIGINT NOT NULL,
  base_currency   VARCHAR(10) NOT NULL,
  quote_currency  VARCHAR(10) NOT NULL,
  type            VARCHAR(20) NOT NULL,
  payload         VARCHAR(100)
);
