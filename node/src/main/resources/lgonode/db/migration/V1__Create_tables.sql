CREATE TABLE batch_view (
  id              BIGINT NOT NULL PRIMARY KEY,
  hash            VARCHAR(242) NOT NULL,
  previous_hash   VARCHAR(242) NOT NULL,
  order_count     INT NOT NULL
);

CREATE TABLE order_view (
  id              BIGINT NOT NULL PRIMARY KEY,
  batch_id        BIGINT NOT NULL,
  key_id          VARCHAR(242) NOT NULL,
  creation_date   BIGINT NOT NULL,
  type            VARCHAR(242) NOT NULL,
  direction       VARCHAR(242) NOT NULL,
  base_currency   VARCHAR(242) NOT NULL,
  quote_currency  VARCHAR(242) NOT NULL,
  quantity        VARCHAR(242) NOT NULL,
  price           VARCHAR(242)
);

CREATE TABLE failed_order_view (
  id              BIGINT NOT NULL PRIMARY KEY,
  batch_id        BIGINT NOT NULL,
  reason          VARCHAR(242),
  details         VARCHAR(2500)
);

CREATE TABLE cancel_order_view (
  id              BIGINT NOT NULL PRIMARY KEY,
  batch_id        BIGINT NOT NULL,
  key_id          VARCHAR(242) NOT NULL,
  creation_date   BIGINT NOT NULL,
  order_to_cancel BIGINT NOT NULL
);

