CREATE TABLE proof_view (
  batch_id              BIGINT NOT NULL PRIMARY KEY,
  timestamped_hash   VARCHAR(242) NOT NULL,
  content            bytea NOT NULL
);
