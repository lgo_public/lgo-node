#!/usr/bin/env bash
set -e

ROOT="$(cd "$(dirname "${BASH_SOURCE[0]}")"; pwd)"
source "${ROOT}/common/env.sh"

SLEEP_TIME_IN_SEC=3600

main() {
  while [[ : ]]; do
    local last_cleaned_batch=$(cat "$CLEANING_CHECKPOINT" || echo 0)
    echo "Last cleaned batch: ${last_cleaned_batch}"

    local downloader_checkpoint=$(cat "$CHECKPOINT")
    echo "Last downloaded batch: ${downloader_checkpoint}"

    local last_analyzed_batch=$(docker exec -it services_postgres_1 psql -d lgonode -Ulgo -t -c "SET search_path TO lgonode; SELECT id from batch_view order by id desc limit 1;" | sed 's/[^0-9]*//g')
    echo "Last analyzed batch: ${last_analyzed_batch}"

    local max_deletable_batch
    [[ $last_analyzed_batch -lt $downloader_checkpoint ]] && max_deletable_batch="$last_analyzed_batch" || max_deletable_batch="$downloader_checkpoint"
    echo "Cleaning from ${last_cleaned_batch} to ${max_deletable_batch}"

    for i in `seq ${last_cleaned_batch} ${max_deletable_batch}`;
     do
       directory=$(printf "%010d" ${i})
       rm -r ${DEST}/$directory || true
       if [[ $(($i % 100)) -eq 0 ]]; then
          echo "Saving cleaning checkpoint at batch ${i}"
          echo ${i} >| "$CLEANING_CHECKPOINT"
       fi
     done

    echo "Saving cleaning checkpoint at batch ${max_deletable_batch}"
    echo ${max_deletable_batch} >| "$CLEANING_CHECKPOINT"

    echo -e "Waiting ${SLEEP_TIME_IN_SEC}sec...\n"
    sleep $SLEEP_TIME_IN_SEC
  done
}



main