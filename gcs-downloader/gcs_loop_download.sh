#!/usr/bin/env bash
set -e

ROOT="$(cd "$(dirname "${BASH_SOURCE[0]}")"; pwd)"
source "${ROOT}/common/env.sh"
source "${ROOT}/common/gcs_common.sh"
source "${ROOT}/common/gcs_check_and_repair.sh"
source "${ROOT}/common/gcs_archives_download.sh"

main() {
    CURRENT_BATCH=$(cat "$CHECKPOINT" || echo 0)
    echo "Starting from ${CURRENT_BATCH}"

    if [[ -d "$DEST/0000000000" || -f "$CLEANING_CHECKPOINT" ]]; then
        check_and_repair
    else
        create_dest_directory
        archives_download
        retrieve_actual_batch_id
    fi

    _save_checkpoint

    while [[ : ]]; do
        _download_last_upgraded_batch_txt_file
        echo "Current batch: ${CURRENT_BATCH}, last upgraded batch: ${LAST_UPGRADED_BATCH}"

        if [[ ${CURRENT_BATCH} -lt ${LAST_UPGRADED_BATCH} ]]; then
            _download_new_batch_directories
            CURRENT_BATCH=$(cat "$CHECKPOINT")
            check_and_repair
            _save_checkpoint
        fi

        echo -e "Waiting 60sec...\n"
        sleep 60
    done

}

_download_last_upgraded_batch_txt_file() {
    download "${BUCKET}/last_upgraded_batch.txt"
    LAST_UPGRADED_BATCH=$(cat "$DEST/last_upgraded_batch.txt" || echo 0)
}

_save_checkpoint() {
    echo "Saving checkpoint at batch ${CURRENT_BATCH}"
    echo ${CURRENT_BATCH} >| "$CHECKPOINT"
}

_download_new_batch_directories() {
    echo "Downloading new files..."

    # Download one by one until 10 multiple
    while [[ $(($CURRENT_BATCH % 10)) -ne 0 && ${CURRENT_BATCH} -lt ${LAST_UPGRADED_BATCH} ]]; do
      local padded=$(padded_batch_id)
      download "${BUCKET}/${padded}" || return 0
      CURRENT_BATCH=$(($CURRENT_BATCH+1))
    done
    # Download 10 by 10 until 100 multiple
    while [[ $(($CURRENT_BATCH % 100)) -ne 0 && ${CURRENT_BATCH} -lt ${LAST_UPGRADED_BATCH} ]]; do
      local padded=$(padded_batch_id)
      download "${BUCKET}/${padded%?}*" || return 0
      CURRENT_BATCH=$(($CURRENT_BATCH+10))
    done
    # Download 100 by 100
    while [[ ${CURRENT_BATCH} -lt ${LAST_UPGRADED_BATCH} ]] ; do
      local padded=$(padded_batch_id)
      download "${BUCKET}/${padded%??}*" || return 0
      CURRENT_BATCH=$(($CURRENT_BATCH+100))
    done
}

main