
check_and_repair() {
    if [[ -d "$DEST" ]]; then
      echo Checking existing content...

      local max_existing_dir=$(ls -I last_upgraded_batch.txt "${DEST}" | sort -r | head -1)
      local max_existing_batch=$(echo "${max_existing_dir}" | sed 's/^0*//')
      [[ -z "$max_existing_batch" ]] && max_existing_batch=0

      echo Checking directories from ${CURRENT_BATCH} to ${max_existing_batch}

      while [[ "$CURRENT_BATCH" -le ${max_existing_batch} ]] ; do
        check_or_download || return 0
      done
    fi
}

check_or_download() {
    local padded=$(padded_batch_id)
    local batch_dir="$DEST/$padded"

    find "${batch_dir}" -name "*.gstmp" -type f -exec rm -f {} \;

    local file_count=$(ls -A "${batch_dir}" | wc -l)

    echo -en "\r\033[KChecking $padded "

    if [[ "$file_count" -ne 5 ]]; then
      download "${BUCKET}/${padded}"
      local new_file_count=$(ls -A "${batch_dir}" | wc -l)
      if [[ "$new_file_count" -ne 5 ]]; then
          return 2
      fi
    fi
    CURRENT_BATCH=$(($CURRENT_BATCH+1))
}