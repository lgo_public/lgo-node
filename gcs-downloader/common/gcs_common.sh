
padded_batch_id() {
    printf "%010d" ${CURRENT_BATCH}
}

download() {
    local origin=$1
    echo "Download $origin"
    gsutil -m -q cp -r "${origin}" "${DEST}"
}

create_dest_directory() {
    [[ -d "$DEST" ]] || mkdir -p "$DEST"
}

retrieve_actual_batch_id() {
    local max_existing_dir=$(ls -f -1 "${DEST}" | sort -r | head -1)
    local max_existing_batch=$(echo "${max_existing_dir}" | sed 's/^0*//')
    if [[ -z "$max_existing_batch" ]]; then
      echo -e "You should first use gcs_initial_download.sh"
      exit 1
    fi
    CURRENT_BATCH=${max_existing_batch}
}