DEST=/data/batch
CHECKPOINT=/data/batch/.checkpoint
CLEANING_CHECKPOINT=/data/batch/.last-cleaned-batch
BUCKET=gs://lgo-markets-batches
ARCHIVES_BUCKET=gs://lgo-markets-batches-archives