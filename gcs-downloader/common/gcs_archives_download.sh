
archives_download() {
    mkdir -p "/tmp/archives"

    echo Downloading last archives...
    gsutil -m cp -r "${ARCHIVES_BUCKET}/*" "/tmp/archives"

    local dir_list=(`ls -A "/tmp/archives"`)
    for ((i=0; i<${#dir_list[@]}; i++)); do
      local file=${dir_list[$i]}
      echo "Unpacking ${file}..."
      tar Jxf "/tmp/archives/${file}" -C "${DEST}"
      echo "Done."
    done
    rm -rf "/tmp/archives"
}
