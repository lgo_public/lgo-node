package lgonode.webapp.view;

import lgonode.webapp.domain.*;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class OrderBooksViewServiceTest {

    @Test
    void it_restores_books_() throws IOException {
        List<OrderBookEvent> events = Arrays.asList(new OrderBookRemoveOrderEvent("B", 40770000, 155386706230300001L));
        OrderBooksRepository orderBooksRepository = mock(OrderBooksRepository.class);
        OrderBookEventRepository eventRepository = mock(OrderBookEventRepository.class);
        OrderBooksViewService service = new OrderBooksViewService(orderBooksRepository, eventRepository);
        String snapshot = "{\"BTC-USD\":{\"B\":{\"40770000\":[{\"id\":155386706230300001,\"qt\":18300000}]},\"S\":{}}}";
        when(orderBooksRepository.findLastSnapshotFor(22)).thenReturn(new OrderBookSnapshot(20, snapshot));
        when(eventRepository.loadForCurrenciesAndBetweenBatches("BTC", "USD", 21, 22)).thenReturn(events);

        List<OrderBookLine> orderBookLines = service.retrieveLinesForBatch(22, "BTC", "USD");

        assertThat(orderBookLines).isEmpty();
    }

    @Test
    void it_restores_books() throws IOException {
        List<OrderBookEvent> events = List.of(
                new OrderBookAddOrderEvent("S", 10000000, 14, 500000000),
                new OrderBookAddOrderEvent("B", 1000000, 16, 1000000000),
                new OrderBookAddOrderEvent("B", 1000000, 17, 1200000000),
                new OrderBookAddOrderEvent("B", 1020000, 18, 1200000000),
                new OrderBookUpdateOrderEvent("B", 1020000, 18, 200000000),
                new OrderBookRemoveOrderEvent("B", 1020000, 18),
                new OrderBookAddOrderEvent("S", 1020000, 20, 1000000000));


        OrderBooksRepository orderBooksRepository = mock(OrderBooksRepository.class);
        OrderBookEventRepository eventRepository = mock(OrderBookEventRepository.class);
        when(orderBooksRepository.findLastSnapshotFor(22)).thenReturn(new OrderBookSnapshot(-1, "{}"));
        when(eventRepository.loadForCurrenciesAndBetweenBatches("BTC", "USD", 0, 22)).thenReturn(events);
        OrderBooksViewService service = new OrderBooksViewService(orderBooksRepository, eventRepository);

        List<OrderBookLine> orderBookLines = service.retrieveLinesForBatch(22, "BTC", "USD");

        assertThat(orderBookLines.get(0)).isEqualToComparingFieldByField(new OrderBookLine("sell", 1, "1000.0000", "5.00000000"));
        assertThat(orderBookLines.get(1)).isEqualToComparingFieldByField(new OrderBookLine("sell", 1, "102.0000", "10.00000000"));
        assertThat(orderBookLines.get(2)).isEqualToComparingFieldByField(new OrderBookLine("buy", 2, "100.0000", "22.00000000"));
    }
}