package lgonode.webapp.domain;

public class OrderBookAddOrderEvent extends OrderBookEvent {

    public OrderBookAddOrderEvent(String side, long price, long orderId, long quantity) {
        this.side = side;
        this.price = price;
        this.orderId = orderId;
        this.quantity = quantity;
    }

    public static OrderBookEvent restore(String payload) {
        String[] split = payload.split(",");
        String side = split[0];
        long price = Long.parseLong(split[1]);
        long orderId = Long.parseLong(split[2]);
        long quantity = Long.parseLong(split[3]);
        return new OrderBookAddOrderEvent(side, price, orderId, quantity);
    }

    public final String side;
    public final long price;
    public final long orderId;
    public final long quantity;
}
