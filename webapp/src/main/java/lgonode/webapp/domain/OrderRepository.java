package lgonode.webapp.domain;

import java.util.List;

public interface OrderRepository {
    List<Order> findByBatchId(long batchId);
}
