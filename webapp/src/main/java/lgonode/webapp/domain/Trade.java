package lgonode.webapp.domain;

public class Trade {

    public Trade(long id, long batchId, String baseCurrency, String quoteCurrency, String quantity, String price, long creationDate) {
        this.id = id;
        this.batchId = batchId;
        this.baseCurrency = baseCurrency;
        this.quoteCurrency = quoteCurrency;
        this.quantity = quantity;
        this.price = price;
        this.creationDate = creationDate;
    }

    public final long id;
    public final long batchId;
    public final String baseCurrency;
    public final String quoteCurrency;
    public final String quantity;
    public final String price;
    public final long creationDate;
}
