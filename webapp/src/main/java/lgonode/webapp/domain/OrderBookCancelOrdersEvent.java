package lgonode.webapp.domain;

public class OrderBookCancelOrdersEvent extends OrderBookEvent {

    public static OrderBookEvent restore() {
        return new OrderBookCancelOrdersEvent();
    }
}
