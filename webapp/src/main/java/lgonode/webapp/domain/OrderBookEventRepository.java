package lgonode.webapp.domain;

import java.util.List;

public interface OrderBookEventRepository {
    List<OrderBookEvent> loadForCurrenciesAndBetweenBatches(String baseCurrency, String quoteCurrency, long startBatchId, long endBatchId);
}
