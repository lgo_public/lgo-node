package lgonode.webapp.domain;

import java.util.List;

public interface FailedOrderRepository {
    List<FailedOrder> findByBatchId(long batchId);
}
