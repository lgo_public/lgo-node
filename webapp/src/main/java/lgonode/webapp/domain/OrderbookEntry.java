package lgonode.webapp.domain;

public class OrderbookEntry {

    public OrderbookEntry() {
    }

    public OrderbookEntry(long id, long qt) {
        this.id = id;
        this.qt = qt;
    }

    public long id;
    public long qt;
}
