package lgonode.webapp.domain;

import java.util.List;

public interface CancelOrderRepository {
    List<CancelOrder> findByBatchId(long batchId);
}
