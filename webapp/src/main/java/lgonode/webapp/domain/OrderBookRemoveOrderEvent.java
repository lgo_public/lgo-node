package lgonode.webapp.domain;

public class OrderBookRemoveOrderEvent extends OrderBookEvent {

    public OrderBookRemoveOrderEvent(String side, long price, long orderId) {
        this.side = side;
        this.price = price;
        this.orderId = orderId;
    }

    public static OrderBookEvent restore(String content) {
        String[] split = content.split(",");
        String side = split[0];
        long price = Long.parseLong(split[1]);
        long orderId = Long.parseLong(split[2]);
        return new OrderBookRemoveOrderEvent(side, price, orderId);
    }

    public final String side;
    public final long price;
    public final long orderId;
}
