package lgonode.webapp.domain;

import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static lgonode.webapp.dao.FailedOrderDAO.*;

@Service
public class FailedOrderJooqRepository implements FailedOrderRepository {

    @Autowired
    public FailedOrderJooqRepository(DSLContext dslContext) {
        this.dslContext = dslContext;
    }

    @Override
    public List<FailedOrder> findByBatchId(long batchId) {
        return dslContext.select(ID, BATCH_ID, REASON)
                .from(TABLE)
                .where(BATCH_ID.eq(batchId))
                .fetchInto(FailedOrder.class);
    }

    private final DSLContext dslContext;
}
