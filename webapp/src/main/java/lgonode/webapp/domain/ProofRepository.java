package lgonode.webapp.domain;

import java.util.List;

public interface ProofRepository {
    Proof find(long id);

    List<Proof> findAll(int requestedPage, int limit);
}
