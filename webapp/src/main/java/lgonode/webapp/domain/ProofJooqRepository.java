package lgonode.webapp.domain;

import lgonode.webapp.dao.ProofDAO;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProofJooqRepository implements ProofRepository {

    @Autowired
    public ProofJooqRepository(DSLContext dslContext) {
        this.dslContext = dslContext;
    }

    @Override
    public List<Proof> findAll(int requestedPage, int limit) {
        return dslContext.select(ProofDAO.BATCH_ID, ProofDAO.TIMESTAMPED_HASH, ProofDAO.CONTENT)
                .from(ProofDAO.TABLE)
                .orderBy(ProofDAO.BATCH_ID.desc())
                .offset((requestedPage - 1) * limit)
                .limit(limit)
                .fetchInto(Proof.class);
    }

    @Override
    public Proof find(long id) {
        return dslContext.select(ProofDAO.BATCH_ID, ProofDAO.TIMESTAMPED_HASH, ProofDAO.CONTENT)
                .from(ProofDAO.TABLE)
                .where(ProofDAO.BATCH_ID.eq(id))
                .fetchOneInto(Proof.class);
    }

    private final DSLContext dslContext;
}
