package lgonode.webapp.domain;

import lgonode.webapp.dao.OrderBooksDAO;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderBooksJooqRepository implements OrderBooksRepository {

    @Autowired
    public OrderBooksJooqRepository(DSLContext dslContext) {
        this.dslContext = dslContext;
    }

    @Override
    public OrderBookSnapshot findLastSnapshotFor(long batchId) {
        return dslContext.select(OrderBooksDAO.BATCH_ID, OrderBooksDAO.CONTENT)
                .from(OrderBooksDAO.TABLE)
                .where(OrderBooksDAO.BATCH_ID.lessOrEqual(batchId))
                .orderBy(OrderBooksDAO.BATCH_ID.desc())
                .limit(1)
                .fetchOptionalInto(OrderBookSnapshot.class)
                .orElse(new OrderBookSnapshot(-1, "{}"));
    }


    private final DSLContext dslContext;
}
