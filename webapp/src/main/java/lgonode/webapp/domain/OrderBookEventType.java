package lgonode.webapp.domain;

public enum OrderBookEventType {

    CANCEL_BOOK {
        @Override
        public OrderBookEvent restore(String payload) {
            return OrderBookCancelOrdersEvent.restore();
        }
    }, UPDATE_ORDER {
        @Override
        public OrderBookEvent restore(String payload) {
            return OrderBookUpdateOrderEvent.restore(payload);
        }
    }, ADD_ORDER {
        @Override
        public OrderBookEvent restore(String payload) {
            return OrderBookAddOrderEvent.restore(payload);
        }
    }, REMOVE_ORDER {
        @Override
        public OrderBookEvent restore(String payload) {
            return OrderBookRemoveOrderEvent.restore(payload);
        }
    };

    public abstract OrderBookEvent restore(String payload);
}
