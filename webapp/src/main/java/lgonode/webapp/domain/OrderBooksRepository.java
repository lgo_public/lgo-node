package lgonode.webapp.domain;

public interface OrderBooksRepository {
    OrderBookSnapshot findLastSnapshotFor(long batchId);
}
