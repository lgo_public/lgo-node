package lgonode.webapp.domain;

import lgonode.webapp.dao.CancelOrderDAO;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CancelOrderJooqRepository implements CancelOrderRepository {

    @Autowired
    public CancelOrderJooqRepository(DSLContext dslContext) {
        this.dslContext = dslContext;
    }

    @Override
    public List<CancelOrder> findByBatchId(long batchId) {
        return dslContext.select(CancelOrderDAO.ID, CancelOrderDAO.BATCH_ID, CancelOrderDAO.KEY_ID, CancelOrderDAO.CREATION_DATE, CancelOrderDAO.ORDER_TO_CANCEL)
                .from(CancelOrderDAO.TABLE)
                .where(CancelOrderDAO.BATCH_ID.eq(batchId))
                .fetchInto(CancelOrder.class);
    }

    private final DSLContext dslContext;
}
