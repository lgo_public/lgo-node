package lgonode.webapp.domain;

import java.util.List;

public interface TradeRepository {
    List<Trade> findByBatchId(long batchId);
    List<Trade> findAll(int requestedPage, int tradePerPage);
}
