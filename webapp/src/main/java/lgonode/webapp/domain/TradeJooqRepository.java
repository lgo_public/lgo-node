package lgonode.webapp.domain;

import lgonode.webapp.dao.TradeDAO;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TradeJooqRepository implements TradeRepository {

    @Autowired
    public TradeJooqRepository(DSLContext dslContext) {
        this.dslContext = dslContext;
    }

    @Override
    public List<Trade> findAll(int requestedPage, int limit) {
        return dslContext.select(TradeDAO.ID, TradeDAO.BATCH_ID, TradeDAO.BASE_CURRENCY, TradeDAO.QUOTE_CURRENCY, TradeDAO.QUANTITY, TradeDAO.PRICE, TradeDAO.CREATION_DATE)
                .from(TradeDAO.TABLE)
                .orderBy(TradeDAO.ID.desc())
                .offset((requestedPage - 1) * limit)
                .limit(limit)
                .fetchInto(Trade.class);
    }

    @Override
    public List<Trade> findByBatchId(long batchId) {
        return dslContext.select(TradeDAO.ID, TradeDAO.BATCH_ID, TradeDAO.BASE_CURRENCY, TradeDAO.QUOTE_CURRENCY, TradeDAO.QUANTITY, TradeDAO.PRICE, TradeDAO.CREATION_DATE)
                .from(TradeDAO.TABLE)
                .where(TradeDAO.BATCH_ID.eq(batchId))
                .fetchInto(Trade.class);
    }

    private final DSLContext dslContext;
}
