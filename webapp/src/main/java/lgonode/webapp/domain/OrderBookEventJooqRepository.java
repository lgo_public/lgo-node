package lgonode.webapp.domain;

import org.jooq.DSLContext;
import org.jooq.Record2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static lgonode.webapp.dao.OrderBookEventsDAO.*;

@Service
public class OrderBookEventJooqRepository implements OrderBookEventRepository {

    @Autowired
    public OrderBookEventJooqRepository(DSLContext dsl) {
        this.dsl = dsl;
    }

    @Override
    public List<OrderBookEvent> loadForCurrenciesAndBetweenBatches(String baseCurrency, String quoteCurrency, long startBatchId, long endBatchId) {
        return this.dsl.select(TYPE, PAYLOAD)
                .from(TABLE)
                .where(BATCH_ID.greaterOrEqual(startBatchId))
                .and(BATCH_ID.lessOrEqual(endBatchId))
                .and(BASE_CURRENCY.eq(baseCurrency))
                .and(QUOTE_CURRENCY.eq(quoteCurrency))
                .orderBy(ID.asc())
                .fetch()
                .map(this::map);
    }

    private OrderBookEvent map(Record2<String, String> record) {
        OrderBookEventType eventType = OrderBookEventType.valueOf(record.component1());
        String payload = record.component2();
        return eventType.restore(payload);
    }

    private final DSLContext dsl;
}
