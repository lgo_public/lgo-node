package lgonode.webapp.domain;

import org.decimal4j.api.DecimalArithmetic;
import org.decimal4j.scale.Scales;

public class Currency {

    private Currency(int scale) {
        this.arithmetic = Scales.getScaleMetrics(scale).getDefaultArithmetic();
    }

    public String format(long unscaledValue) {
        return arithmetic.toString(unscaledValue);
    }

    private final DecimalArithmetic arithmetic;

    public static final Currency BTC = new Currency(8);
    public static final Currency USD = new Currency(4);

}
