package lgonode.webapp.dao;

import org.jooq.Field;
import org.jooq.Record;
import org.jooq.Table;
import org.jooq.impl.DSL;

public class FailedOrderDAO {
    public static Table<Record> TABLE = DSL.table("failed_order_view");
    public static Field<Long> ID = DSL.field("id", Long.class);
    public static Field<Long> BATCH_ID = DSL.field("batch_id", Long.class);
    public static Field<String> REASON = DSL.field("reason", String.class);
}