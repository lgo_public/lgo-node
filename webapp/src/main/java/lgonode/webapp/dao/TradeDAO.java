package lgonode.webapp.dao;

import org.jooq.Field;
import org.jooq.Record;
import org.jooq.Table;
import org.jooq.impl.DSL;

public class TradeDAO {
    public static Table<Record> TABLE = DSL.table("trade_view");
    public static Field<Integer> ID = DSL.field("id", Integer.class);
    public static Field<Long> BATCH_ID = DSL.field("batch_id", Long.class);
    public static Field<Long> CREATION_DATE = DSL.field("creation_date", Long.class);
    public static Field<String> BASE_CURRENCY = DSL.field("base_currency", String.class);
    public static Field<String> QUOTE_CURRENCY = DSL.field("quote_currency", String.class);
    public static Field<String> QUANTITY = DSL.field("quantity", String.class);
    public static Field<String> PRICE = DSL.field("price", String.class);
}