package lgonode.webapp.dao;

import org.jooq.Field;
import org.jooq.Record;
import org.jooq.Table;
import org.jooq.impl.DSL;

public class OrderBookEventsDAO {
    public static Table<Record> TABLE = DSL.table("orderbookevents_view");
    public static Field<Integer> ID = DSL.field("id", Integer.class);
    public static Field<Long> BATCH_ID = DSL.field("batch_id", Long.class);
    public static Field<String> BASE_CURRENCY = DSL.field("base_currency", String.class);
    public static Field<String> QUOTE_CURRENCY = DSL.field("quote_currency", String.class);
    public static Field<String> TYPE = DSL.field("type", String.class);
    public static Field<String> PAYLOAD = DSL.field("payload", String.class);
}