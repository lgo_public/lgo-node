package lgonode.webapp.controller;

import lgonode.webapp.view.TradeViewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/trades")
public class TradesController {

    @Autowired
    public TradesController(TradeViewService tradeViewService) {
        this.tradeViewService = tradeViewService;
    }

    @GetMapping
    public String fetch(Model model, @RequestParam(value = "page", required = false, defaultValue = "1") Integer page) {
        int currentPage = (page == null || page <= 0) ? 1 : page;
        int prevPage = currentPage - 1;
        int nextPage = currentPage + 1;
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("prevPage", prevPage);
        model.addAttribute("nextPage", nextPage);
        model.addAttribute("trades", tradeViewService.tradeList(page));
        return "trades";
    }

    private final TradeViewService tradeViewService;
}
