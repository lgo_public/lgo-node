package lgonode.webapp.controller;


import lgonode.webapp.view.OrderBooksViewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@Controller
@RequestMapping("/batches/{batchId}/orderbooks/{product}")
public class BatchOrderBookController {

    @Autowired
    public BatchOrderBookController(OrderBooksViewService orderBooksViewService) {
        this.orderBooksViewService = orderBooksViewService;
    }

    @GetMapping
    public String getPartial(Model model, @PathVariable(value = "batchId") long batchId, @PathVariable(value = "product") String product) throws IOException {
        String[] split = product.split("-");
        model.addAttribute("orderbook", orderBooksViewService.retrieveLinesForBatch(batchId, split[0], split[1]));
        model.addAttribute("batchId", batchId);
        model.addAttribute("product", product);
        return "order_book_fragment :: book";
    }

    private final OrderBooksViewService orderBooksViewService;

}
