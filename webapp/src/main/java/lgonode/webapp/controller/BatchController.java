package lgonode.webapp.controller;

import lgonode.webapp.view.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@Controller
@RequestMapping("/batches/{batchId}")
public class BatchController {

    @Autowired
    public BatchController(BatchViewService batchViewService, TradeViewService tradeViewService, OrderBooksViewService orderBooksViewService) {
        this.batchViewService = batchViewService;
        this.tradeViewService = tradeViewService;
        this.orderBooksViewService = orderBooksViewService;
    }

    @GetMapping
    public String fetchOne(Model model, @PathVariable(value = "batchId") long batchId) throws IOException {
        model.addAttribute("product", "BTC-USD");
        model.addAttribute("batch", batchViewService.retrieve(batchId));
        model.addAttribute("trades", tradeViewService.retrieveForBatch(batchId));
        model.addAttribute("orderbook", orderBooksViewService.retrieveLinesForBatch(batchId, "BTC", "USD"));
        return "batch";
    }

    private final BatchViewService batchViewService;
    private final TradeViewService tradeViewService;
    private final OrderBooksViewService orderBooksViewService;

}
