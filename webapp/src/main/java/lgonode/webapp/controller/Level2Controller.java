package lgonode.webapp.controller;

import lgonode.webapp.view.OrderBookLine;
import lgonode.webapp.view.OrderBooksViewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/products/{product}/l2")
public class Level2Controller {

    @Autowired
    private OrderBooksViewService service;


    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public Response level2(@PathVariable("product") String product, @RequestParam("batchId") long batchId) throws IOException {
        if (product == null || product.isBlank()) {
            return null;
        }
        String[] currencies = product.split("-");
        if (currencies.length != 2) {
            return null;
        }
        List<OrderBookLine> lines = service.retrieveLinesForBatch(batchId, currencies[0], currencies[1]);
        Response response = new Response(product, batchId);
        for (OrderBookLine line : lines) {
            if (line.side.equals("sell")) {
                response.asks.add(Arrays.asList(line.price, line.quantity));
            } else {
                response.bids.add(Arrays.asList(line.price, line.quantity));
            }
        }
        return response;
    }

    static class Response {

        public Response(String productId, long batchId) {
            this.productId = productId;
            this.batchId = batchId;
        }

        public String productId;
        public long batchId;
        public List<List<String>> bids = new ArrayList<>();
        public List<List<String>> asks = new ArrayList<>();
    }
}
