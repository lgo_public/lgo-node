package lgonode.webapp.view;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lgonode.webapp.domain.Currency;
import lgonode.webapp.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;

import static java.util.List.of;
import static java.util.stream.Collectors.toList;

@Service
public class OrderBooksViewService {

    @Autowired
    public OrderBooksViewService(OrderBooksRepository orderBooksRepository, OrderBookEventRepository eventRepository) {
        this.orderBooksRepository = orderBooksRepository;
        this.eventRepository = eventRepository;
    }

    public List<OrderBookLine> retrieveLinesForBatch(long batchId, String baseCurrency, String quoteCurrency) throws IOException {
        Map<String, TreeMap<Long, LinkedList<OrderbookEntry>>> orderbook = buildOrderBook(batchId, baseCurrency, quoteCurrency);
        return convertToLines(orderbook);
    }

    private List<OrderBookLine> convertToLines(Map<String, TreeMap<Long, LinkedList<OrderbookEntry>>> orderbook) {
        List<OrderBookLine> lines = new ArrayList<>();
        List<OrderBookLine> sellLines = orderbook.get("S").entrySet().stream().map(entry -> mapToLineView(entry, "sell")).collect(toList());
        Collections.reverse(sellLines);
        lines.addAll(sellLines);
        List<OrderBookLine> buyLines = orderbook.get("B").entrySet().stream().map(entry -> mapToLineView(entry, "buy")).collect(toList());
        Collections.reverse(buyLines);
        lines.addAll(buyLines);
        return lines;
    }

    private OrderBookLine mapToLineView(Map.Entry<Long, LinkedList<OrderbookEntry>> entry, String side) {
        int count = entry.getValue().size();
        long quantity = entry.getValue().stream().mapToLong(e -> e.qt).sum();
        long price = entry.getKey();
        return new OrderBookLine(side, count, Currency.USD.format(price), Currency.BTC.format(quantity));
    }

    private Map<String, TreeMap<Long, LinkedList<OrderbookEntry>>> buildOrderBook(long batchId, String baseCurrency, String quoteCurrency) throws IOException {
        OrderBookSnapshot snapshot = orderBooksRepository.findLastSnapshotFor(batchId);
        Map<String, Map<String, TreeMap<Long, LinkedList<OrderbookEntry>>>> byBatchId = objectMapper.readValue(snapshot.serializedContent, typeRef);
        Map<String, TreeMap<Long, LinkedList<OrderbookEntry>>> orderbook = byBatchId.getOrDefault(baseCurrency+"-"+quoteCurrency, emptyBook());

        if (snapshot.batchId < batchId) {
            List<OrderBookEvent> events = eventRepository.loadForCurrenciesAndBetweenBatches(baseCurrency, quoteCurrency, snapshot.batchId + 1, batchId);
            for (OrderBookEvent event : events) {
                if (event instanceof OrderBookCancelOrdersEvent) {
                    orderbook.get("B").clear();
                    orderbook.get("S").clear();
                }
                if (event instanceof OrderBookUpdateOrderEvent) {
                    OrderBookUpdateOrderEvent e = (OrderBookUpdateOrderEvent) event;
                    for (OrderbookEntry o : orderbook.get(e.side).get(e.price)) {
                        if (o.id == e.orderId) {
                            o.qt = e.quantity;
                            break;
                        }
                    }
                }
                if (event instanceof OrderBookAddOrderEvent) {
                    OrderBookAddOrderEvent addEvent = (OrderBookAddOrderEvent) event;
                    TreeMap<Long, LinkedList<OrderbookEntry>> side = orderbook.get(addEvent.side);
                    OrderbookEntry entry = new OrderbookEntry(addEvent.orderId, addEvent.quantity);
                    Optional.ofNullable(side.get(addEvent.price))
                            .ifPresentOrElse(
                                    e -> e.add(entry),
                                    () -> side.put(addEvent.price, new LinkedList<>(of(entry))));
                }
                if (event instanceof OrderBookRemoveOrderEvent) {
                    OrderBookRemoveOrderEvent orderBookEvent = (OrderBookRemoveOrderEvent) event;
                    LinkedList<OrderbookEntry> entries = orderbook.get(orderBookEvent.side).get(orderBookEvent.price);
                    for (OrderbookEntry entry : entries) {
                        if (entry.id == orderBookEvent.orderId) {
                            entries.remove(entry);
                            break;
                        }
                    }
                    if (entries.isEmpty()) {
                        orderbook.get(orderBookEvent.side).remove(orderBookEvent.price);
                    }
                }
            }
        }
        return orderbook;
    }

    private Map<String, TreeMap<Long, LinkedList<OrderbookEntry>>> emptyBook() {
        Map<String, TreeMap<Long, LinkedList<OrderbookEntry>>> orderBook = new HashMap<>();
        orderBook.put("B", new TreeMap<>());
        orderBook.put("S", new TreeMap<>());
        return orderBook;
    }

    private final OrderBooksRepository orderBooksRepository;
    private final OrderBookEventRepository eventRepository;
    private final ObjectMapper objectMapper = new ObjectMapper();
    private TypeReference<Map<String, Map<String, TreeMap<Long, LinkedList<OrderbookEntry>>>>> typeRef = new TypeReference<>() {};
}
