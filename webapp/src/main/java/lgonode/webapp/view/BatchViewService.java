package lgonode.webapp.view;

import lgonode.webapp.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class BatchViewService {

    public static final int BATCHES_PER_PAGE = 50;

    @Autowired
    public BatchViewService(BatchRepository batchRepository, OrderRepository orderRepository, FailedOrderRepository failedOrderRepository, CancelOrderRepository cancelOrderRepository, ProofRepository proofRepository) {
        this.batchRepository = batchRepository;
        this.orderRepository = orderRepository;
        this.failedOrderRepository = failedOrderRepository;
        this.cancelOrderRepository = cancelOrderRepository;
        this.proofRepository = proofRepository;
    }

    public List<Metadata> batchMetadataList(Integer page) {
        int requestedPage = page == null ? 1 : page;
        requestedPage = requestedPage < 1 ? 1 : requestedPage;
        return batchRepository.findAll(requestedPage, BATCHES_PER_PAGE);
    }

    public BatchView retrieve(long id) {
        Metadata metadata = batchRepository.find(id);
        return buildBatchView(metadata);
    }

    private BatchView buildBatchView(Metadata metadata) {
        List<Order> orders = orderRepository.findByBatchId(metadata.id);
        List<FailedOrder> failedOrders = failedOrderRepository.findByBatchId(metadata.id);
        List<CancelOrder> cancelOrders = cancelOrderRepository.findByBatchId(metadata.id);
        Proof proof = proofRepository.find(metadata.id);
        Map<Long, OrderView> views = new HashMap<>();
        for (Order order : orders) {
            OrderView orderView = new OrderView(order);
            views.put(orderView.id, orderView);
        }
        for (CancelOrder cancelOrder : cancelOrders) {
            OrderView orderView = new OrderView(cancelOrder);
            views.put(orderView.id, orderView);
        }
        for (FailedOrder failedOrder : failedOrders) {
            if (views.containsKey(failedOrder.id)) {
                views.get(failedOrder.id).failed(failedOrder);
            } else {
                views.put(failedOrder.id, new OrderView(failedOrder));
            }
        }
        ArrayList<OrderView> list = new ArrayList<>(views.values());
        Collections.sort(list);
        return new BatchView(metadata.id, metadata.hash, metadata.previousHash, metadata.orderCount, metadata.cancelOrderBooks, list, new ProofView(proof.timestampedHash, proof.content));
    }

    private final BatchRepository batchRepository;
    private final OrderRepository orderRepository;
    private final FailedOrderRepository failedOrderRepository;
    private final CancelOrderRepository cancelOrderRepository;
    private final ProofRepository proofRepository;
}
