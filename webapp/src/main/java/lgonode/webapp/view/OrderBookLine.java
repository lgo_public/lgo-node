package lgonode.webapp.view;

public class OrderBookLine {

    public OrderBookLine(String side, int count, String price, String quantity) {
        this.side = side;
        this.count = count;
        this.price = price;
        this.quantity = quantity;
    }

    public final String side;
    public final int count;
    public final String price;
    public final String quantity;
}
