package lgonode.webapp.view;

import lgonode.webapp.domain.Trade;

import java.util.Date;

public class TradeView {

    public TradeView(Trade trade) {
        this.creationDate = new Date(trade.creationDate);
        this.description = String.format("Trade of %s%s at %s%s", trade.quantity, trade.baseCurrency, trade.price, trade.quoteCurrency);
        this.batchId = trade.batchId;
        this.id = trade.id;
    }

    public final long id;
    public final long batchId;
    public final Date creationDate;
    public final String description;
}
