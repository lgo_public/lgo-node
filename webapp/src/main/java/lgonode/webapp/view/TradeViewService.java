package lgonode.webapp.view;

import lgonode.webapp.domain.Trade;
import lgonode.webapp.domain.TradeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TradeViewService {

    private static final int TRADES_PER_PAGE = 20;

    @Autowired
    public TradeViewService(TradeRepository tradeRepository) {
        this.tradeRepository = tradeRepository;
    }

    public List<TradeView> retrieveForBatch(long batchId) {
        return tradeRepository.findByBatchId(batchId).stream().map(TradeView::new).collect(Collectors.toList());
    }

    public List<TradeView> tradeList(Integer page) {
        int requestedPage = page == null ? 1 : page;
        requestedPage = requestedPage < 1 ? 1 : requestedPage;
        List<Trade> trades = tradeRepository.findAll(requestedPage, TRADES_PER_PAGE);
        return trades.stream().map(TradeView::new).collect(Collectors.toList());
    }

    private final TradeRepository tradeRepository;

}
