package lgonode.webapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FairnessApplication {

    public static void main(String[] args) {
        SpringApplication.run(FairnessApplication.class, args);
    }
}
