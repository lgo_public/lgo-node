
### Batching

#### Batch hash calculation

Batch's hash is a **SHA256 hash** of the batch content plus last bast's hash for chaining.

The batch's hash contains, in this order:

1. The hash of the previous batch (or the orders hash for the first batch of the chain)
1. The orders hash

The orders hash is also a SHA256 hash of the orders list. It contains, for each order:

1. The order identifier
1. The order public key identifier
1. The order encrypted data

#### Public data produced by the platform

For each batch, the LGO platform produces 5 files in a GCS bucket. These files are stored in a directory named with the identifier of the batch padded on ten zeros. 

Ex: batch 642638 can be found at *gs://lgo-markets-batches/0000642638*.

These files are:
- The batch metadata file ``0000642638.metadata``
- The encrypted order list ``0000642638.avro``
- The failed orders list ``0000642638-failed-orders.avro``
- The OpenTimestamps proof and upgraded proof ``0000642638.ots`` and ``0000642638.ots.upgraded.ots``
