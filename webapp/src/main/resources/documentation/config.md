### LGO platform configuration

#### Order checks 

- quantity : (in base currency for a limit order and a market sell order, in quote currency for a market buy order)

  min ≤ remaining quantity ≤ max

- price (quote currency for limit order)

  min ≤ price ≤ max

- amount (price × remaining quantity, quote currency)

  min ≤ amount ≤ max

- price increment

  price is a multiple of quote increment
  
Market orders are only checked on quantity.

Actual limits are, for BTC/USD pair:

| Limit               | Value        |
|---------------------|--------------|
| BTC quantity min    | BTC 0.001    |
| BTC quantity max    | BTC 1000     |
| USD price min       | USD 10       |
| USD price max       | USD 1000000  |
| USD price increment | USD 0.10     |
| Amount min          | USD 10       |
| Amount max          | USD 50000000 |


And for LGO/USD pair:

| Limit               | Value        |
|---------------------|--------------|
| LGO quantity min    | LGO 500      |
| LGO quantity max    | LGO 10000000 |
| USD price min       | USD 0.001    |
| USD price max       | USD 100      |
| USD price increment | USD 0.0001   |
| Amount min          | USD 10       |
| Amount max          | USD 3000000  |

#### Algorithms

- Actual Self Trade Prevention behavior: cancel remaining quantity of resting order
- Actual Matching algorithm: FIFO