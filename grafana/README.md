#Grafana dashboard

These command lines will launch a Grafana container with preloaded candlestick plugin + LGO dashboard (queries on the influxdb database):

```
ID=$(id -u)
docker run -d --user $ID --volume "$PWD/grafana:/var/lib/grafana" --volume "$PWD/grafana/config:/etc/grafana" --network=lgo-node --name=grafana -p localhost:3000:3000 grafana/grafana
```

Admin default account: 
* user: admin
* password: admin

Viewer account:
* user: viewer 
* password: viewer 