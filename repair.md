# Some useful things...

## Postgres

### Select and update trade_view id sequence

```
SELECT last_value FROM trade_view_id_seq;
SELECT setval('trade_view_id_seq', (SELECT MAX(id) FROM trade_view));
```

### Find missing batch id

```
SELECT generate_series(0, (SELECT MAX(id) FROM batch_view)) EXCEPT SELECT id FROM batch_view;
```

### Delete batch files

It would be really long to just `rm -rf cache/`, so you can instead:

```
mkdir /tmp/empty
rsync -a --delete /tmp/empty/ cache/batch/
```

### Repair old creation date

```
update order_view set creation_date = LEFT(id::varchar(255), -5)::int8;
update cancel_order_view set creation_date = LEFT(id::varchar(255), -5)::int8;
update trade_view set creation_date=order_view.creation_date from order_view where trade_view.batch_id=order_view.batch_id;
```